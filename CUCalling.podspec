#
# Be sure to run `pod lib lint CUCalling.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'CUCalling'
  s.version          = '0.1.0'
  s.summary          = 'A short description of CUCalling.'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = <<-DESC
TODO: Add long description of the pod here.
                       DESC

  s.homepage         = 'https://www.getcuapp.com'
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'sourabh_yapapp' => 'sourabh.sharotria@yapits.com' }
  s.source           = { :git => 'https://sourabh_yapapp@bitbucket.org/sourabh_yapapp/cucalling.git', :tag => s.version.to_s }
  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'

  s.ios.deployment_target = '10.0'

  s.source_files = 'CUCalling/Classes/*'
  
  # s.resource_bundles = {
  #   'CUCalling' => ['CUCalling/Assets/*.png']
  # }

  # s.public_header_files = 'Pod/Classes/**/*.h'
  # s.frameworks = 'UIKit', 'MapKit'
  # s.dependency 'AFNetworking', '~> 2.3'
  s.dependency 'TDDBService', '0.1.4'
  s.dependency 'GoogleWebRTC'
  s.dependency 'Socket.IO-Client-Swift'
  s.dependency 'TDWebServiceAlamofire','4.6.0.1'
  s.dependency 'TDDBServiceKeychainSwift'
  s.dependency 'JSQSystemSoundPlayer'
  s.dependency 'RealmSwift', '2.10.1'
  s.dependency 'ObjectMapper'
end
