//
//  ViewController.swift
//  CUCalling
//
//  Created by sourabh_yapapp on 03/22/2018.
//  Copyright (c) 2018 sourabh_yapapp. All rights reserved.
//

import UIKit
import CUCalling

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func pushToCall(_ sender: Any) {
        let callHandlerObj = CallHandler()
        callHandlerObj.pushToCallView(navigationCont: self.navigationController!)
        
    }
    
}

