//
//  Utilities+JSON.swift
//  Nehao
//
//  Created by Abhimanu Jindal on 27/02/17.
//  Copyright © 2017 Amit Tripathi. All rights reserved.
//

import Foundation

extension(Utilities){
    
    static func convertJsonStringToDictionary(text: String) -> [String: Any]? {
        if let data = text.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            } catch {
                YALog.print(error.localizedDescription)
            }
        }
        return nil
    }
    
    static func convertDictionaryToJsonString(dic:[String:Any]) -> String? {
        do {
            let theJSONData = try JSONSerialization.data(withJSONObject: dic, options: JSONSerialization.WritingOptions.init(rawValue: 0))
            let theJSONText = NSString(data: theJSONData,encoding: String.Encoding.ascii.rawValue)
            return "\(theJSONText!)"
        } catch {
            YALog.print(error.localizedDescription)
        }
        return nil
    }
}
