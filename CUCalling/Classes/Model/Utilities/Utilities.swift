//
//  Utilities.swift
//  Nehao
//
//  Created by Abhimanu Jindal on 27/02/17.
//  Copyright © 2017 Amit Tripathi. All rights reserved.
//

import Foundation
import AssetsLibrary

var isCallInProgress :Bool = false
var contactBeingDeleted:Contact?
//var user:User?

enum ApiError: Error{
    case badAccessToken
    case accountRestored
    case generalError
    case emptyResponse
    case invalidResponseType
    case unknown
}

class Utilities:NSObject{
    
    static func isArrayEmpty(_ array:Array<Any>?) -> Bool{
        if (array?.count == nil ? -1:(array?.count)!) > 0{
            return false
        }
        return true
    }
    
    static func getRedThemeColor() -> UIColor {
        return Utilities.colorWithHexString(hex: "#EF233C")
    }
    static func colorWithHexString (hex:String) -> UIColor {
        var hexString:String = hex.trimmingCharacters(in: NSCharacterSet.whitespacesAndNewlines).uppercased()
        if (hexString.hasPrefix("#")) {
            hexString = (hexString as NSString).substring(from: 1)
        }
        if (hexString.characters.count != 6) {
            return UIColor.gray
        }
        let rString = (hexString as NSString).substring(to: 2)
        let gString = ((hexString as NSString).substring(from: 2) as NSString).substring(to: 2)
        let bString = ((hexString as NSString).substring(from: 4) as NSString).substring(to: 2)
        var r:CUnsignedInt = 0, g:CUnsignedInt = 0, b:CUnsignedInt = 0;
        Scanner(string: rString).scanHexInt32(&r)
        Scanner(string: gString).scanHexInt32(&g)
        Scanner(string: bString).scanHexInt32(&b)
        return UIColor(red: CGFloat(r) / 255.0, green: CGFloat(g) / 255.0, blue: CGFloat(b) / 255.0, alpha: CGFloat(1))
    }
}
