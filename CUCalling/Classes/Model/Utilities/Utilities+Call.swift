//
//  Utilities+CallMediaAlert.swift
//  Nehao
//
//  Created by Tina Gupta on 19/05/17.
//  Copyright © 2017 Amit Tripathi. All rights reserved.
//

import Foundation

enum CallMediaAccessibilityAlert: String{
    case OutGoingCallFailedForCamera = "To place calls, CU needs access to your device camera. tap Settings and Turn on Camera"
    case OutGoingCallFailedForMic = "To place calls, CU needs access to your device microphone. tap Settings and Turn on Microphone"
    case IncomingVideoCallFailedForCamera = "To use video calls, CU needs access to your device camera. tap Settings and Turn on Camera"
    case IncomingVideoCallFailedForMic = "To use video calls, CU needs access to your device microphone. tap Settings and Turn on Microphone"
    case IncomingAudioCallFailed = "To use audio calls, CU needs access to your device microphone. tap Settings and Turn on Microphone"
}


extension(Utilities)
{
    
    //MARK:- Checking Media Access for call
    static func checkMediaAccessForVideoCall(completionBlock:@escaping(_ isGranted: Bool, _ mediaAccessType: Utilities.MediaAccessType)->Void)
    {
            Utilities.requestForHardwareAccess(accessType: .Microphone, completionBlock: { (isGranted) in
                if isGranted{
                    Utilities.requestForHardwareAccess(accessType: .Camera, completionBlock: { (isGranted) in
                        completionBlock(isGranted,.Camera)
                    })
                }
                else{
                    completionBlock(isGranted,.Microphone)
                }
            })
    }
    
   static func checkMediaAccessForAudioCall(completionBlock:@escaping(_ isGranted: Bool, _ mediaAccessType: Utilities.MediaAccessType)->Void)
    {
        Utilities.requestForHardwareAccess(accessType: .Microphone, completionBlock: { (isGranted) in
                    completionBlock(isGranted,.Microphone)
        })
    }
    

    
    //MARK:- Alert fpr Media Access
    static func showNoAccessAlert(isVideoCall: Bool, accessType: Utilities.MediaAccessType, isIncomingCall: Bool)
    {
        var alertText = ""
        if isVideoCall
        {
            alertText = setAlertTextForVideoCall(accessType: accessType, isIncomingCall: isIncomingCall)
        }
        else{
            alertText = setAlertTextForAudioCall(accessType: accessType, isIncomingCall: isIncomingCall)
        }
        
//        AlertCentral.showSettingsAlertView(withTitle: "CU", body: alertText, requiredCancelButton: true)

    }
    
    private static func setAlertTextForVideoCall(accessType:Utilities.MediaAccessType, isIncomingCall:Bool )-> String
    {
        var alertText = ""
        if isIncomingCall
        {
            alertText = alertTextForIncomingVideoCall(accessType: accessType)
        }
        else{
            alertText = alertTextForOutgoingVideoCall(accessType: accessType)
        }
        return alertText
    }
    
    private static func setAlertTextForAudioCall(accessType:Utilities.MediaAccessType, isIncomingCall:Bool )-> String
    {
        var alertText = ""
        if isIncomingCall{
            alertText = CallMediaAccessibilityAlert.IncomingAudioCallFailed.rawValue
        }
        else{
            alertText = CallMediaAccessibilityAlert.OutGoingCallFailedForMic.rawValue
        }
        return alertText
    }

    private static func alertTextForOutgoingVideoCall(accessType:Utilities.MediaAccessType )-> String
    {
        var alertText = ""
        if accessType == .Camera
        {
            alertText = CallMediaAccessibilityAlert.OutGoingCallFailedForCamera.rawValue
        }
        else
        {
            alertText = CallMediaAccessibilityAlert.OutGoingCallFailedForMic.rawValue
        }

        return alertText
    }
    
    private static func alertTextForIncomingVideoCall(accessType:Utilities.MediaAccessType )-> String
    {
        var alertText = ""
        if accessType == .Camera
        {
            alertText = CallMediaAccessibilityAlert.IncomingVideoCallFailedForCamera.rawValue
        }
        else
        {
            alertText = CallMediaAccessibilityAlert.IncomingVideoCallFailedForMic.rawValue
        }
        return alertText
    }

}
