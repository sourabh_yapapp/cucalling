//
//  Utilities+chat.swift
//  Nehao
//
//  Created by Tina Gupta on 25/05/17.
//  Copyright © 2017 Amit Tripathi. All rights reserved.
//

import Foundation

extension(Utilities)
{
    
    static func getRoomID(contact: Contact?)-> String
    {
        if contact == nil{
        return ""
        }
        if !self.isArrayEmpty(contact!.connection_details) && contact!.connection_details[0].roomId != nil
        {
            return contact!.connection_details[0].roomId
        }
        
        return contact!.roomId
       
    }
    
    static func getRoomIDCustomContactModel(contact: CustomDataModelContact?)-> String
    {
        if contact == nil{
            return ""
        }
        if contact!.connection_details != nil && contact!.connection_details.count > 0 && contact!.connection_details[0].roomId != nil
        {
            return contact!.connection_details[0].roomId
        }
        
        return contact!.roomId
        
    }

}
