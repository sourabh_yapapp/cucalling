//
//  ICEServer.swift
//  Nehao
//
//  Created by Abhimanu Jindal on 06/03/17.
//  Copyright © 2017 Amit Tripathi. All rights reserved.
//

import Foundation

struct CallICEServer {
    var url:String?
    var userName:String = ""
    var password:String = ""
    var serverType:ICEServerType = .UNKNOWN
    
    enum ICEServerType:Int {
        case STUN
        case TURN
        case UNKNOWN
    }
    
    mutating func encodeToDictionary()->Dictionary<AnyHashable, Any>{
        var dictionary = Dictionary<AnyHashable, Any>()
        dictionary["url"] = self.url
        dictionary["userName"] = self.userName
        dictionary["password"] = self.password
        dictionary["serverType"] = self.serverType.rawValue
        return dictionary
    }
    
    mutating func decodeFromDictionary(dictionary: Dictionary<AnyHashable, Any>){
        self.url = dictionary["url"] as! String?
        self.userName = dictionary["userName"] as! String
        self.password = dictionary["password"] as! String
        let type = dictionary["serverType"] as! Int
        switch type {
        case 0:
            self.serverType = .STUN
        case 1:
            self.serverType = .TURN
        default:
            self.serverType = .UNKNOWN
        }
    }
}
