//
//  YAError.swift
//  YapAppCallingModule
//
//  Created by Abhimanu Jindal on 23/11/16.
//  Copyright © 2016 YapApp India Pvt. Ltd. All rights reserved.
//

import UIKit
import TDResult
public protocol YAErrorProtocol {
    func generateError(description:String?, localizedDescription:String?, debugDescription:String?, code:Int?) -> YAError?
}
public struct YAError {
    
    public enum CodeType:Int {
        case NetworkError
        case WebServiceError
        case DBError
    }
    
    public var localizedDescription:String?
    public var description:String?
    public var debugDescription:String?
    public var code:Int?
    public var codeString:String?
    
    public static func generateError(description:String?, localizedDescription:String?, debugDescription:String?, code:Int?) -> YAError?{
        var error = YAError()
        error.localizedDescription = localizedDescription
        error.description = description
        error.debugDescription = debugDescription
        error.code = code
        return error
    }
    
    public static func generateTDError(error:YAError?)->TDError{
        if error == nil {
            return TDError.init(ApiError.unknown, code: 410, description: "")
        }
       return TDError.init(ApiError.unknown, code: error?.code, description: error?.localizedDescription)
    }
    

}
