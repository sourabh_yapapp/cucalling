//
//  YADataModelConfigurator.swift
//  WebServiceTestApp
//
//  Created by Abhimanu Jindal on 09/02/17.
//  Copyright © 2017 YapApp India Pvt. Ltd. All rights reserved.
//

import UIKit

class YADataModelConfigurator: NSObject {

    static func initConfiguratorWithClient(client:YADataModelConfigurator) -> YADataModelConfigurator{
        return client
    }

    func mapValues(fromModel:AnyObject, toModel:AnyObject){
        fatalError("This function should only be called by  its concrete class")
    }
    
    func mapValuesToDictionary(fromModel:AnyObject)->[String:AnyObject]{
        fatalError("This function should only be called by  its concrete class")
    }
    
}
