//
//  YADataModel.swift
//  YapAppCallingModule
//
//  Created by Abhimanu Jindal on 25/11/16.
//  Copyright © 2016 YapApp India Pvt. Ltd. All rights reserved.
//

import UIKit

protocol YADataModelMapper:class {
    func mapModelToCustomDataModel()
    func mapCustomDataModelToModel()
}

open class YADataModel: NSObject, YADataModelMapper {
    
    var customDataModel:AnyObject?
    
    func mapModelToCustomDataModel(){
        fatalError("This function should only be called by  its concrete class")
    }
    
    func mapCustomDataModelToModel(){
        fatalError("This function should only be called by  its concrete class")
    }
}
