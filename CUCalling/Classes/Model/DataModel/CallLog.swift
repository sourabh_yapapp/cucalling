//
//  CallLog.swift
//  Nehao
//
//  Created by VaneetMoudgill on 03/07/17.
//  Copyright © 2017 Amit Tripathi. All rights reserved.
//

import RealmSwift
import ObjectMapper

enum CallLogType:String {
    case missed, outgoing, incoming, unknown
}

enum CallLogMediaType:String {
    case Audio, Video, unknown
}



@objcMembers class CustomDataModelCallLog: Object, Mappable {
    @objc var id: String  = ""
    @objc var senderId: String = ""
    @objc var receiverId: String = ""
    @objc var callDuration:Double = 0.0
    @objc var callLogType:String = CallLogType.unknown.rawValue.lowercased()
    @objc var callTime:Double = 0.0
    @objc var callMediaType = CallLogMediaType.unknown.rawValue
    @objc var roomId = ""

    
    required convenience init?(map: Map) {
        self.init()
    }
    
    @objc override class func primaryKey() -> String? {
        return "id"
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        senderId <- map["senderId"]
        receiverId <- map["receiverId"]
        callDuration <- map["callDuration"]
        callLogType <- map["callLogType"]
        callTime <- map["callTime"]
        callMediaType <- map["callMediaType"]
        roomId <- map["roomId"]
    }
    
    @objc class func mapJsonData(jsonString: String) -> CustomDataModelCallLog? {
        return Mapper<CustomDataModelCallLog>().map(JSONString: jsonString)
    }
    @objc class func mapJsonDictionaryData(jsonDictionary:[String:AnyObject])->CustomDataModelCallLog? {
        return Mapper<CustomDataModelCallLog>().map(JSON:jsonDictionary)
    }

    @objc class func mapModelData(model:CustomDataModelCallLog)->String{
        return Mapper<CustomDataModelCallLog>().toJSONString(model)!
    }
   
}

//*********-------------------------------***********----------------------------***********************//


class CallLog: YADataModel {
    
    lazy var dataModelConfigurator = YADataModelConfigurator.initConfiguratorWithClient(client: DataModelConfiguratorClient.sharedInstance)
    var id: String  = ""
    var senderId: String = ""
    var receiverId: String = ""
    var callDuration:Double = 0.0
    var callLogType:String = CallLogType.unknown.rawValue.lowercased()
    var callTime:Double = 0.0
    var callMediaType = CallLogMediaType.unknown.rawValue
    var roomId = ""

    
    override init() {
        super.init()
        self.customDataModel = CustomDataModelCallLog()
    }
    
    override func mapModelToCustomDataModel() {
        let dataModel = self.customDataModel! as! CustomDataModelCallLog
        dataModelConfigurator.mapValues(fromModel: self, toModel: dataModel)
    }
    
    override func mapCustomDataModelToModel() {
        let dataModel = self.customDataModel! as! CustomDataModelCallLog
        dataModelConfigurator.mapValues(fromModel: dataModel, toModel: self)
    }
    
}
