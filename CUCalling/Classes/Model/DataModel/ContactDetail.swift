//
//  ContactDetail.swift
//  Nehao
//
//  Created by VaneetMoudgill on 09/02/17.
//  Copyright © 2017 Amit Tripathi. All rights reserved.
//

import UIKit
import RealmSwift
import ObjectMapper

enum GroupImageType:String {
    case auto,manual
}
enum MerchantType:String{
    case merchant = "MERCHANT", department = "DEPARTMENT"
}


@objcMembers class CustomDataModelContactArray: Object, Mappable {
    var contactArray = List<CustomDataModelContact>()
    @objc var id = "-1"
    required convenience init?(map: Map) {
        self.init()
    }

    override class func primaryKey() -> String {
        return "id"
    }
    func mapping(map: Map) {
        var connections = Array<CustomDataModelContact>()
        connections <- map["data"]
        for conn in connections {
            contactArray.append(conn)
        }

    }
    class func mapJsonData(jsonString: String) -> CustomDataModelContactArray? {
        return Mapper<CustomDataModelContactArray>().map(JSONString: jsonString)
    }
    class func mapJsonDictionaryData(jsonDictionary: [String: AnyObject]) -> CustomDataModelContactArray? {
        return Mapper<CustomDataModelContactArray>().map(JSON: jsonDictionary)
    }
}

@objcMembers class CustomDataModelContact: Object, Mappable {
    @objc var remoteThumbImageURL = ""
    @objc var remoteOriginalImageURL = ""
    @objc var isProfilePic = false
    @objc var remoteOriginalImageID = ""
    @objc var connected = false
    @objc var favorite = false
    @objc var smartURL = ""
    @objc var qrCode = ""
    @objc var onlineStatus = false
    @objc var unreadMessagesCount = 0
    @objc var name: String = ""
    @objc var isShowName: Bool = true
    @objc var qrCodeStatus: Bool = false
    @objc var supportUser = false
    @objc var createdAt = 0.0
    @objc var isDeleted = false
    @objc var captureImageName = ""
    @objc var duplicateImage = false
    @objc var localImageURL = ""
    @objc var updatedTimeInterval = 0.0
    var connection_details = List<CustomDataModelConnectionDetail>()
    @objc var savedTemporaryText = ""
    @objc var isGroup = false
    @objc var roomId = ""
    var groupId: CustomDataModelGroupId?
    @objc var sortByTime = 0.0

    @objc override class func primaryKey() -> String {
        return "remoteOriginalImageID"
    }

    required convenience init?(map: Map) {
        self.init()
    }

    func mapping(map: Map) {
        remoteThumbImageURL <- map["thumb"]
        remoteOriginalImageURL <- map["photo_url"]
        isProfilePic <- map["profilePic"]
        remoteOriginalImageID <- map["_id"]
        favorite <- map["favorite"]
        connected <- map["connected"]
        isDeleted <- map["isDeleted"]
        qrCode <- map["qrCode"]
        smartURL <- map["smartURL"]
        createdAt <- map["createdAt"]
        updatedTimeInterval <- map["updatedAt"]
        onlineStatus <- map["onlineStatus"]
        isGroup <- map["isGroup"]
        groupId <- map["groupId"]
        name <- map["name"]
        isShowName <- map["showName"]
        supportUser <- map["supportUser"]
        roomId <- map["roomId"]
        sortByTime <- map["sortByTime"]
//        isMerchant <- map["isMerchant"]
        var connectionDetail = Array<CustomDataModelConnectionDetail>()
        connectionDetail <- map["connection_details"]

        for conn in connectionDetail {
            connection_details.append(conn)
        }


    }
    class func mapJsonData(jsonString: String) -> CustomDataModelContact? {
        return Mapper<CustomDataModelContact>().map(JSONString: jsonString)
    }

    func mapToDictionary() -> [String: AnyObject] {
        var json = Mapper<CustomDataModelContact>().toJSON(self)
        var connectionDetailArray: [[String: AnyObject]] = []
        for (_, conn) in self.connection_details.enumerated() {
            let connectionDetail = conn.mapToDictionary()
            connectionDetailArray.append(connectionDetail)
        }
        json["connection_details"] = connectionDetailArray
        return json as [String: AnyObject]
    }

    class func mapJsonDictionaryData(jsonDictionary: [String: AnyObject]) -> CustomDataModelContact? {
        return Mapper<CustomDataModelContact>().map(JSON: jsonDictionary)
    }
}

@objcMembers class CustomDataModelConnectionDetail: Object, Mappable {
    @objc  var _id = ""
    var userId: CustomDataModelUserId?
    @objc var roomId = ""
    @objc var time = 0.0

    required convenience init?(map: Map) {
        self.init()
    }
   @objc  override class func primaryKey() -> String {
        return "_id"
    }

    func mapping(map: Map) {
        userId <- map["user_id"]
        roomId <- map["roomId"]
        time <- map["time"]
        _id  <- map["_id"]
    }

    func mapToDictionary() -> [String: AnyObject] {
        let json = Mapper<CustomDataModelConnectionDetail>().toJSON(self)
        return json as [String: AnyObject]
    }
}

@objcMembers class CustomDataModelUserId: Object, Mappable {
    @objc var _id = ""
    @objc var deviceName = ""
    @objc var uuid = ""
    @objc var isMerchant = false
    @objc var merchantPhone = ""
    @objc var merchantAddress = ""
    @objc var merchantCode = ""
    @objc var storeUrl = ""
    @objc var isDeleted = false
//    var profilePics = List<CustomDataModelprofilePics>()
    @objc var vpa = ""
    @objc var payeeName = ""
    @objc var authId = ""
    @objc var merchantType = ""
    @objc var isUserDepartment = false

    required convenience init?(map: Map) {
        self.init()
    }
    @objc override class func primaryKey() -> String {
        return "_id"
    }
    func mapping(map: Map) {
        _id <- map["_id"]
        deviceName <- map["deviceName"]
        uuid <- map["uuid"]
        merchantPhone <- map["merchantPhone"]
        merchantAddress <- map["merchantAddress"]
        merchantCode <- map["merchantCode"]
        merchantType <- map["merchantType"]

        if merchantType.lowercased() == MerchantType.department.rawValue.lowercased(){
            isUserDepartment = true
        }else{
            isUserDepartment = false
        }
        
        authId <- map["authId"]
        storeUrl <- map["storeUrl"]
        isDeleted <- map["isDeleted"]
        vpa <- map["vpa"]
        payeeName <- map["payeeName"]
        isMerchant <- map["isMerchant"]
//        var profilePic = Array<CustomDataModelprofilePics>()
        /*profilePic <- map["profilePics"]
        for tempProfilePic in profilePic {
            profilePics.append(tempProfilePic)
        }
        */
    }
}

@objcMembers class CustomDataModelGroupId: Object, Mappable {
    @objc var _id = ""
    @objc var name = ""
    @objc var photoURL = ""
    @objc var smartURL = ""
    @objc var qrCode = ""
    @objc var thumb = ""
    var participantsPhotoId = List<CustomDataModelParticipantPhotoIds>()
    @objc var createdAt = 0.0
    @objc var updatedAt = 0.0
    @objc var groupProfilePicType = GroupImageType.auto.rawValue
    required convenience init?(map: Map) {
        self.init()
    }
    @objc override class func primaryKey() -> String {
        return "_id"
    }
    func mapping(map: Map) {
        _id <- map["_id"]
        name <- map["name"]
        createdAt <- map["createdAt"]
        smartURL <- map["smartURL"]
        photoURL <- map["photo_url"]
        qrCode <- map["qrCode"]
        thumb <- map["thumb"]
        updatedAt <- map["updatedAt"]
        groupProfilePicType <- map["groupProfilePicType"]
        var tempParticipantsId = Array<CustomDataModelParticipantPhotoIds>()
        tempParticipantsId <- map["participantPhotoIds"]
        for participant in tempParticipantsId {
            participantsPhotoId.append(participant)
        }

    }
}

@objcMembers class CustomDataModelParticipantPhotoIds: Object, Mappable {
    @objc var _id = ""
    @objc var isAdmin = false
    var user_id: CustomDataModelUserId?
    required convenience init?(map: Map) {
        self.init()
    }
    @objc override class func primaryKey() -> String {
        return "_id"
    }
    func mapping(map: Map) {
        _id <- map["_id"]
        isAdmin <- map["isAdmin"]
        user_id <- map["user_id"]
    }
}




//-------------------------------**************------------------------------//

class ContactArray: YADataModel {
    lazy var dataModelConfigurator = YADataModelConfigurator.initConfiguratorWithClient(client: DataModelConfiguratorClientNotSignleton())
    var contactArray = [Contact]()
    var id = "-1"

    override init() {
        super.init()
        self.customDataModel = CustomDataModelContactArray()
    }

    override func mapModelToCustomDataModel() {
        let dataModel = self.customDataModel! as! CustomDataModelContactArray
        dataModelConfigurator.mapValues(fromModel: self, toModel: dataModel)
        dataModel.contactArray.removeAll()
        for (_, contact) in self.contactArray.enumerated() {
            contact.mapModelToCustomDataModel()
            dataModel.contactArray.append(contact.customDataModel as! CustomDataModelContact)
        }
    }

    override func mapCustomDataModelToModel() {
        let dataModel = self.customDataModel! as! CustomDataModelContactArray
        dataModelConfigurator.mapValues(fromModel: dataModel, toModel: self)
        self.contactArray.removeAll()
        for (_, contact) in dataModel.contactArray.enumerated() {
            let customContact = Contact()
            customContact.customDataModel = contact
            customContact.mapCustomDataModelToModel()
            self.contactArray.append(customContact)
        }

    }

}


class Contact: YADataModel {
    lazy var dataModelConfigurator = YADataModelConfigurator.initConfiguratorWithClient(client: DataModelConfiguratorClientNotSignleton())
    var remoteThumbImageURL = ""
    var remoteOriginalImageURL = ""
    var isProfilePic = false
    var remoteOriginalImageID = ""
    var connected = Bool()
    var favorite = Bool()
    var smartURL = ""
    var qrCode = ""
    var onlineStatus = Bool()
    var unreadMessagesCount = Int()
    var qrCodeStatus: Bool = false
    var isShowName: Bool = true
    var name: String = ""
    var supportUser = false
    var createdAt = Double()
    var isDeleted = Bool()
    var captureImageName = ""
    var duplicateImage = Bool()
    var localImageURL = ""
    var updatedTimeInterval = Double()
    var connection_details = [ConnectedDetail]()
    var savedTemporaryText = ""
    var isGroup = false
    var roomId = ""
    var groupId = GroupID()
    var sortByTime = 0.0
    

    
    override init() {
        super.init()
        self.customDataModel = CustomDataModelContact()
    }

    override func mapModelToCustomDataModel() {
        let dataModel = self.customDataModel! as! CustomDataModelContact
        dataModelConfigurator.mapValues(fromModel: self, toModel: dataModel)
        dataModel.connection_details.removeAll()
        for (_, connectedDetail) in self.connection_details.enumerated() {
            var customConnectionDetails = ConnectedDetail()
            customConnectionDetails = connectedDetail
            customConnectionDetails.mapModelToCustomDataModel()
            dataModel.connection_details.append(customConnectionDetails.customDataModel as! CustomDataModelConnectionDetail)
        }
        if !dataModel.isGroup {
            return
        }
        self.groupId.customDataModel = dataModel.groupId
        self.groupId.mapModelToCustomDataModel()
    }

    override func mapCustomDataModelToModel() {
        let dataModel = self.customDataModel! as! CustomDataModelContact
        dataModelConfigurator.mapValues(fromModel: dataModel, toModel: self)
        self.connection_details.removeAll()
        for (_, connectionDetail) in dataModel.connection_details.enumerated() {
            let customConnectionDetails = ConnectedDetail()
            customConnectionDetails.customDataModel = connectionDetail
            customConnectionDetails.mapCustomDataModelToModel()
            self.connection_details.append(customConnectionDetails)
        }
        if !self.isGroup {
            return
        }
        self.groupId.customDataModel = dataModel.groupId
        self.groupId.mapCustomDataModelToModel()
    }

    func getUniqueId(forContact contact: Contact) -> String
    {
//        let smartUrl = contact.connection_details[0].userId.profilePics[0].smartURL
        let uniqueId =  ""//smartUrl.lastPathComponent
        return uniqueId
    }
}




class ConnectedDetail: YADataModel {
    lazy var dataModelConfigurator = YADataModelConfigurator.initConfiguratorWithClient(client: DataModelConfiguratorClientNotSignleton())
    var _id = ""
    var roomId = ""
    var time = 0.0
    var userId = UserID()
    
    override init() {
        super.init()
        self.customDataModel = CustomDataModelConnectionDetail()
    }

    override func mapModelToCustomDataModel() {
        let dataModel = self.customDataModel! as! CustomDataModelConnectionDetail
        dataModelConfigurator.mapValues(fromModel: self, toModel: dataModel)
        self.userId.customDataModel = dataModel.userId
        self.userId.mapModelToCustomDataModel()
    }

    override func mapCustomDataModelToModel() {
        let dataModel = self.customDataModel! as! CustomDataModelConnectionDetail
        dataModelConfigurator.mapValues(fromModel: dataModel, toModel: self)
        self.userId.customDataModel = dataModel.userId
        self.userId.mapCustomDataModelToModel()

    }

}
class UserID: YADataModel {
    lazy var dataModelConfigurator = YADataModelConfigurator.initConfiguratorWithClient(client: DataModelConfiguratorClientNotSignleton())
    var _id = ""
    var deviceName = ""
    var uuid = ""
    var isMerchant = false
    var merchantPhone = ""
    var merchantAddress = ""
    var merchantCode = ""
    var storeUrl = ""
    var isDeleted = false
//    var profilePics = [ProfilePics]()
    var vpa = ""
    var payeeName = ""
    var authId = ""
    var merchantType = ""
    var isUserDepartment = false
    
    override init() {
        super.init()
        self.customDataModel = CustomDataModelUserId()
    }


    override func mapModelToCustomDataModel() {
        let dataModel = self.customDataModel as! CustomDataModelUserId
        dataModelConfigurator.mapValues(fromModel: self, toModel: dataModel)
        /*dataModel.profilePics.removeAll()
        for (_, tempProfileData) in self.profilePics.enumerated() {
            let profileData = CustomDataModelprofilePics()//new
            tempProfileData.customDataModel = profileData
            tempProfileData.mapModelToCustomDataModel()
            dataModel.profilePics.append(profileData)
        }
*/
    }

    override func mapCustomDataModelToModel() {
        guard let dataModel = self.customDataModel as? CustomDataModelUserId else {
            return
        }
        dataModelConfigurator.mapValues(fromModel: dataModel, toModel: self)
        /*self.profilePics.removeAll()
        for (_, tempProfileData) in dataModel.profilePics.enumerated() {
            let profileData = ProfilePics()
            profileData.customDataModel = tempProfileData
            profileData.mapCustomDataModelToModel()
            self.profilePics.append(profileData)
        }
        */
    }

}

class GroupID: YADataModel {
    lazy var dataModelConfigurator = YADataModelConfigurator.initConfiguratorWithClient(client: DataModelConfiguratorClientNotSignleton())
    var _id = ""
    var name = ""
    var participantsPhotoId = [ParticipantPhotoIds]()
    var photoURL = ""
    var smartURL = ""
    var thumb = ""
    var qrCode = ""
    var createdAt = 0.0
    var updatedAt = 0.0
    var groupProfilePicType = GroupImageType.auto.rawValue


    override init() {
        super.init()
        self.customDataModel = CustomDataModelGroupId()
    }

    override func mapModelToCustomDataModel() {
        let dataModel = self.customDataModel as! CustomDataModelGroupId
        dataModelConfigurator.mapValues(fromModel: self, toModel: dataModel)
        dataModel.participantsPhotoId.removeAll()
        for (_, tempparticipantsId) in self.participantsPhotoId.enumerated() {
            let tempParticipantsPhotoId = CustomDataModelParticipantPhotoIds()
            tempparticipantsId.customDataModel = tempParticipantsPhotoId
            tempparticipantsId.mapModelToCustomDataModel()
            dataModel.participantsPhotoId.append(tempParticipantsPhotoId)
        }
    }

    override func mapCustomDataModelToModel() {
        guard let dataModel = self.customDataModel as? CustomDataModelGroupId else {
            return
        }
        dataModelConfigurator.mapValues(fromModel: dataModel, toModel: self)
        self.participantsPhotoId.removeAll()
        for (_, tempparticipantsId) in dataModel.participantsPhotoId.enumerated() {
            let tempParticipantsPhotoId = ParticipantPhotoIds()
            tempParticipantsPhotoId.customDataModel = tempparticipantsId
            tempParticipantsPhotoId.mapCustomDataModelToModel()
            self.participantsPhotoId.append(tempParticipantsPhotoId)
        }
    }

    class ParticipantPhotoIds: YADataModel {
        lazy var dataModelConfigurator = YADataModelConfigurator.initConfiguratorWithClient(client: DataModelConfiguratorClientNotSignleton())
        var _id = ""
        var isAdmin = false
        var user_id = UserID()
        override init() {
            super.init()
            self.customDataModel = CustomDataModelParticipantPhotoIds()
        }

        override func mapModelToCustomDataModel() {
            let dataModel = self.customDataModel as! CustomDataModelParticipantPhotoIds
            dataModelConfigurator.mapValues(fromModel: self, toModel: dataModel)
            self.user_id.customDataModel = CustomDataModelUserId()
            self.user_id.mapModelToCustomDataModel()
            dataModel.user_id = self.user_id.customDataModel as? CustomDataModelUserId
        }
        override func mapCustomDataModelToModel() {
            let dataModel = self.customDataModel as! CustomDataModelParticipantPhotoIds
            dataModelConfigurator.mapValues(fromModel: dataModel, toModel: self)
            self.user_id.customDataModel = dataModel.user_id
            self.user_id.mapCustomDataModelToModel()
        }
    }


}

