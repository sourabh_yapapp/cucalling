//
//  DataModelConfiguratorClient.swift
//  WebServiceTestApp
//
//  Created by Abhimanu Jindal on 09/02/17.
//  Copyright © 2017 YapApp India Pvt. Ltd. All rights reserved.
//

import UIKit

@objcMembers class DataModelConfiguratorClient: YADataModelConfigurator {
    
    //MARK:- Variables
 @objc   static let sharedInstance = DataModelConfiguratorClient()

   @objc override func mapValues(fromModel:AnyObject, toModel:AnyObject){
        let fromModelMirror = Mirror(reflecting: fromModel)
        let toModelMirror = Mirror(reflecting: toModel)
        
        let fromChildrenArray = fromModelMirror.children.flatMap{ $0 }
        let toChildrenArray = toModelMirror.children.flatMap{ $0 }
        
        for case let (fromLabel?, fromValue) in fromChildrenArray {
            for case let (toLabel?, toValue) in toChildrenArray {
                if fromLabel == toLabel{
                    let fromType = type(of: fromValue)
                    let toType = type(of: toValue)
                    if fromType != toType{
                        //NOT TO SET VALUE
                    }
                    else{
                        toModel.setValue(fromValue, forKey: fromLabel)
                    }
                    
                }
            }
        }
    }
    
    //NOT TO BE USED CURRENTLY - INSTEAD USE CUSTOMDATAMODEL INBUILT MAPPING
    override func mapValuesToDictionary(fromModel:AnyObject)->[String:AnyObject]{
        let fromModelMirror = Mirror(reflecting: fromModel)
        var dictionary:[String:AnyObject] = [:]
        for case let (fromLabel?, fromValue) in fromModelMirror.children {
            dictionary[fromLabel] = fromValue as AnyObject?
        }
        return dictionary
    }
}

@objcMembers class DataModelConfiguratorClientNotSignleton: YADataModelConfigurator {
    
    //MARK:- Variables
    @objc override func mapValues(fromModel:AnyObject, toModel:AnyObject){
        let fromModelMirror = Mirror(reflecting: fromModel)
        let toModelMirror = Mirror(reflecting: toModel)
        
        let fromChildrenArray = fromModelMirror.children.flatMap{ $0 }
        let toChildrenArray = toModelMirror.children.flatMap{ $0 }
        
        for case let (fromLabel?, fromValue) in fromChildrenArray {
            for case let (toLabel?, toValue) in toChildrenArray {
                if fromLabel == toLabel{
                    let fromType = type(of: fromValue)
                    let toType = type(of: toValue)
                    if fromType != toType{
                        //NOT TO SET VALUE
                    }
                    else{
                        toModel.setValue(fromValue, forKey: fromLabel)
                    }
                    
                }
            }
        }
    }
    
    //NOT TO BE USED CURRENTLY - INSTEAD USE CUSTOMDATAMODEL INBUILT MAPPING
    override func mapValuesToDictionary(fromModel:AnyObject)->[String:AnyObject]{
        let fromModelMirror = Mirror(reflecting: fromModel)
        var dictionary:[String:AnyObject] = [:]
        for case let (fromLabel?, fromValue) in fromModelMirror.children {
            dictionary[fromLabel] = fromValue as AnyObject?
        }
        return dictionary
    }
}




