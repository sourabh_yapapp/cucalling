//
//  CallManager+CallHandshake.swift
//  Nehao
//
//  Created by Tina Gupta on 17/04/17.
//  Copyright © 2017 Amit Tripathi. All rights reserved.
//

import Foundation

extension (CallManager): CallHandshakeDelegate {
    
    //MARK:- Public Method(s)
    func initiateCallHandshake()
    {
//        if currentCallType == .Outgoing{
        if currentCallType == .OngoingOut || currentCallType == .Outgoing{

            callHandshakeManager.startCallHandshake(fromId: currentSenderID!, toId: currentReceiverID!, data: [:], isSender: true)
            invalidateHandshakeTimer()
            initiatePingTimer()
        }
//        if currentCallType == .Incoming{
        if currentCallType == .OngoingIn || currentCallType == .Incoming{
            callHandshakeManager.startCallHandshake(fromId: currentSenderID!, toId: currentReceiverID!, data: [:], isSender: false)
            invalidateHandshakeTimer()
            initiatePingTimer()
        }
    }
    
    func endCallHandshake(){
        invalidateHandshakeTimer()
    }
    //MARK:- Private Method(s)
    private func invalidateHandshakeTimer(){
        if let timer = callHandshakePingTimer{
            timer.invalidate()
            callHandshakePingTimer = nil
        }
    }
    
    private func initiatePingTimer(){
        callHandshakePingTimer = Timer.scheduledTimer(timeInterval: 10, target: self, selector: #selector(noSocketEventRecieved), userInfo: nil, repeats: false)
    }
    
    @objc private func noSocketEventRecieved(){
        
        stopAllAudio()
        updateCallUserControls(localVideo: false, remoteVideo: false, localMic: false, remoteMic: false)
        DispatchQueue.global(qos: .utility).async {
            self.endPeerConnection()
        }
        
        if currentCallType == .Outgoing {
            self.delegate?.didReceiveCallPingTimeOut(type: .PingTimeOutgoing)
        }
        if currentCallType == .Incoming{
            self.delegate?.didReceiveCallPingTimeOut(type: .PingTimeIncoming)
        }
        if currentCallType == .Ongoing || currentCallType == .OngoingIn || currentCallType == .OngoingOut{
            self.delegate?.didReceiveCallPingTimeOut(type: .PingTimeOngoing)
        }
    }

    
    //MARK:- CallHandshake Delegate Method(s)
    func socketCallHandshakeFailed(error:YAError){
        //DON'T HANDLE IT
    }
    
    func didReceiveCallHandshakeRequest(fromID:String?, toID:String?, isSender:Bool){
        if currentSenderID == toID{
            callHandshakeManager.completeCallHandshake(fromId: currentSenderID!, toId: currentReceiverID!, data: [:], isSender: isSender)
        }
    }
    
    func didReceiveCallHandshakeResponse(fromID: String?, toID: String?, isSender:Bool){
        if currentSenderID == toID{
            invalidateHandshakeTimer()
            callHandshakeManager.startCallHandshake(fromId: currentSenderID!, toId: currentReceiverID!, data: [:], isSender: isSender)
            initiatePingTimer()
        }
    }
    
}
