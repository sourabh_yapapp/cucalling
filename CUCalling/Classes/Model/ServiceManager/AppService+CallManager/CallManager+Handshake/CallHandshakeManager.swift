//
//  CallHandshakeManager.swift
//  Nehao
//
//  Created by Tina Gupta on 17/04/17.
//  Copyright © 2017 Amit Tripathi. All rights reserved.
//

import Foundation

protocol CallHandshakeDelegate:class {
    func socketCallHandshakeFailed(error:YAError)
    func didReceiveCallHandshakeRequest(fromID:String?, toID:String?, isSender:Bool)
    func didReceiveCallHandshakeResponse(fromID: String?, toID: String?, isSender:Bool)
}

class CallHandshakeManager: NSObject, YASocketEventDelegate {

    private var socketCallPingHandShake:SocketEventCallPingHandshake?
    weak var delegate:CallHandshakeDelegate?
    
    //MARK:- Overridden Method(S)
    override init(){
        super.init()
        
        let configuration = YAConfiguration.initConfigurationWithClient(client: AppConfigurations())
        let socketAPI = configuration.socketAPIClient()
        socketCallPingHandShake = SocketEventCallPingHandshake()
        socketCallPingHandShake?.socketAPI = socketAPI
        socketCallPingHandShake?.delegate = self
        socketCallPingHandShake?.handleSocketEvents()
        
    }

    func startCallHandshake(fromId: String, toId: String, data: [String:AnyObject], isSender:Bool){
        var customData:[String:AnyObject] = [:]
        customData["senderId"] = fromId as AnyObject?
        customData["receiverId"] = toId as AnyObject?
        if isSender{
            socketCallPingHandShake?.sendPingEvent(fromId: fromId, toId: toId, data: customData, pingHandshakeType: .PingSender)
        }
        else{
            socketCallPingHandShake?.sendPingEvent(fromId: fromId, toId: toId, data: customData, pingHandshakeType: .PingReceiver)
        }
    }
    
    func completeCallHandshake(fromId: String, toId: String, data: [String:AnyObject],isSender:Bool){
        var customData:[String:AnyObject] = [:]
        customData["senderId"] = fromId as AnyObject?
        customData["receiverId"] = toId as AnyObject?
        
        if isSender{
            socketCallPingHandShake?.sendPingEvent(fromId: fromId, toId: toId, data: customData, pingHandshakeType: .PongSender)
        }
        else{
            socketCallPingHandShake?.sendPingEvent(fromId: fromId, toId: toId, data: customData, pingHandshakeType: .PongReceiver)
        }
    }
    
    //MARK:- Private Method(s)
    private func processNewRequest(message:YASocketMessage?, isSender:Bool){
        let message = Utilities.convertJsonStringToDictionary(text: (message?.data)! as! String)
        self.delegate?.didReceiveCallHandshakeRequest(fromID: message?["senderId"] as? String, toID: message?["receiverId"] as? String, isSender: isSender)
    }
    
    
    private func processRequestResponse(message:YASocketMessage?, isSender:Bool){
        let message = Utilities.convertJsonStringToDictionary(text: (message?.data)! as! String)
        self.delegate?.didReceiveCallHandshakeResponse(fromID: message?["senderId"] as? String, toID: message?["receiverId"] as? String, isSender: isSender)
    }

    

    //MARK:- Socket Event Delegate Method(s)
    func socketEventReceived(socketEvent:YASocketEvent){
        let type = socketEvent.socketAPI?.message?.type
        guard type != nil else {
            self.delegate?.socketCallHandshakeFailed(error:YAError.generateError(description: "Ping Error", localizedDescription: "Ping Failed", debugDescription: "NO HANDSHAKE TYPE FOUND", code: YAError.CodeType.NetworkError.hashValue)!)
            return
        }
        let typeValue = type!
        switch typeValue {
        case SocketEventCallPingHandshake.PingHandshakeType.PingSender.rawValue:
            processNewRequest(message:socketEvent.socketAPI?.message, isSender: true)
        case SocketEventCallPingHandshake.PingHandshakeType.PongSender.rawValue:
            processRequestResponse(message:socketEvent.socketAPI?.message, isSender: true)
        case SocketEventCallPingHandshake.PingHandshakeType.PingReceiver.rawValue:
            processNewRequest(message:socketEvent.socketAPI?.message, isSender: false)
        case SocketEventCallPingHandshake.PingHandshakeType.PongReceiver.rawValue:
            processRequestResponse(message:socketEvent.socketAPI?.message, isSender: false)
        default:
            self.delegate?.socketCallHandshakeFailed(error:YAError.generateError(description: "Call Error", localizedDescription: "Call Failed", debugDescription: "NO HANDSHAKE TYPE FOUND", code: YAError.CodeType.NetworkError.hashValue)!)
            
        }
        
    }
    
    func socketEventFailed(socketEvent:YASocketEvent){
        //        self.delegate?.socketHandshakeFailed(error:YAError.generateError(description: "Call Error", localizedDescription: "Call Failed", debugDescription: "NO HANDSHAKE TYPE FOUND", code: YAError.CodeType.NetworkError.hashValue)!)
        
    }

}
