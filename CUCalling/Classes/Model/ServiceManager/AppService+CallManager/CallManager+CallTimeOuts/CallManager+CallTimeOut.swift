//
//  CallManager+CallTimeOuts.swift
//  Nehao
//
//  Created by Abhimanu Jindal on 22/03/17.
//  Copyright © 2017 Amit Tripathi. All rights reserved.
//

extension(CallManager):CallTimeOutManagerDelegate{
    
    func initateCallTimeOut(type:CallManager.CallTimeOutType){
        self.callTimeOutManager.initiateCallTimeOut(type:type)
    }
    
    func cancelAllTimers(){
        self.callTimeOutManager.cancelAllTimers()

        
    }
    
    func cancelCallTimeOut(type: CallManager.CallTimeOutType){
        self.callTimeOutManager.cancelCallTimeOut(type:type)
    }
    
    //MARK:- Delegate Method(s)
    func didReceiveCallTimeOut(type:CallManager.CallTimeOutType){
        self.delegate?.didReceiveCallTimeOut(type: type)
    }
}
