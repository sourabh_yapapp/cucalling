//
//  CallTimeOutManager.swift
//  Nehao
//
//  Created by Abhimanu Jindal on 22/03/17.
//  Copyright © 2017 Amit Tripathi. All rights reserved.
//

import Foundation

protocol CallTimeOutManagerDelegate:class{
    func didReceiveCallTimeOut(type:CallManager.CallTimeOutType)
}

class CallTimeOutManager:NSObject{
    
    weak var delegate:CallTimeOutManagerDelegate?
    private var callTimeOutTimers:[CallManager.CallTimeOutType:Timer] = [:]
    
    func initiateCallTimeOut(type:CallManager.CallTimeOutType){
        
        self.cancelCallTimeOut(type:type)

        DispatchQueue.main.async {
            let timer = Timer.scheduledTimer(timeInterval: TimeInterval(type.rawValue), target: self, selector: #selector(self.didReceiveCallTimeOut), userInfo: ["type":type], repeats: false)
            self.callTimeOutTimers[type] = timer

        }
    }
    
    func cancelCallTimeOut(type:CallManager.CallTimeOutType){
        DispatchQueue.main.async {
            if let timer = self.callTimeOutTimers[type]{
                timer.invalidate()
                self.callTimeOutTimers.removeValue(forKey: type)
            }
        }
    }
    
    func cancelAllTimers(){
        self.cancelCallTimeOut(type:.OutGoingCallNotConnected)
        self.cancelCallTimeOut(type:.OutGoingCallNoAnswer)
        self.cancelCallTimeOut(type:.IncomingGoingCallNoAnswer)
        self.cancelCallTimeOut(type: .NOSDPOfferReceived)
        
    }
    
    @objc private func didReceiveCallTimeOut(timer:Timer){
        let userInfo = timer.userInfo as! [String:AnyObject]
        let timerType = userInfo["type"] as! CallManager.CallTimeOutType
        cancelCallTimeOut(type: timerType)
        
        self.delegate?.didReceiveCallTimeOut(type: timerType)
    }
    
}
