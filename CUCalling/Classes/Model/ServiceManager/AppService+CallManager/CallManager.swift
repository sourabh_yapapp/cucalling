//
//  CallManager.swift
//  Nehao
//
//  Created by Abhimanu Jindal on 01/03/17.
//  Copyright © 2017 Amit Tripathi. All rights reserved.
//

import Foundation


protocol CallManagerDelegate: class {

    //PEER CONNECTION
    func didSetUpPeerConnection()
    func failedSetUpPeerConnection()
    func peerConnectionConnectedSuccessfully()
    func updateMediaStreamOnRtcConnection()
    func updateReconnectingViewOnPeerConnected(isVisible: Bool)
    func audioSessionActivatedByCallKit()

    //Socket Connection
    func didReceiveRequestToUpdateSocket(shouldConnect:Bool)

    
    //CALL INITIATION
    func didReceiveIncomingCallRequest(senderID:String, receiverID:String, userName:String, details:[String:AnyObject], isCallTypeVideo:Bool)
    func shouldInitiateCall(forSenderID senderID:String, receiverID:String, userName:String, details:[String:AnyObject], isCallTypeVideo:Bool,withCOntact contact:Contact)
    
    //CALL OPERATION
    func didReceiveCallOperation(forType operationType : CallOperationType, isMicOrVideoEnabled isEnabled: Bool, callType : CallManager.CallType, isVideoEnabled: Bool, isLocalVideoEnabled:Bool, isPeerConnected:Bool)
    func didSentCallOperation(forType operationType: CallOperationType, forCallType callType:CallManager.CallType?)
    func didSentCallOperationMissedViaCallKit()
    
    func didReceiveCallTimeOut(type: CallManager.CallTimeOutType)
    func didReceiveCallPingTimeOut(type: CallManager.CallPingTimeOutType)

    func didUpdateCurrentCallStatus(type: CallManager.CallCurrentStatus)
    func outgoingCallInitiated(contact: Contact?, isVideo: Bool, isBTConnected: Bool)
    func didUpdateCallMediaType(isVideoEnabled:Bool)
    func didReceiveEndCallState(with callType: CallManager.CallType?)
    func callManager(_ callManager: CallManager, didFinishInsertingLog callLog: CallLog)
    func callManager(_ callManager: CallManager, sendMissedCallLog callLog: CallLog)
    
    func didUpdateVideoViewChangeInBitrate(isSlow: Bool)
    //MEDIA
    func remoteViewDidChangeSize(size: CGSize)

    //AUDIO
    func speakerToggled(isSpeaker: Bool, reason : AudioManager.RouteChangeReason)
    func availableOutputChanged(isAdded:Bool, type: String)
    func availableOutputPortChanged(isBTConnected: Bool)
    
    //Handle Call Timer
    func didUpdateOngoingCallTime()



}

class CallData {
    var callMessage: AnyObject?
    var messageType: String?
}

enum CallOperationType {
    case CallAccepted
    case EndOngoingCall
    case CallControlsVideoUpdated
    case CallControlsMicUpdated
    case UserBusy
    case CallDisconnected
    case ContactDeleted
    case CallOnHold
    case CallResume
    case CallUserEnteredBackground
    case CallUserEnteredForeground
    case CallConnectionRateUpdated
}

class CallManager: NSObject {

    enum CallPendingDictKeys : String {
        case callSessionId = "callSessionId"
        case callSenderId = "callSenderId"
        case callReceiverId = "callReceiverId"
        case requestToConnectToSocket = "requestToConnectToSocket"
        case callControlVideoUpdated = "callControlVideoUpdated"
        case callControlMicUpdated = "callControlMicUpdated"
        case callDisconnected = "callDisconnected"
        case callUsername = "callUsername"
        case isCallTypeVideo = "isCallTypeVideo"

    }
    enum CallType {
        case Outgoing
        case Incoming
        case Ongoing
        case OngoingIn
        case OngoingOut
        case Idle
        case OnHold
    }
    
    enum CallCurrentMediaType {
        case Audio
        case Video
    }
    
    enum CallCurrentStatus: String {
        case Ringing = "Ringing"
        case Connecting = "Connecting"
        case UserBusy = "User Busy"
        case NoAnswer = "No Answer"
        case Incoming = "Incoming"
        case OnHold = "On Hold"
        case Waiting = "Waiting"
        case OtherCallInOngoingCall = "OtherCallInOngoingCall"
        case Disconnecting = "Disconnecting"
        case Rejected = "Rejected"
        case Ongoing = "  "
        case Missed = "Missed call"
        case ContactDeleted = "This connection is no longer active."
        case None = " "

    }

    enum CallTimeOutType: Int {
        case OutGoingCallNotConnected = 60
        case OutGoingCallNoAnswer = 59
        case IncomingGoingCallNoAnswer = 58
        case NOSDPOfferReceived = 17
        case ReceiverConnectedToSocketNotReceived = 20
    }

    enum CallPingTimeOutType {
        case PingTimeOutgoing
        case PingTimeIncoming
        case PingTimeOngoing
    }
    
    enum ConnectionNetworkRange{
        case HighRange
        case MediumRange
        case LowRange
    }
    
    //MARK:- Variables/Constants

    weak var delegate: CallManagerDelegate?
    lazy var isSocketConnected: Bool = false

    lazy var isPeerConnectionSet = false

    lazy var rtcManager = RTCManager()
    lazy var callICEServerManager = CallICEServerManager()
    lazy var callInitiationManager = CallInitiationManager()
    lazy var callSDPManager = CallSDPManager()
    lazy var callICECandidateManager = CallICECandidatesManager()
    lazy var callMediaManager = CallMediaManager()
    lazy var callAudioManager = CallAudioManager()
    lazy var callOperationManager = CallOperationManager()
    lazy var callTimeOutManager = CallTimeOutManager()
    lazy var callHandshakeManager = CallHandshakeManager()

    lazy var internetBitRateManager = InternetBitRateManager()
    
    //CALLKIT MANAGER
    lazy var callkitManager = CallkitManager()


    var currentCallType: CallType = .Idle
    var currentCallState: CallCurrentStatus = .None

    var currentCallMediaType : CallCurrentMediaType = .Audio
    var currentSenderID: String?
    var currentReceiverID: String?
    var currentUserName: String?
    var currentCallerHandle: String! = "CU"
    var currentCallerName: String! = "CU"
    var currentCallerRoomId: String?
    
    var currentCallInitiationDetails: [String: AnyObject]?

    //CALL PUSHEVENTS
    lazy var pushEventTypes: [String] = []
    lazy var pendingPushMessages: [PushMessage] = []

    //lazy var pending new Call - This bool keeps the state if call is started at outgoing end and that time socket was disconnected
    lazy var isCallInititationPendingForSender:Bool = false
    lazy var isCallInititationPendingForReceiver:Bool = false


    //CURRENT MEDIA CONTROLS
    var currentRemoteVideoEnableValue = true
    var currentRemoteMicEnableValue = true
    var currentLocalVideoEnableValue = true
    var currentLocalMicEnableValue = true
    var currentSpeakerEnableValue = false
    var currentSelectedAudioOutputValue: String = ""

    //SDP RELATED LOGIC VARIABLES
    var isSignallingInitiated: Bool = false
    var isRenegotiationInitiated: Bool = true
    var currentSDP: String = ""
    var currentSDPType: String = ""

    //Handling Timer for Ongoing Call
    var ongoingCallTimer: Timer?
    var ongoingCallCurrentTime: Int = 0

    var callHandshakePingTimer: Timer?

    var isCallTypeVideo :Bool = false
    var isSlowNetwork : Bool = false
    var currentCallSessionID : String = ""
    var callPendingDict :[String : AnyObject] = [:]
    var currentConnectionIsSlow: Bool = false
    var connectedUserConnectionIsSlow : Bool = false
    var currentCallLogID : String = ""
    var isOtherUserInBackground = false
    
    var incomingCallhandler : IncomingCallHandler = IncomingCallHandler()
    var callSessionIdHandler :CallSessionIDHandler = CallSessionIDHandler()
    
    //Handling ScreenIdleTimer
    lazy var screenIdleTimerManager = ScreenIdleTimerManager()
    
    //Media values before call
    var prevMediaControlValues :CallMediaControlStateModel = CallMediaControlStateModel()
    var userSelectedOutputPref : CurrentSelectedAudioOutput?
//    let callLogManager : CallLogManager = CallLogManager()
    let ardBitrateManager : ARDBitrateManager = ARDBitrateManager()
    let sharedAudioSession = CallAudioSession()
    var currentNetworkRange: ConnectionNetworkRange = .MediumRange
    
    var views:[YACallMediaView] = []
    let localMediaView = YACallMediaView.init()
    
    override init(){

        super.init()
        setUpPushEvents()
        rtcManager.delegate = self
        callICEServerManager.delegate = self
        callInitiationManager.delegate = self
        callSDPManager.delegate = self
        callICECandidateManager.delegate = self
        callMediaManager.delegate = self
        callOperationManager.delegate = self
        callAudioManager.delegate = self
        callTimeOutManager.delegate = self
        callHandshakeManager.delegate = self
        callkitManager.delegate = self
        internetBitRateManager.delegate = self
        incomingCallhandler.delegate = self
        ardBitrateManager.delegate = self
        sharedAudioSession.configure()

    }

    //MARK:- Public Method(s)

    func cleanUpCallDetails() {
        isCallInProgress = false
        
        callAudioManager.setUpAudioSessionCategory(category: AVAudioSessionCategoryAmbient)
        sharedAudioSession.stop()
        cancelAllTimers()
        endOngoingCallTimer()
        currentCallSessionID = ""
        currentCallType = .Idle
        currentSenderID = nil
        currentReceiverID = nil
        currentUserName = nil
        currentCallerRoomId = nil
        currentCallInitiationDetails = nil
        currentCallState = .None
        self.delegate?.didUpdateCurrentCallStatus(type: .None)

        isSignallingInitiated = false
        isRenegotiationInitiated = true
        currentSDP = ""
        currentSDPType = ""
        
        isCallInititationPendingForSender = false
        isCallInititationPendingForReceiver = false
        currentCallLogID = ""
        print("Log Cleared in cleanup logID \(self.currentCallLogID)")

        currentRemoteVideoEnableValue = true
        currentRemoteMicEnableValue = true
        currentLocalVideoEnableValue = true
        currentLocalMicEnableValue = true
        self.isCallTypeVideo = false
        updateScreenIdleTimer(shouldDisable: true)
        
        var currentNetworkSpeed = self.currentBandwidthSpeed()
        if currentNetworkSpeed == .poor || currentNetworkSpeed == .unavailable
        {
            isSlowNetwork = true
        }
        else
        {
            isSlowNetwork = false
        }
        YALog.print("******** Cleanup ***")
        userSelectedOutputPref = nil
        self.stopBitrateStatsUpdate()
        self.handleScreenIdleTimer(isLocalVideoEnabled: false, isRemoteVideoEnabled: false)
    }

    //MARK:... CALL OPERATION METHOD(S)
    func initiateOutGoingCall(senderID: String, receiverID: String, userName: String, details: [String: AnyObject], isCallTypeVideo: Bool, callerRoomId: String) {
        self.startBitrateStatsUpdate()

        isCallInProgress = true
        callAudioManager.setUpAudioSessionCategory(category: AVAudioSessionCategoryPlayAndRecord)
        var currentNetworkSpeed = self.currentBandwidthSpeed()
        if currentNetworkSpeed == .poor || currentNetworkSpeed == .unavailable
        {

            isSlowNetwork = true
        }
        
        self.isCallTypeVideo = isCallTypeVideo
        
        self.currentSenderID = senderID
        self.currentReceiverID = receiverID
        self.currentUserName = userName
        self.currentCallerRoomId = callerRoomId
        self.currentCallType = .Outgoing
        currentCallSessionID = self.generateUniqueCallSessionId()

        var allDetails = details
        var otherDetails = allDetails["otherDetails"] as! [String:AnyObject]
        otherDetails["isSlowNetwork"] = isSlowNetwork as AnyObject
        otherDetails["callId"] = currentCallSessionID as AnyObject
        
        allDetails["otherDetails"] = otherDetails as AnyObject
        
        self.currentCallInitiationDetails = allDetails
        self.currentCallInitiationDetails?["isVideoCall"] = isCallTypeVideo as AnyObject

        if !isSocketConnected{
            self.delegate?.didReceiveRequestToUpdateSocket(shouldConnect: true)
            isCallInititationPendingForSender = true
            return
        }
        
        startNetworkHandshake()
        playCallAudio(type: .Connecting, shouldToggleSpeakerForVideoCall: isCallTypeVideo)
        self.delegate?.didUpdateCurrentCallStatus(type: .Connecting)
        self.delegate?.didUpdateCallMediaType(isVideoEnabled: isCallTypeVideo)
        
         self.reportCall(hasVideo: isCallTypeVideo, callType: .NewOutgoingStart, currentCallIdentifier: self.currentCallerHandle, currentCallLocalizedName: self.currentCallerName)
        
        DispatchQueue.global(qos: .userInteractive).async{
            self.setUpMediaStream(shouldCreateAudioStream: true, shouldCreateVideoStream: true)
        }
        initateCallTimeOut(type: .OutGoingCallNotConnected)
        updateScreenIdleTimer(shouldDisable: false)

        setSpeakerEnabledValue(isEnabled: isCallTypeVideo)
        
        
    }
    
    func initiateIncomingCall(senderID:String, receiverID:String, userName:String, details:[String:AnyObject], isCallTypeVideo:Bool, callerRoomID: String){
        self.startBitrateStatsUpdate()
        
        isCallInProgress = true
//        AlertCentral.dismissAllPresentedViews()
        self.isCallTypeVideo = isCallTypeVideo
        self.currentSenderID = senderID
        self.currentReceiverID = receiverID
        self.currentCallerRoomId = callerRoomID
        self.currentUserName = userName
        self.currentCallInitiationDetails = details
        self.currentCallType = .Incoming
        
        var currentNetworkSpeed = self.currentBandwidthSpeed()
        if currentNetworkSpeed == .poor || currentNetworkSpeed == .unavailable
        {
            isSlowNetwork = true
        }
        
        var allDetails = details
        
        allDetails["isSlowNetwork"] = isSlowNetwork as AnyObject
        var detailsDict : [String: AnyObject]? = [:]
        
        detailsDict?["otherDetails"] = allDetails as AnyObject
        detailsDict?["callId"] = currentCallSessionID as AnyObject
        
        if !isSocketConnected{
            self.delegate?.didReceiveRequestToUpdateSocket(shouldConnect: true)
            isCallInititationPendingForReceiver = true
            return
        }
      
        self.currentCallInitiationDetails = detailsDict
        completeNetworkHandshake()
    
        if isSlowNetwork
        {
            displayIncomingCallScreen()
        }
        else
        {
            DispatchQueue.global(qos: .userInitiated).async  {
                self.setUpMediaStream(shouldCreateAudioStream: true, shouldCreateVideoStream: true)
            }
        }
        
        
        self.delegate?.didUpdateCurrentCallStatus(type: .Incoming)
        cancelAllTimers()
        initateCallTimeOut(type: .IncomingGoingCallNoAnswer)
        self.delegate?.didUpdateCallMediaType(isVideoEnabled: isCallTypeVideo)
        self.incomingCallhandler.processNextNewPendingmessage()
        setSpeakerEnabledValue(isEnabled: isCallTypeVideo)
        //initateCallTimeOut(type: .NOSDPOfferReceived)
       
    }

    func initiateSignalling() {
        if isRenegotiationInitiated {
            isSignallingInitiated = false
            rtcManager.startSignalling()
        }
            else {
                isSignallingInitiated = true
        }
    }
    
    func setSpeakerEnabledValue(isEnabled: Bool)
    {
        if isEnabled
        {
            if callAudioManager.audioManager.checkIfHeadphonesConnected()
            {
                currentSpeakerEnableValue = false
            }
            else{
                currentSpeakerEnableValue = isEnabled
            }
            return
        }
        
        currentSpeakerEnableValue = isEnabled
    }

    func rtcConnectedSuccessfully() {

        self.delegate?.updateReconnectingViewOnPeerConnected(isVisible: false)

        if isSlowNetwork
        {
            self.delegate?.updateMediaStreamOnRtcConnection()
            self.delegate?.updateReconnectingViewOnPeerConnected(isVisible: false)
             if self.currentCallType == .Incoming
             {
                updateScreenIdleTimer(shouldDisable: false)
            }
            DispatchQueue.main.async {
                self.updateStreamCallViews(view: UIView(), isLocal: true, shouldAdd: true)
            }
            return
        }
 
        if self.currentCallType == .Incoming {
           displayIncomingCallScreen()
//            self.delegate?.peerConnectionConnectedSuccessfully()
//            self.playCallAudio(type: .Incoming)
            updateScreenIdleTimer(shouldDisable: false)
           /// initiateCallHandshake()
        }
        if self.currentCallType == .Outgoing {
            if self.userSelectedOutputPref != nil
            {
                playCallAudio(type: .Ringing)
            }
            else
            {
                playCallAudio(type: .Ringing, shouldToggleSpeakerForVideoCall: isCallTypeVideo)
            }
            self.delegate?.didUpdateCurrentCallStatus(type: .Ringing)
            cancelAllTimers()
            initateCallTimeOut(type: .OutGoingCallNoAnswer)
//            self.reportCall(hasVideo: isCallTypeVideo, callType: .NewOutgoingStart, currentCallIdentifier: self.currentCallerHandle, currentCallLocalizedName: self.currentCallerName)

           // initiateCallHandshake()
        }
    }

    func handleRTCConnectionCloseState() {

    }


    func displayIncomingCallScreen()
    {
        if #available(iOS 10.0, *)
        {
            callAudioManager.setUpAudioSessionCategory(category: AVAudioSessionCategorySoloAmbient)

            self.reportCall(hasVideo: self.isCallTypeVideo, callType: CallkitManager.CallTypeForCallKit.NewIncomingStart, currentCallIdentifier: self.currentCallerHandle, currentCallLocalizedName: self.currentCallerName)
            
        }
        else {
            self.delegate?.peerConnectionConnectedSuccessfully()
            self.playCallAudio(type: .Incoming)
        }
    }


    func endCallInAllStates(with callType: CallManager.CallType?)
    {
//        if self.currentSenderID == user?.userDetails.id
//            {
            if callType == .Outgoing {
                if currentCallLogID.count == 0{
                    print("Missed call inserted for logID \(self.currentCallLogID)")
                    self.currentCallLogID = self.currentCallSessionID
//                    self.insertCallLogIntoDB(withType: currentCallType, isCallbackRequired: true, forCallLog:  nil)
                }
            }
//        }

        if callType == .Ongoing || callType == .OngoingIn || callType == .OngoingOut
            {
                if currentCallLogID.count == 0{
                    self.currentCallLogID = self.currentCallSessionID
//                    self.insertCallLogIntoDB(withType: currentCallType, isCallbackRequired: false, forCallLog:  nil)
                }
        }
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            self.processEndCallOperation(with: callType)
        }

    }

    private func processEndCallOperation(with callType: CallManager.CallType?) {
        self.reportCall(hasVideo: false, callType: CallkitManager.CallTypeForCallKit.EndOngoingCall, currentCallIdentifier: currentCallerHandle, currentCallLocalizedName: currentCallerName)
        updateCallUserControls(localVideo: false, remoteVideo: false, localMic: false, remoteMic: false)
        self.delegate?.didReceiveEndCallState(with: callType)
        self.disableProximitySensor()
        self.cleanUpCallDetails()
    }

    
    func sdpOfferCreatedSuccessfully()
    {
       

//            if currentCallType == .OngoingOut || currentCallType == .Outgoing
//            {
////            self.reportCall(hasVideo: isCallTypeVideo, callType: .NewOutgoingStart, currentCallIdentifier: self.currentCallerHandle, currentCallLocalizedName: self.currentCallerName)
//        
//            }
//            else
//            {
//            displayIncomingCallScreen()
//            }
    }
    
       
    
    func checkIfCallInProgress() -> Bool{
        if currentCallSessionID.characters.count > 0 {
            return true
        }
        return false
    }
    
    //MARK:... SOCKET METHOD(s)
    func socketConnected() {
        isSocketConnected = true
        if currentCallType == .Idle || isCallInititationPendingForSender || isCallInititationPendingForReceiver{
            setUpCallConnection()
        }
        setUpNetworkBitRate()
    }

    func socketDisconnected() {
        isSocketConnected = false
    }

    //MARK:- Private Method(s)
    private func setUpCallConnection() {
        callICEServerManager.setupICEServers()
    }

    private func endCallConnection() {
        callICEServerManager.cleanUpICEServers()
    }

    func updateCallerContact(contact:Contact)
    {
        self.currentCallerName = contact.name
        self.reportCall(hasVideo:  self.isCallTypeVideo, callType: CallkitManager.CallTypeForCallKit.UpdateContactName, currentCallIdentifier:  self.currentCallerHandle, currentCallLocalizedName:  self.currentCallerName)
    }
    

        //Setup Callkit Handle and Idetifier
    func setupCallKitHandle(handle: String, name: String)
    {
        self.currentCallerHandle = handle
        self.currentCallerName = name
    }
    
    func callLogRecieved(callLog:CallLog){
        if callLog.callLogType.lowercased() == CallLogType.missed.rawValue.lowercased()
        {
//            self.insertCallLogIntoDB(withType: .Incoming, isCallbackRequired: false, forCallLog:  callLog)
        }
    }
    
    /*
    //XTRA METHODS
    
    func addViewOnWindow()
    {
        let label = UILabel(frame: CGRect(x: 50, y: 100, width: 100, height: 50))
        label.tag = 102310
        label.backgroundColor = UIColor.black
        label.textColor = UIColor.white
        label.text = "hi"

        UIApplication.shared.delegate?.window!?.addSubview(label)
    }
    
    func updateWindowLabel(text: String)
    {
        DispatchQueue.main.async {
            for subView in (UIApplication.shared.delegate?.window!?.subviews)!
            {
                if subView.tag == 102310
                {
                    let label = subView as! UILabel
                    
                    label.textColor = UIColor.white
                    label.text = text
            }
            }

        }
        
    }
    
 */
    
}
