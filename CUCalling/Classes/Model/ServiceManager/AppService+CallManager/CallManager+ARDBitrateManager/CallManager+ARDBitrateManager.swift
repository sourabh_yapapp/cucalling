//
//  CallManager+ARDBitrateManager.swift
//  Nehao
//
//  Created by Tina Gupta on 08/06/17.
//  Copyright © 2017 Amit Tripathi. All rights reserved.
//

import UIKit

extension (CallManager): ARDBitrateManagerDelegate
{
    
    func startBitrateStatsUpdate()
    {
        self.ardBitrateManager.shouldTrackBitrateStats(shouldGet: true, withPeerConnection: self.rtcManager.peerConnection)
    }
    
    func stopBitrateStatsUpdate()
    {
        self.ardBitrateManager.shouldTrackBitrateStats(shouldGet: false, withPeerConnection: nil)
    }
    
    func checkNetworkRangeIsPoor() -> Bool
    {
        if self.ardBitrateManager.getCurrentNteworkRange() == .LowRange
        {
            return true
        }
        return false
    }
    
    func didCheckNetworkRangeOnAudioVideoSwitching(isLocalVideoEnabled:Bool, isRemoteVideoEnabled:Bool)
    {
        if isLocalVideoEnabled
        {
            if self.ardBitrateManager.getCurrentNteworkRange() == .LowRange
                {
//                    AlertCentral.displayOkAlertOnWindow(withTitle: "CU", message: "You cannot switch to video call due to slow network.")
                }
        }
    }
    
    func didGetNewUpdate(rangeString: String, range : ARDBitrateManager.ConnectionNetworkRange)    {
        let connRange : CallManager.ConnectionNetworkRange!
        
        switch range {
        case .LowRange:
            connRange = .LowRange
        case .MediumRange:
            connRange = .MediumRange
        case .HighRange:
            connRange = .HighRange
        default:
            print("")
        }
        self.currentNetworkRange = connRange
//        self.updateWindowLabel(text: range)
    }
    
    func checkNetworkRate(){
        let connRange : ARDBitrateManager.ConnectionNetworkRange!

        switch self.currentNetworkRange {
        case .LowRange:
            connRange = .LowRange
        case .MediumRange:
            connRange = .MediumRange
        case .HighRange:
            connRange = .HighRange
        default:
            print("")
        }
        self.didChangeNetworkBitrateRange(withNewRange: connRange)
    }
    
    func didChangeNetworkBitrateRange(withNewRange newRange: ARDBitrateManager.ConnectionNetworkRange)
    {
        YALog.print("NEW RANGE \(newRange)")
        if newRange == .LowRange{
            self.callConnectionRateUpdated(isSlow: true)
        }
        else if newRange == .MediumRange
        {
            self.callConnectionRateUpdated(isSlow: false)
        }
        else {
            self.callConnectionRateUpdated(isSlow: false)
        }
    }
}
