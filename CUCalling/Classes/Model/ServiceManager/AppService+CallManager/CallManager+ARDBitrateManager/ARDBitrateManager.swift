//
//  ARDBitrateManager.swift
//  Nehao
//
//  Created by Tina Gupta on 08/06/17.
//  Copyright © 2017 Amit Tripathi. All rights reserved.
//

import Foundation
import WebRTC

protocol ARDBitrateManagerDelegate: class
{
    func didChangeNetworkBitrateRange(withNewRange newRange: ARDBitrateManager.ConnectionNetworkRange)
    //    func didGetNewUpdate(range: String)
    func didGetNewUpdate(rangeString: String, range : ARDBitrateManager.ConnectionNetworkRange)
}


class ARDBitrateManager
{
    
    enum ConnectionNetworkRange{
        case HighRange
        case MediumRange
        case LowRange
    }
    
    private var statsTimerProxy : ARDTimerProxy = ARDTimerProxy()
    //private var highSpeedNetworkRange: Double = 800 //in Kbps

    private var highNetworkrange: Double = 599 //in Kbps
    
    private var mediumNetworkrange: Double = 1000 //in Kbps
    private var lowNetworkrange: Double = 1001
    private var peerConnection : RTCPeerConnection?
    private var statsBuilder = ARDStatsBuilder()
    private var prevConnectionSendRange: ConnectionNetworkRange?
    private var newConnectionrange :ConnectionNetworkRange?
    private var notifiedConnectionRange :ConnectionNetworkRange?
    
    weak var delegate:ARDBitrateManagerDelegate?
    private var statsTimer : Timer?
    private var updateTimer : Timer?
    
    
    func shouldTrackBitrateStats(shouldGet :Bool, withPeerConnection peerConnection: RTCPeerConnection?)
    {
        
        if shouldGet
        {
            self.peerConnection = peerConnection
            startGettingStats()
        }
        else{
            stopGettingStats()
            self.prevConnectionSendRange = nil
            self.newConnectionrange = nil
            self.notifiedConnectionRange = nil
            self.peerConnection = nil
        }
    }
    
    func defineNetworkRange(minimumHigh highSpeed : Double, minimumMedium mediumSpeed : Double, minimumLow lowSpeed : Double)
    {
        //highSpeedNetworkRange = highSpeed
        mediumNetworkrange = mediumSpeed
        lowNetworkrange = lowSpeed
    }
    func getCurrentNteworkRange() -> ConnectionNetworkRange
    {
        return self.getNetworkRange(connectionRTT: statsBuilder.connectionRTTTime)
    }
    
    //MARK:- Private Methods
    
    private func startGettingStats()
    {
        DispatchQueue.main.async {
            self.statsTimer = Timer.scheduledTimer(withTimeInterval: 0.1, repeats: true, block: { (timer) in
                self.getPeerConnectionStats()
            })
        }
        
    }
    
    private func getPeerConnectionStats()
    {
        if self.peerConnection != nil
        {
            self.peerConnection!.stats(for: nil, statsOutputLevel: RTCStatsOutputLevel.standard, completionHandler:     { (legacyReportArray) in
                self.didGetStats(statsArray: legacyReportArray)
            })
        }
    }
    
    private func stopGettingStats()
    {
        statsBuilder = ARDStatsBuilder()
        stopstatsTimerProxy()
    }
    
    private func stopstatsTimerProxy()
    {
        statsTimer?.invalidate()
    }
    
    @objc private func didGetStats(statsArray : [RTCLegacyStatsReport])
    {
        for report in statsArray
        {
            statsBuilder.parseStatsReport(report )
        }
        
        YALog.print(" \(statsBuilder.statsString)")
        let newRange = getNetworkRange(connectionRTT: statsBuilder.connectionRTTTime)
        
        self.delegate?.didGetNewUpdate(rangeString: String(format: "%f", statsBuilder.connectionRTTTime), range: newRange)
        self.setNewConnectionNetworkRange(connectionSendBitrate: statsBuilder.connectionRTTTime)
    }
    
    
    private func setNewConnectionNetworkRange(connectionSendBitrate: Double)
    {
        if connectionSendBitrate == 0{return}
        var newRange = getNetworkRange(connectionRTT: connectionSendBitrate)
        checkForSignificantChangeInRange(with: newRange)
    }
    
    private func getNetworkRange(connectionRTT: Double)-> ConnectionNetworkRange{
        var newRange: ConnectionNetworkRange = .LowRange
        
        if connectionRTT > 0
        {
            if connectionRTT < highNetworkrange
            {
                newRange = .HighRange
                return newRange
            }
            else if connectionRTT < mediumNetworkrange
            {
                newRange = .MediumRange
                return newRange
            }
            else
            {
                newRange = .LowRange
                return newRange
                
            }
        }
        else{
            newRange = .LowRange
            return newRange
        }
        return newRange
    }
    
    private func checkForSignificantChangeInRange(with newRange :ConnectionNetworkRange)
    {
        if prevConnectionSendRange == nil{
            self.prevConnectionSendRange = newRange
            self.delegate?.didChangeNetworkBitrateRange(withNewRange: newRange)
        }
        else
        {
            if updateTimer == nil
            {
                startUpdateStatusNotifier()
            }
            newConnectionrange = newRange
        }
        
    }
    
    private func rangeDidChange(newRange: ConnectionNetworkRange)
    {
        notifiedConnectionRange = newRange
    }
    
    private func startUpdateStatusNotifier()
    {
        DispatchQueue.main.async {
            
            if self.prevConnectionSendRange != self.newConnectionrange
            {
                self.prevConnectionSendRange = self.newConnectionrange
                if  self.prevConnectionSendRange == nil{
                            return
                    }
                    
                
                    if let prevConnSendRange = self.prevConnectionSendRange
                    {
                        self.delegate?.didChangeNetworkBitrateRange(withNewRange: prevConnSendRange)
                        self.invalidateTimer()
                    }
                    
                }
        }
    }
    
    private func invalidateTimer()
    {
        updateTimer?.invalidate()
        updateTimer = nil
    }
    
}
