//
//  ARDTimerProxy.h
//  Nehao
//
//  Created by Tina Gupta on 08/06/17.
//  Copyright © 2017 Amit Tripathi. All rights reserved.
//

#import <Foundation/Foundation.h>

// We need a proxy to NSTimer because it causes a strong retain cycle. When
// using the proxy, |invalidate| must be called before it properly deallocs.
@interface ARDTimerProxy : NSObject

- (instancetype)initWithInterval:(NSTimeInterval)interval
                         repeats:(BOOL)repeats
                    timerHandler:(void (^)(void))timerHandler;
- (void)invalidate;

@end

