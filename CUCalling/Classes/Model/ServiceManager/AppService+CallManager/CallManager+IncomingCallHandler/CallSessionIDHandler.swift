//
//  CallSessionIDHandler.swift
//  Nehao
//
//  Created by Tina Gupta on 12/06/17.
//  Copyright © 2017 Amit Tripathi. All rights reserved.
//

import UIKit

class CallSessionIDHandler: NSObject {
    private var callSessionId :String = ""
    private var cacheArray = [String?](repeating: nil, count: 5)
    
    private func insertCallId(callId: String)
    {
        if cacheArray.count == 5
        {
            cacheArray.remove(at: 0)
        }
        cacheArray.append(callId)
    }
    
    func checkCallIdExists(callId: String)-> Bool
    {
        let filteredArray = cacheArray.filter{$0 == callId}
        
        if filteredArray.count > 0
        {
            return false
        }
        else{
            self.insertCallId(callId: callId)
            return true
        }
        
    }
    
    
}
