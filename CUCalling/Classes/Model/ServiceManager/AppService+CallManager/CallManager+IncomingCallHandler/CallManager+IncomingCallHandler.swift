//
//  CallManager+IncomingCallHandler.swift
//  Nehao
//
//  Created by Tina Gupta on 09/06/17.
//  Copyright © 2017 Amit Tripathi. All rights reserved.
//

import UIKit

extension (CallManager) : IncomingCallHandlerDelegate
{
    func shouldInitiateCall(withContact contact: Contact, senderID: String, receiverID: String, userName: String, details: [String : AnyObject], isCallTypeVideo: Bool) {
       self.delegate?.shouldInitiateCall(forSenderID: senderID, receiverID: receiverID, userName: userName, details: details, isCallTypeVideo: isCallTypeVideo,withCOntact: contact)
    }
    
    func sendOperationBusyToUser(senderID: String, receiverID: String, userName: String, details: [String : AnyObject], isCallTypeVideo: Bool) {

    }
    
    func sendOperationContactDeleted(senderID: String, receiverID: String)
    {
        self.updateCallOperation(operationType: .ContactDeleted, isVideoEnabled: nil, isMicEnabled: nil, senderId: receiverID, receiverId: senderID, callType: nil)

    }
    
    func shouldExecuteSocketOperation(type: String?, data: [String: AnyObject]?)
    {
        guard let socketOperationType = type else{return}
        if data == nil {return}
        
        DispatchQueue.global().async {
            if socketOperationType == IncomingCallHandler.poolCallOperation.NewCallOperation.rawValue
            {
                self.handleNewCall(data: data!)
            }
            else{
            self.executeSocketOperation(type: type, data: data)
            }
        }
    }
    
    func handleNewCall(data : [String:AnyObject])
    {
        if let callSessionId = data["callId"] as? String{
            self.currentCallSessionID = callSessionId
        }
        if let receiverId = data["receiverId"] as? String, let senderId = data["senderId"] as? String, let userName = data["userName"] as? String,let details = data["details"] as? [String : AnyObject], let callTypeVideo = data["isCallTypeVideo"] as? Bool{
            self.incomingCallhandler.handleIncomingCall(senderID: senderId, receiverID:  receiverId, userName:  userName, details:  details, isCallTypeVideo:  callTypeVideo)

        }
    }
    
}

