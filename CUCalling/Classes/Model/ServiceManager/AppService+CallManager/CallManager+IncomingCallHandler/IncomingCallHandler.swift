//
//  IncomingCallHandler.swift
//  Nehao
//
//  Created by Tina Gupta on 09/06/17.
//  Copyright © 2017 Amit Tripathi. All rights reserved.
//

import UIKit

protocol IncomingCallHandlerDelegate :class{
    func shouldInitiateCall(withContact contact:Contact, senderID:String, receiverID:String, userName:String, details:[String:AnyObject], isCallTypeVideo:Bool)
    func sendOperationBusyToUser(senderID:String, receiverID:String, userName:String, details:[String:AnyObject], isCallTypeVideo:Bool)
    func sendOperationContactDeleted(senderID: String, receiverID: String)
    func shouldExecuteSocketOperation(type: String?, data: [String: AnyObject]?)
}


class IncomingCallHandler: NSObject {

    weak var delegate :IncomingCallHandlerDelegate?
//    lazy var contactDBServiceManager = DBServiceManagerContactsRealm()
    var isCallOperationExecuting = false
    var pendingOperationsArray = [AnyObject]()
    var tempOperationArray = [AnyObject]()
    var isCallPoolExecuting = false
   public enum poolCallOperation:String{
        case NewCallOperation = "NewCallOperation"
        case CallDisconnected = "CallDisconnected"

    }
    
    func handleIncomingCall(senderID:String, receiverID:String, userName:String, details:[String:AnyObject], isCallTypeVideo:Bool)
    {
        let customContact = CustomDataModelContact.mapJsonDictionaryData(jsonDictionary: details)
        let contact = Contact()
        contact.customDataModel = customContact
        contact.mapCustomDataModelToModel()
        
        isCallOperationExecuting = true
        /*
        contactDBServiceManager.callDBService(data: CustomDataModelContact(), methodType: .FETCH, queryString: "isDeleted == false AND connected == true", sortQueryString: nil, sortType: nil) { (resultData, error) in
            let tempResult = resultData as? [CustomDataModelContact]
            if (tempResult?.count == nil ? -1:(tempResult?.count)!) > 0 {
//                let filteredContactArray =  tempResult?.filter { $0.connection_details.first?.roomId == contact.connection_details.first?.roomId }
                let filteredContactArray =  tempResult?.filter { Utilities.getRoomIDCustomContactModel(contact: $0) == Utilities.getRoomID(contact:contact)  }
                if (filteredContactArray?.count == nil ? -1:(filteredContactArray?.count)!) > 0 {
                    let tempContact = Contact()
                    tempContact.customDataModel = filteredContactArray?[0]
                    tempContact.mapCustomDataModelToModel()
                    
                    var shouldReject = true
                    if (tempResult?.count == nil ? -1:(tempResult?.count)!) > 0 {
                        for singleContact in tempResult! {
                            if(singleContact.isDeleted){
                                continue;
                            }
                            if(singleContact.connection_details.first?.userId?._id == senderID){
                                shouldReject = false
                                break;
                            }
                        }
                    }
                    if(shouldReject || ((contactBeingDeleted != nil) && tempContact.connection_details.first?.userId._id == contactBeingDeleted?.connection_details.first?.userId._id)){
                        //send contact deleted
                        self.delegate?.sendOperationContactDeleted(senderID: senderID, receiverID: receiverID)
                        self.isCallOperationExecuting = false
                        DispatchQueue.global().asyncAfter(deadline: .now() + 1 , execute: {
                            self.mergeTempArrayIntoPendingDict()
                            self.deletePendingOperationForDeletedContact(callId: details["callId"] as? String)

                        })
                        
                    }else{
                        ///continue with call
                        self.delegate?.shouldInitiateCall(withContact: tempContact, senderID: senderID, receiverID: receiverID, userName: userName, details: details, isCallTypeVideo: isCallTypeVideo)
                        self.isCallOperationExecuting = false
                        self.mergeTempArrayIntoPendingDict()

                    }
                    
                }else{
                    self.delegate?.sendOperationContactDeleted(senderID: senderID, receiverID: receiverID)
                    self.isCallOperationExecuting = false
                    DispatchQueue.global().asyncAfter(deadline: .now() + 1 , execute: {
                        self.mergeTempArrayIntoPendingDict()
                        self.deletePendingOperationForDeletedContact(callId: details["callId"] as? String)

                    })
                }
            }else{
                self.delegate?.sendOperationContactDeleted(senderID: senderID, receiverID: receiverID)
                self.isCallOperationExecuting = false
                DispatchQueue.global().asyncAfter(deadline: .now() + 1 , execute: {
                    self.mergeTempArrayIntoPendingDict()
                    self.deletePendingOperationForDeletedContact(callId: details["callId"] as? String)

                })
            }
        }
        */
    }
    
    //MARK:- Handling Socket Operations
    
    func handleSocketOperation(type: String?, data: [String: AnyObject]?)
    {
        if isCallOperationExecuting{
            saveDataInTempDict(type: type, data: data)
        }
        else{
            saveDataInPendingDict(type: type, data: data)
        }
        
    }
    private func processPendingCallOperations(){
        if pendingOperationsArray.count == 0{
            isCallPoolExecuting = false
            return
        }
        if !isCallPoolExecuting {
            isCallPoolExecuting = true
            DispatchQueue.main.async {
            if self.pendingOperationsArray.count > 0{
                let operationType = (self.pendingOperationsArray[0] as? [String:AnyObject])?["type"] as? String
                if let opData = (self.pendingOperationsArray[0] as? [String:AnyObject]){
                    
                    if let operationData = opData["data"] as? [String: AnyObject]{
                            if self.pendingOperationsArray.count > 0 {
                                self.pendingOperationsArray.remove(at: 0)
                            }
                        self.delegate?.shouldExecuteSocketOperation(type: operationType, data: operationData)
                    }
                }
            }

            }
        }
    }
    
    
     func processNextNewPendingmessage() {
        
        isCallPoolExecuting = false
        processPendingCallOperations()
    }
    
    
    private func saveDataInTempDict(type: String?, data: [String: AnyObject]?)
    {
        let pendingDict : [String: AnyObject] = ["type": type as AnyObject, "data" : data as AnyObject]
        tempOperationArray.append(pendingDict as AnyObject)
    }
    
    private func saveDataInPendingDict(type: String?, data: [String: AnyObject]?)
    {
        checkAndDeleteNewCallIfDisconnectReceived(type: type, data: data)
        let pendingDict : [String: AnyObject] = ["type": type as AnyObject, "data" : data as AnyObject]
        pendingOperationsArray.append(pendingDict as AnyObject)
        YALog.print("**************** Pending Array Updated \(pendingOperationsArray)")

        processPendingCallOperations()
    }
    
    private func mergeTempArrayIntoPendingDict()
    {
        for operation in tempOperationArray{
        pendingOperationsArray.append(operation)
        }
        tempOperationArray.removeAll()
        processPendingCallOperations()
    }
    

    func addInitiateCallOperationToQueue(senderID:String, receiverID:String, userName:String, details:[String:AnyObject], isCallTypeVideo:Bool, callId: String)
    {
        let operationType = poolCallOperation.NewCallOperation.rawValue
        let operationData = ["senderId": senderID, "receiverId": receiverID, "userName": userName, "isCallTypeVideo":isCallTypeVideo, "details": details, "callId": callId] as [String : Any]
        handleSocketOperation(type: operationType, data: operationData as [String : AnyObject])
    }
    
    func insertDisconnectOperationInQueue(senderID:String, receiverID:String, userName:String, details:[String:AnyObject], isCallTypeVideo:Bool, callId: String)
    {
        let operationType = poolCallOperation.CallDisconnected.rawValue
        let operationData = ["senderId": senderID, "receiverId": receiverID, "userName": userName, "isCallTypeVideo":isCallTypeVideo, "callId": callId] as [String : Any]
        handleSocketOperation(type: operationType, data: operationData as [String : AnyObject])
    }
    
    
    private func checkAndDeleteNewCallIfDisconnectReceived(type: String?, data: [String: AnyObject]?)
    {
        if type == poolCallOperation.CallDisconnected.rawValue
        {
            let callID = data?["callId"] as? String
            var filteredOperationArray = pendingOperationsArray.filter{(($0["data"] as? [String:AnyObject])!["callId"] as? String) != callID
            }

            YALog.print("**************** Old Array \(pendingOperationsArray)")
           pendingOperationsArray = filteredOperationArray
            YALog.print("**************** New Array \(pendingOperationsArray)")

        }
        
    }

    
    //For handling when user calls immidiately after making new connection and connection is not made yet on receiver's end
    private func deletePendingOperationForDeletedContact(callId: String?)
    {
        
        let filteredOperationArray = pendingOperationsArray.filter{(($0["data"] as? [String:AnyObject])!["callId"] as? String) != callId
        }
        
        YALog.print("**************** Old Array \(pendingOperationsArray)")
        pendingOperationsArray = filteredOperationArray
        YALog.print("**************** New Array \(pendingOperationsArray)")
        processPendingCallOperations()
    }
    
    func clearPendingCallOperationArray()
    {
        DispatchQueue.main.async {
            self.isCallPoolExecuting = false
            self.pendingOperationsArray.removeAll()
            self.isCallOperationExecuting = false
            self.tempOperationArray.removeAll()
        }
       
    }
    
}
