//
//  CallManager+OngoingCallTimeHandler.swift
//  Nehao
//
//  Created by Tina Gupta on 12/04/17.
//  Copyright © 2017 Amit Tripathi. All rights reserved.
//

extension(CallManager){
    
    func initiateOngoingCallTimer()
    {
        endOngoingCallTimer()
        DispatchQueue.main.async {
        self.ongoingCallTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(self.updateTimer), userInfo: nil, repeats: true)
        self.ongoingCallCurrentTime = 0
        }
    }
    
    //MARK: Private Methods
    @objc private func updateTimer()
    {
        self.delegate?.didUpdateOngoingCallTime()
    }
    
    func endOngoingCallTimer(){
        self.ongoingCallCurrentTime = 0
        if let timer = ongoingCallTimer{
            timer.invalidate()
            ongoingCallTimer = nil
            
        }
    }
    
}

