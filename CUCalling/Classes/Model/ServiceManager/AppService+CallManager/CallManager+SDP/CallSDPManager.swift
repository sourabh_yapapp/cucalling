//
//  CallManager.swift
//  Nehao
//
//  Created by Abhimanu Jindal on 01/03/17.
//  Copyright © 2017 Amit Tripathi. All rights reserved.
//

import Foundation

protocol CallSDPManagerDelegate:class {
    func didReceiveSDP(sdp:String, type:String)
    func failedToParseReceivedSDP(error:YAError)
}


class CallSDPManager: NSObject, YASocketEventDelegate {
    
    //MARK:- Variables/ Constants
    weak var delegate:CallSDPManagerDelegate?
    private var socketEventSDP:SocketEventSDP?
    
    
    //MARK:- Private Method(s)
    override init() {
        super.init()
        let configuration = YAConfiguration.initConfigurationWithClient(client: AppConfigurations())
        let socketAPI = configuration.socketAPIClient()
        socketEventSDP = SocketEventSDP()
        socketEventSDP?.socketAPI = socketAPI
        socketEventSDP?.delegate = self
        socketEventSDP?.handleSocketEvents()
    }
    
    
    //MARK:- Public Method(s)
    func sendSDPToServer(fromId: String, toId: String, data: [String:AnyObject], isOffer:Bool){
        if isOffer{
            socketEventSDP?.sendOfferSDPEvent(fromId: fromId, toId: toId, data: data)
            return
        }
        socketEventSDP?.sendAnswerSDPEvent(fromId: fromId, toId: toId, data: data)
    }
    
    //MARK:- Socket Event SDP Delegate Method(S)
    func socketEventReceived(socketEvent:YASocketEvent){
        var jsonResult:Any?
        let data = socketEvent.socketAPI?.message?.data
        guard data != nil else {
            self.delegate?.failedToParseReceivedSDP(error: YAError.generateError(description: "Call Error", localizedDescription: "Call Error", debugDescription: "Received event has no data - \(String(describing: socketEvent.socketAPI?.message))", code: YAError.CodeType.NetworkError.hashValue)!)
            return
        }
        if let jsonString = data! as? String {
            jsonResult = Utilities.convertJsonStringToDictionary(text: jsonString)
        }
        let resultData = jsonResult
        let result = resultData as! [String:String]
        
        if result["sdpDescription"] == nil {
            self.delegate?.failedToParseReceivedSDP(error: YAError.generateError(description: "Call Error", localizedDescription: "Call Error", debugDescription: "SDP Response is not correct - \(String(describing: socketEvent.socketAPI?.message))", code: YAError.CodeType.NetworkError.hashValue)!)
            return
        }
        if result["sdpType"] == nil {
            YALog.print("Socket SDP Type Response is empty")
            self.delegate?.failedToParseReceivedSDP(error: YAError.generateError(description: "Call Error", localizedDescription: "Call Error", debugDescription: "SDP Type Response is empty - \(String(describing: socketEvent.socketAPI?.message))", code: YAError.CodeType.NetworkError.hashValue)!)
            
            return
        }
        self.delegate?.didReceiveSDP(sdp: result["sdpDescription"]!, type: result["sdpType"]!)
        
    }
    
    func socketEventFailed(socketEvent:YASocketEvent){
        self.delegate?.failedToParseReceivedSDP(error: YAError.generateError(description: "Call Error", localizedDescription: "Call Error", debugDescription: "SDP Socket Event Errpr \(String(describing: socketEvent.socketAPI?.message))", code: YAError.CodeType.NetworkError.hashValue)!)
    }

    
}
