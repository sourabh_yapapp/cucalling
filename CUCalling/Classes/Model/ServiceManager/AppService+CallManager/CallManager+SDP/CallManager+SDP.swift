//
//  CallManager.swift
//  Nehao
//
//  Created by Abhimanu Jindal on 01/03/17.
//  Copyright © 2017 Amit Tripathi. All rights reserved.
//

extension (CallManager):CallSDPManagerDelegate {
    
    func sendSDPToServer(fromId: String, toId: String, data: [String:AnyObject], isOffer:Bool){
        callSDPManager.sendSDPToServer(fromId: fromId, toId: toId, data: data, isOffer: isOffer)
    }
    
    func didReceiveSDP(sdp:String, type:String){
        //        if self.currentCallType == .Incoming && type == "offer"{

        if type == "offer" && (self.currentCallType == .Incoming || self.currentCallType == .OngoingIn){
            //cancelAllTimers()
            if isRenegotiationInitiated{
                isSignallingInitiated = false
                rtcManager.processSDP(sdpDescription: sdp, sdpType: type)
                return
            }
            else{
                isSignallingInitiated = true
                currentSDP = sdp
                currentSDPType = type
            }
        }
        //        if self.currentCallType == .Outgoing && type == "answer"{

        if type == "answer" && (self.currentCallType == .Outgoing || self.currentCallType == .OngoingOut){
            if isRenegotiationInitiated{
                isSignallingInitiated = false
                rtcManager.processSDP(sdpDescription: sdp, sdpType: type)
                return
            }
            else{
                isSignallingInitiated = true
                currentSDP = sdp
                currentSDPType = type
            }
        }
    }
    
    func failedToParseReceivedSDP(error:YAError){
        
    }
}
