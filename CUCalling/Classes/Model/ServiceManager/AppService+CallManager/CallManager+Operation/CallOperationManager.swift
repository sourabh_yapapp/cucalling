//
//  CallManager.swift
//  Nehao
//
//  Created by Abhimanu Jindal on 01/03/17.
//  Copyright © 2017 Amit Tripathi. All rights reserved.
//

import Foundation

protocol CallOperationManagerDelegate:class {
    func didReceiveCallSocketOperation(type:String?, data:[String:AnyObject]?)
    func failedToReceiveCallOperation(error:YAError?)
}

class CallOperationManager: NSObject, YASocketEventDelegate {
    
    weak var delegate:CallOperationManagerDelegate?
    
    private var socketEventCallOperation:SocketEventCallOperations?
    
    override init(){
        super.init()
        let configuration = YAConfiguration.initConfigurationWithClient(client: AppConfigurations())
        let socketAPI = configuration.socketAPIClient()
        socketEventCallOperation = SocketEventCallOperations()
        socketEventCallOperation?.socketAPI = socketAPI
        socketEventCallOperation?.delegate = self
        socketEventCallOperation?.handleSocketEvents()
    }
    
    func sendCallOperationToServer(fromId: String, toId: String, data: [String:AnyObject], operationType:SocketEventCallOperations.OperationType){
        socketEventCallOperation?.sendCallOperation(fromId: fromId, toId: toId, data: data, operationType: operationType)
    }
    
    func processPushMessage(message:PushMessage){
        if message.messageType == "CallMissed"{
            if let data = message.message?["data"] as? String{
                let dict = Utilities.convertJsonStringToDictionary(text: data)
                self.delegate?.didReceiveCallSocketOperation(type: message.messageType, data: dict as [String : AnyObject]?)
            }
        }
        
        if message.messageType == "CallDisconnected"{
            if let data = message.message?["data"] as? String{
                let dict = Utilities.convertJsonStringToDictionary(text: data)
                self.delegate?.didReceiveCallSocketOperation(type: message.messageType, data: dict as [String : AnyObject]?)
            }
        }
    }
    
    //MARK:- Socket Event Delegate Method(s)
    func socketEventReceived(socketEvent:YASocketEvent){
        let data = Utilities.convertJsonStringToDictionary(text: (socketEvent.socketAPI?.message?.data)! as! String)
        self.delegate?.didReceiveCallSocketOperation(type: socketEvent.socketAPI?.message?.type, data: data as [String : AnyObject]?)
    }
    
    func socketEventFailed(socketEvent:YASocketEvent){
        self.delegate?.failedToReceiveCallOperation(error: YAError.generateError(description: "Call Error", localizedDescription: "Call Error", debugDescription: "Call Operation Event Errpr \(String(describing: socketEvent.socketAPI?.message))", code: YAError.CodeType.NetworkError.hashValue))
    }
}
