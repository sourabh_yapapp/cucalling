//
//  CallManager.swift
//  Nehao
//
//  Created by Abhimanu Jindal on 01/03/17.
//  Copyright � 2017 Amit Tripathi. All rights reserved.
//

extension (CallManager): CallOperationManagerDelegate {

    func handleCallFailOperation(operationType: CallOperationType, isVideoEnabled: Bool?, isMicEnabled: Bool?, senderId: String?, receiverId: String?, callType: CallType?)
    {
        handleCallTimer(operationType: .CallDisconnected)
        sendOperationToServer(isVideoEnabled: nil, isMicEnabled: nil, operationType: .CallDisconnected, senderId: senderId, receiverId: receiverId)
        updateCallUserControls(localVideo: false, remoteVideo: false, localMic: false, remoteMic: false)
        DispatchQueue.global(qos: .utility).async {
            self.endPeerConnection()
        }
        self.reportCall(hasVideo: self.isCallTypeVideo, callType: CallkitManager.CallTypeForCallKit.EndOngoingCall, currentCallIdentifier: self.currentCallerHandle, currentCallLocalizedName: self.currentCallerName)
        
    }
    
    func updateCallOperation(operationType: CallOperationType, isVideoEnabled: Bool?, isMicEnabled: Bool?, senderId: String?, receiverId: String?, callType: CallType?)
    {
        switch operationType {
        case .CallAccepted:
            self.setSpeakerEnabledValue(isEnabled: isVideoEnabled!)
            updateCallUserControls(localVideo: isVideoEnabled!, remoteVideo: isVideoEnabled!, localMic: true, remoteMic: true)
            handleSlowNetworkAcceptOperation()
            handleCallTimer(operationType: .CallAccepted)
            sendOperationToServer(isVideoEnabled: isVideoEnabled, isMicEnabled: true, operationType: .CallAccepted, senderId: senderId, receiverId: receiverId)
            self.delegate?.didReceiveCallOperation(forType: .CallControlsVideoUpdated, isMicOrVideoEnabled: currentRemoteVideoEnableValue, callType: self.currentCallType, isVideoEnabled: false, isLocalVideoEnabled: false, isPeerConnected: false)
            self.delegate?.didReceiveCallOperation(forType: .CallControlsMicUpdated, isMicOrVideoEnabled: currentRemoteMicEnableValue, callType: self.currentCallType, isVideoEnabled: false, isLocalVideoEnabled: false, isPeerConnected: false)
            self.delegate?.didReceiveCallOperation(forType: .CallAccepted, isMicOrVideoEnabled: true, callType: self.currentCallType, isVideoEnabled: currentRemoteVideoEnableValue, isLocalVideoEnabled: currentLocalVideoEnableValue, isPeerConnected: rtcManager.isPeerConnectionConnected)

        case .CallDisconnected:
            handleCallTimer(operationType: .CallDisconnected)
            sendOperationToServer(isVideoEnabled: nil, isMicEnabled: nil, operationType: .CallDisconnected, senderId: senderId, receiverId: receiverId)
            updateCallUserControls(localVideo: false, remoteVideo: false, localMic: false, remoteMic: false)
            DispatchQueue.global(qos: .utility).async {
                self.endPeerConnection()
            }
            self.delegate?.didSentCallOperation(forType: .CallDisconnected, forCallType: callType!)

            self.reportCall(hasVideo: self.isCallTypeVideo, callType: CallkitManager.CallTypeForCallKit.EndOngoingCall, currentCallIdentifier: self.currentCallerHandle, currentCallLocalizedName: self.currentCallerName)


        case .CallControlsVideoUpdated:
            updateCallUserControls(localVideo: isVideoEnabled!, remoteVideo: nil, localMic: nil, remoteMic: nil)
            sendOperationToServer(isVideoEnabled: isVideoEnabled!, isMicEnabled: nil, operationType: .CallControlsVideoUpdated, senderId: senderId, receiverId: receiverId)

        case .CallControlsMicUpdated:
            updateCallUserControls(localVideo: nil, remoteVideo: nil, localMic: isMicEnabled!, remoteMic: nil)
            sendOperationToServer(isVideoEnabled: nil, isMicEnabled: isMicEnabled!, operationType: .CallControlsMicUpdated, senderId: senderId, receiverId: receiverId)

        case .UserBusy:
            sendOperationToServer(isVideoEnabled: nil, isMicEnabled: nil, operationType: .UserBusy, senderId: senderId, receiverId: receiverId)

        case .ContactDeleted:
            handleCallTimer(operationType: .ContactDeleted)
            sendOperationToServer(isVideoEnabled: nil, isMicEnabled: nil, operationType: .ContactDeleted, senderId: senderId, receiverId: receiverId)
            self.reportCall(hasVideo: isCallTypeVideo, callType: .EndOngoingCall, currentCallIdentifier: self.currentCallerHandle, currentCallLocalizedName: self.currentCallerName)
            self.delegate?.didSentCallOperation(forType: .ContactDeleted, forCallType: .Incoming)
            
        case .EndOngoingCall:
            YALog.print("")
            
        case .CallOnHold:
            YALog.print("On Hold")
            self.saveCurrentMediaValuesForOngoingCall()
            sendOperationToServer(isVideoEnabled: nil, isMicEnabled: nil, operationType: .CallHold, senderId: senderId, receiverId: receiverId)
            self.delegate?.didSentCallOperation(forType: .CallOnHold, forCallType: callType!)

            
        case .CallResume:
            YALog.print("Call Resumed")
            self.resetMediaValues()
            sendOperationToServer(isVideoEnabled: nil, isMicEnabled: nil, operationType: .CallResume, senderId: senderId, receiverId: receiverId)
            self.delegate?.didSentCallOperation(forType: .CallResume, forCallType: callType!)

        case .CallUserEnteredBackground:
            YALog.print("user enter background")
            sendOperationToServer(isVideoEnabled: nil, isMicEnabled: nil, operationType: .CallUserEnteredBackground, senderId: senderId, receiverId: receiverId)
            self.delegate?.didSentCallOperation(forType: .CallUserEnteredBackground, forCallType: callType!)

        case .CallUserEnteredForeground:
            YALog.print("user enter foreground")
            sendOperationToServer(isVideoEnabled: nil, isMicEnabled: nil, operationType: .CallUserEnteredForeground, senderId: senderId, receiverId: receiverId)
            self.delegate?.didSentCallOperation(forType: .CallUserEnteredForeground, forCallType: callType!)
      
        case .CallConnectionRateUpdated:
            if UIApplication.shared.applicationState == .active{
                YALog.print("connection speed updated")
                sendOperationToServer(isVideoEnabled: isVideoEnabled, isMicEnabled: nil, operationType: .CallConnectionRateUpdated, senderId: senderId, receiverId: receiverId)
                self.delegate?.didSentCallOperation(forType: .CallConnectionRateUpdated, forCallType: callType!)
            }
        
            
        default:
            YALog.print("")

        }

    }

    //MARK:- Private Method(s)
    private func sendOperationToServer(isVideoEnabled: Bool?, isMicEnabled: Bool?, operationType: SocketEventCallOperations.OperationType, senderId: String?, receiverId: String?)
    {
        var data: [String: AnyObject] = [:]
        data["senderId"] = senderId as AnyObject?
        data["receiverId"] = receiverId as AnyObject?

        if operationType != .UserBusy
            {
            data["callId"] = self.currentCallSessionID as AnyObject?
        }

        switch operationType {
        case .CallAccepted:
            data["isVideoEnabled"] = isVideoEnabled! as AnyObject?
            data["isMicEnabled"] = isMicEnabled! as AnyObject?
        case .CallControlsVideoUpdated:
            data["isVideoEnabled"] = isVideoEnabled! as AnyObject?
        case .CallControlsMicUpdated:
            data["isMicEnabled"] = isMicEnabled! as AnyObject?
        case .CallConnectionRateUpdated:
            data["isVideoEnabled"] = isVideoEnabled! as AnyObject?
        default:
            YALog.print("")
        }

        if operationType == .UserBusy
            {
            callOperationManager.sendCallOperationToServer(fromId: senderId!, toId: receiverId!, data: data, operationType: operationType)
        }
            else
        {
            if senderId != nil && receiverId != nil {
                callOperationManager.sendCallOperationToServer(fromId: senderId!, toId: receiverId!, data: data, operationType: operationType)
            }
        }
    }

    private func handleCallTimer(operationType: SocketEventCallOperations.OperationType)
    {
        if operationType == .UserBusy || operationType == .CallControlsVideoUpdated || operationType == .CallControlsMicUpdated
            {
            return
        }
        cancelAllTimers()
    }

    private func handleSlowNetworkAcceptOperation()
    {
        if self.isSlowNetwork
            {
            self.currentCallType = .OngoingIn
            self.delegate?.updateMediaStreamOnRtcConnection()
            DispatchQueue.global(qos: .userInteractive).async {
                self.setUpMediaStream(shouldCreateAudioStream: true, shouldCreateVideoStream: true)
            }
        }
    }
    
    private func saveCurrentMediaValuesForOngoingCall()
    {
        prevMediaControlValues.setValues(localMic: currentLocalMicEnableValue, localVideo: currentLocalVideoEnableValue, remoteMic: currentRemoteMicEnableValue, remoteVideo: currentRemoteVideoEnableValue)
        updateCallUserControls(localVideo: false, remoteVideo: false, localMic: false, remoteMic: false)

    }
    
    private func resetMediaValues()
    {
        updateCallUserControls(localVideo: prevMediaControlValues.localVideoEnabled, remoteVideo: prevMediaControlValues.remoteVideoEnabled, localMic: prevMediaControlValues.localMicEnabled, remoteMic: prevMediaControlValues.remoteMicEnabled)
    }

    //MARK:- CallKit Methods
    
    func endCallKitButtonTapped()
    {
        if (self.currentCallType == .Ongoing)
        {
            self.updateCallOperation(operationType: .CallDisconnected, isVideoEnabled: nil, isMicEnabled: nil, senderId: currentSenderID, receiverId: currentReceiverID, callType: .Ongoing)
            self.delegate?.didReceiveCallOperation(forType: .EndOngoingCall, isMicOrVideoEnabled: false, callType: self.currentCallType, isVideoEnabled: false, isLocalVideoEnabled: false, isPeerConnected: false)
            return

        }
        if (self.currentCallType == .Outgoing || currentCallType == .OngoingOut)
        {
            self.updateCallOperation(operationType: .CallDisconnected, isVideoEnabled: nil, isMicEnabled: nil, senderId: currentSenderID, receiverId: currentReceiverID!, callType: .Outgoing)
            self.delegate?.didSentCallOperationMissedViaCallKit()
            return

            
        }
        if (self.currentCallType == .Incoming) || currentCallType == .OngoingIn
        {
            self.updateCallOperation(operationType: .CallDisconnected, isVideoEnabled: nil, isMicEnabled: nil, senderId: currentSenderID, receiverId: currentReceiverID, callType: .Incoming)
            return
        }
        if (self.currentCallType == .Idle)
        {
            self.updateCallOperation(operationType: .CallDisconnected, isVideoEnabled: nil, isMicEnabled: nil, senderId: currentSenderID, receiverId: currentReceiverID, callType: .Ongoing)
            return

        }
    }
    
    func incominCallAnsweringFailed(reason: CallFailureReason, callTypeVideo isVideo: Bool)
    {
        self.updateCallOperation(operationType: .CallDisconnected, isVideoEnabled: nil, isMicEnabled: nil, senderId: currentSenderID, receiverId: currentReceiverID, callType: .Ongoing)
    }

    func audioSessionActivatedByCallkit() {
        if self.currentCallState == .Connecting {
            if self.userSelectedOutputPref != nil
            {
                self.callAudioManager.playCallAudio(type: .Connecting)
            }
            else
            {
                self.callAudioManager.playCallAudio(type: .Connecting, shouldToggleSpeakerForVideoCall: isCallTypeVideo)
            }
        }
        self.delegate?.audioSessionActivatedByCallKit()
    }
    
    
    func callSetOnHold(isVideoCall: Bool)
    {
        callMediaManager.toggleTrack(isEnabled: false, isAudio: true, isLocal: true)
        self.updateCallOperation(operationType: .CallOnHold, isVideoEnabled: false, isMicEnabled: false, senderId: currentSenderID, receiverId: currentReceiverID, callType: .Ongoing)
    }
    
    func callResumed(isVideoCall: Bool)
    {
        callMediaManager.toggleTrack(isEnabled: true, isAudio: true, isLocal: true)
        self.updateCallOperation(operationType: .CallResume, isVideoEnabled: false, isMicEnabled: false, senderId: currentSenderID, receiverId: currentReceiverID, callType: .Ongoing)
    }

    
    func callMuteUpdated(isMute: Bool)
    {
        self.toggleMic(isMicEnabled: !isMute)
    }
    
    
    //MARK:- Bitrate Handling Methods
    
    func callConnectionRateUpdated(isSlow: Bool)
    {
        self.currentConnectionIsSlow = isSlow
        if self.currentConnectionIsSlow
        {
            self.delegate?.didUpdateVideoViewChangeInBitrate(isSlow: true)
        }
        else
        {
            self.delegate?.didUpdateVideoViewChangeInBitrate(isSlow: self.connectedUserConnectionIsSlow)
        }
        self.updateCallOperation(operationType: .CallConnectionRateUpdated, isVideoEnabled: !isSlow, isMicEnabled: nil, senderId: currentSenderID, receiverId: currentReceiverID, callType: .Ongoing)
    }
    
    //MARK:-

    func updateCallUserControls(localVideo: Bool?, remoteVideo: Bool?, localMic: Bool?, remoteMic: Bool?) {
        if localVideo != nil {
            currentLocalVideoEnableValue = localVideo!
            YALog.print("currentLocalVideoEnableValue - \(currentLocalVideoEnableValue)")
        }
        if remoteVideo != nil {
            currentRemoteVideoEnableValue = remoteVideo!
        }
        if localMic != nil {
            currentLocalMicEnableValue = localMic!
        }
        if remoteMic != nil {
            currentRemoteMicEnableValue = remoteMic!
        }
    }

    //MARK:- Delegate Method(s)
    func didReceiveCallSocketOperation(type: String?, data: [String: AnyObject]?) {
        self.incomingCallhandler.handleSocketOperation(type: type, data: data)
    }
    
    func executeSocketOperation(type: String?, data: [String: AnyObject]?){

        let senderID = data?["senderId"] as? String
        let receiverID = data?["receiverId"] as? String
        let callSessionID = data?["callId"] as? String
        
               
        if senderID == nil || receiverID == nil {
            self.incomingCallhandler.processNextNewPendingmessage()
            return
        }
        
        
        if (callSessionID != nil) && (callSessionID != currentCallSessionID)
        {        self.incomingCallhandler.processNextNewPendingmessage()
            return
        }
        
        if senderID! != currentReceiverID || receiverID! != currentSenderID {
            self.incomingCallhandler.processNextNewPendingmessage()
            return
        }
        
        if type == SocketEventCallOperations.OperationType.CallDisconnected.rawValue {
            cancelAllTimers()
            updateCallUserControls(localVideo: false, remoteVideo: false, localMic: false, remoteMic: false)
            self.delegate?.didReceiveCallOperation(forType: .CallDisconnected, isMicOrVideoEnabled: false, callType: self.currentCallType, isVideoEnabled: false, isLocalVideoEnabled: false, isPeerConnected: false)
            self.reportCall(hasVideo: self.isCallTypeVideo, callType: CallkitManager.CallTypeForCallKit.EndOngoingCall, currentCallIdentifier: self.currentCallerHandle, currentCallLocalizedName: self.currentCallerName)
            self.endPeerConnection()

            stopAllAudio()
            endCallHandshake()
        }
    
        
        if type == SocketEventCallOperations.OperationType.ContactDeleted.rawValue {
            if currentCallType == .Outgoing || currentCallType == .OngoingOut {
                cancelAllTimers()
                updateCallUserControls(localVideo: false, remoteVideo: false, localMic: false, remoteMic: false)
                self.reportCall(hasVideo: self.isCallTypeVideo, callType: CallkitManager.CallTypeForCallKit.EndOngoingCall, currentCallIdentifier: self.currentCallerHandle, currentCallLocalizedName: self.currentCallerName)

                self.delegate?.didReceiveCallOperation(forType: .ContactDeleted, isMicOrVideoEnabled: false, callType: self.currentCallType, isVideoEnabled: false, isLocalVideoEnabled: false, isPeerConnected: false)
                DispatchQueue.global(qos: .utility).async {
                    self.endPeerConnection()
                }
                stopAllAudio()
                endCallHandshake()
                
            }
        }
        
        if type == SocketEventCallOperations.OperationType.CallMissed.rawValue {
            let isVideoEnabled = data?["isVideoEnabled"] as! Bool
            
            if currentCallType == .Incoming || (currentCallType == .Ongoing && self.currentSenderID == receiverID) {
                cancelAllTimers()
                updateCallUserControls(localVideo: false, remoteVideo: false, localMic: false, remoteMic: false)
                // self.delegate?.didReceiveCallOperationMissed()
                DispatchQueue.global(qos: .utility).async {
                    self.endPeerConnection()
                }
                stopAllAudio()
                endCallHandshake()
                self.reportCall(hasVideo: isVideoEnabled, callType: CallkitManager.CallTypeForCallKit.EndOngoingCall, currentCallIdentifier: self.currentCallerHandle, currentCallLocalizedName: self.currentCallerName)
            }
            
        }
        if type == SocketEventCallOperations.OperationType.CallAccepted.rawValue {
            if currentCallType == .Outgoing || currentCallType == .OngoingOut {
                let isVideoEnabled = data?["isVideoEnabled"] as! Bool
                cancelAllTimers()

                self.currentCallType = .Ongoing
                if self.isSlowNetwork{
                    self.currentCallType = .OngoingOut
                }
                self.updateCallUserControls(localVideo: nil, remoteVideo: isVideoEnabled, localMic: nil, remoteMic: nil)
                stopAllAudio()
                self.delegate?.didReceiveCallOperation(forType: .CallAccepted, isMicOrVideoEnabled: true, callType: self.currentCallType, isVideoEnabled: isVideoEnabled, isLocalVideoEnabled: currentLocalVideoEnableValue, isPeerConnected: rtcManager.isPeerConnectionConnected)

                //self.outgoingCallAnswered(isVideo: isVideoEnabled)
                if self.isSlowNetwork
                {
                    DispatchQueue.global(qos: .userInitiated).asyncAfter(deadline: .now() + 1, execute: {
                        self.initiateSignalling()
                    })
                }
                self.reportCall(hasVideo: isVideoEnabled, callType: .OutGoingCallAccepted, currentCallIdentifier: self.currentCallerHandle, currentCallLocalizedName: self.currentCallerName)
                
                
            }
        }
        
        if type == SocketEventCallOperations.OperationType.EndOngoingCall.rawValue {
            cancelAllTimers()
            if currentCallType == .Ongoing || currentCallType == .OngoingIn || currentCallType == .OngoingOut {
                updateCallUserControls(localVideo: false, remoteVideo: false, localMic: false, remoteMic: false)
                self.delegate?.didReceiveCallOperation(forType: .EndOngoingCall, isMicOrVideoEnabled: false, callType: self.currentCallType, isVideoEnabled: false, isLocalVideoEnabled: false, isPeerConnected: false)
                DispatchQueue.global(qos: .utility).async {
                    self.endPeerConnection()
                }
                endCallHandshake()
            }
            self.reportCall(hasVideo: self.isCallTypeVideo, callType: CallkitManager.CallTypeForCallKit.EndOngoingCall, currentCallIdentifier: self.currentCallerHandle, currentCallLocalizedName: self.currentCallerName)
        }
        
        if type == SocketEventCallOperations.OperationType.CallControlsVideoUpdated.rawValue {
            let isVideoEnabled = data?["isVideoEnabled"] as! Bool
            updateCallUserControls(localVideo: nil, remoteVideo: isVideoEnabled, localMic: nil, remoteMic: nil)
            updateRemoteAudioVideoStream(isVideoEnabled: isVideoEnabled)
            self.delegate?.didReceiveCallOperation(forType: .CallControlsVideoUpdated, isMicOrVideoEnabled: isVideoEnabled, callType: self.currentCallType, isVideoEnabled: false, isLocalVideoEnabled: false, isPeerConnected: false)
            
            toggleSpeakerOnVideoUpdatedOperation(isSpeaker: isVideoEnabled)
            self.delegate?.didUpdateCallMediaType(isVideoEnabled: isVideoEnabled)
            if isVideoEnabled {
                self.reportCall(hasVideo: isVideoEnabled, callType: CallkitManager.CallTypeForCallKit.UpdateCallToVideo, currentCallIdentifier: self.currentCallerHandle, currentCallLocalizedName: self.currentCallerName)
            }
            else
            {
                self.reportCall(hasVideo: isVideoEnabled, callType: CallkitManager.CallTypeForCallKit.UpdateCallToAudio, currentCallIdentifier: self.currentCallerHandle, currentCallLocalizedName: self.currentCallerName)
            }
            
        }
        
        if type == SocketEventCallOperations.OperationType.CallControlsMicUpdated.rawValue {
            let isMicEnabled = data?["isMicEnabled"] as! Bool
            updateCallUserControls(localVideo: nil, remoteVideo: nil, localMic: nil, remoteMic: isMicEnabled)
            self.delegate?.didReceiveCallOperation(forType: .CallControlsMicUpdated, isMicOrVideoEnabled: isMicEnabled, callType: self.currentCallType, isVideoEnabled: false, isLocalVideoEnabled: false, isPeerConnected: false)
        }
        
        if type == SocketEventCallOperations.OperationType.UserBusy.rawValue {
            cancelAllTimers()
            updateCallUserControls(localVideo: false, remoteVideo: false, localMic: false, remoteMic: false)
            self.delegate?.didReceiveEndCallState(with:self.currentCallType)
            self.delegate?.didReceiveCallOperation(forType: .UserBusy, isMicOrVideoEnabled: false, callType: self.currentCallType, isVideoEnabled: false, isLocalVideoEnabled: false, isPeerConnected: false)
            self.reportCall(hasVideo: self.isCallTypeVideo, callType: CallkitManager.CallTypeForCallKit.EndOngoingCall, currentCallIdentifier: self.currentCallerHandle, currentCallLocalizedName: self.currentCallerName)
            DispatchQueue.global(qos: .utility).async {
                self.endPeerConnection()
            }
            stopAllAudio()
            endCallHandshake()
        }
        
        if type == SocketEventCallOperations.OperationType.CallHold.rawValue {
            self.saveCurrentMediaValuesForOngoingCall()
            self.playCallAudio(type: .OnHold)
            updateCallUserControls(localVideo: false, remoteVideo: false, localMic: false, remoteMic: false)
            self.delegate?.didReceiveCallOperation(forType: .CallOnHold, isMicOrVideoEnabled: false, callType: self.currentCallType, isVideoEnabled: false, isLocalVideoEnabled: false, isPeerConnected: false)
        }
        
        if type == SocketEventCallOperations.OperationType.CallResume.rawValue {
            self.resetMediaValues()
            stopAllAudio()
            self.delegate?.didReceiveCallOperation(forType: .CallResume, isMicOrVideoEnabled: false, callType: self.currentCallType, isVideoEnabled: currentRemoteVideoEnableValue, isLocalVideoEnabled: false, isPeerConnected: false)
        }
        
        if type == SocketEventCallOperations.OperationType.CallUserEnteredBackground.rawValue {
            
        self.delegate?.didReceiveCallOperation(forType: .CallUserEnteredBackground, isMicOrVideoEnabled: false, callType: self.currentCallType, isVideoEnabled: currentRemoteVideoEnableValue, isLocalVideoEnabled: false, isPeerConnected: false)
        }

        if type == SocketEventCallOperations.OperationType.CallUserEnteredForeground.rawValue {
            self.delegate?.didReceiveCallOperation(forType: .CallUserEnteredForeground, isMicOrVideoEnabled: false, callType: self.currentCallType, isVideoEnabled: currentRemoteVideoEnableValue, isLocalVideoEnabled: false, isPeerConnected: false)
        }

        if type == SocketEventCallOperations.OperationType.CallConnectionRateUpdated.rawValue
        {
            let isVideoEnabled = data?["isVideoEnabled"] as! Bool
            self.delegate?.didReceiveCallOperation(forType: .CallConnectionRateUpdated, isMicOrVideoEnabled: false, callType: self.currentCallType, isVideoEnabled: isVideoEnabled, isLocalVideoEnabled: false, isPeerConnected: false)
        }
        
        DispatchQueue.global().asyncAfter(deadline: .now() + 2) {
            self.incomingCallhandler.processNextNewPendingmessage()
        }

    }
    
    func failedToReceiveCallOperation(error: YAError?)
    {
        // Disconnect Call when socket operation failed
        if self.checkIfCallInProgress(){
//            self.handleCallFailOperation(operationType: .CallDisconnected, isVideoEnabled: nil, isMicEnabled: nil, senderId: self.currentSenderID, receiverId: self.currentReceiverID, callType: self.currentCallType)
        }
        YALog.print("\(String(describing: error?.localizedDescription))")
    }



    }
