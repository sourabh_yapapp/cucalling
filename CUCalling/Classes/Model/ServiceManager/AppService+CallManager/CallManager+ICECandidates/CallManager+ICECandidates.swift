//
//  CallManager.swift
//  Nehao
//
//  Created by Abhimanu Jindal on 01/03/17.
//  Copyright © 2017 Amit Tripathi. All rights reserved.
//

extension (CallManager):CallICECandidatesManagerDelegate {

    func sendICECandidatesToServer(fromId: String, toId: String, data: [String:AnyObject]){
        callICECandidateManager.sendICECandidateEvent(fromId: fromId, toId: toId, data: data)
    }
    
    func processICECandidate(sdpMId: String, sdpMLineIndex: Int, sdp: String){
        rtcManager.processICECandidate(sdpMId: sdpMId, sdpMLineIndex: sdpMLineIndex, sdp: sdp)
    }
    
    //MARK:- Delegate Method(s)
    
    func didReceiveICECandidate(sdpMId: String, sdpMLineIndex: Int, sdp: String) {
        processICECandidate(sdpMId: sdpMId, sdpMLineIndex: sdpMLineIndex, sdp: sdp)
    }
    
    func failedToReceiveICECandidate(error: YAError?) {
        YALog.print("Failed to log ICE Candidate")
    }
    
}
