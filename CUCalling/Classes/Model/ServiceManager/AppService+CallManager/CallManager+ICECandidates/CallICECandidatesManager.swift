//
//  CallManager.swift
//  Nehao
//
//  Created by Abhimanu Jindal on 01/03/17.
//  Copyright © 2017 Amit Tripathi. All rights reserved.
//

import Foundation

protocol CallICECandidatesManagerDelegate:class {
    func didReceiveICECandidate(sdpMId: String, sdpMLineIndex: Int, sdp: String)
    func failedToReceiveICECandidate(error:YAError?)
}

class CallICECandidatesManager: NSObject, YASocketEventDelegate {
    
    weak var delegate:CallICECandidatesManagerDelegate?
    private var socketEventICECandidates:SocketEventICECandidates?
    
    //MARK:- Private Method(s)
    override init() {
        super.init()
        let configuration = YAConfiguration.initConfigurationWithClient(client: AppConfigurations())
        let socketAPI = configuration.socketAPIClient()
        socketEventICECandidates = SocketEventICECandidates()
        socketEventICECandidates?.socketAPI = socketAPI
        socketEventICECandidates?.delegate = self
        socketEventICECandidates?.handleSocketEvents()
    }
    
    func sendICECandidateEvent(fromId:String, toId: String, data:[String:AnyObject]) {
        socketEventICECandidates?.sendICECandidateEvent(fromId: fromId, toId: toId, data: data)
    }
    
    
    //MARK:- Socket Event Delegate Method(s)
    func socketEventReceived(socketEvent:YASocketEvent){
        let data = socketEvent.socketAPI?.message?.data
        var jsonResult:Any?
        guard data != nil else {
            YALog.print("No Data available")
            self.delegate?.failedToReceiveICECandidate(error: nil)
            return
        }
        if data! is String {
            do {
                let jsonString = data as! String
                let jsonData = jsonString.data(using: String.Encoding.utf8, allowLossyConversion: false)!
                jsonResult = try JSONSerialization.jsonObject(with: jsonData, options:
                    JSONSerialization.ReadingOptions.mutableContainers)
            } catch {
                print("JSON Processing Failed")
            }
        }
        let resultData = jsonResult
        let result = resultData as! [String:AnyObject]
        
        if result["sdpMId"] == nil {
            YALog.print("Socket sdpMId Response is not correct")
            self.delegate?.failedToReceiveICECandidate(error: nil)
            return
        }
        if result["sdpMLineIndex"] == nil {
            YALog.print("Socket sdpMLineIndex Response is empty")
            self.delegate?.failedToReceiveICECandidate(error: nil)
            return
        }
        if result["sdp"] == nil {
            YALog.print("Socket sdp Response is empty")
            self.delegate?.failedToReceiveICECandidate(error: nil)
            return
        }
        
        self.delegate?.didReceiveICECandidate(sdpMId: (result["sdpMId"] as! String), sdpMLineIndex: (result["sdpMLineIndex"] as! Int), sdp: (result["sdp"] as! String))
    }
    
    func socketEventFailed(socketEvent:YASocketEvent){
        self.delegate?.failedToReceiveICECandidate(error: nil)
    }
    
}
