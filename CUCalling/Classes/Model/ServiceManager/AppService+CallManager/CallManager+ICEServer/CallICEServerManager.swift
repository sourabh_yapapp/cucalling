//
//  CallManager.swift
//  Nehao
//
//  Created by Abhimanu Jindal on 01/03/17.
//  Copyright © 2017 Amit Tripathi. All rights reserved.
//

import Foundation

protocol CallICEServerManagerDelegate:class {
    func didReceiveICEServers(iceServers:[CallICEServer])
    func iceServerFailed(error:YAError)
}

class CallICEServerManager: NSObject, YASocketEventDelegate {
    //MARK:- Varialbles/Constants
    weak var delegate:CallICEServerManagerDelegate?
    private var socketEventICEServer:SocketEventICEServer?
    private var isICEServerListShared = false
    private var isICEServerListFetched = false
    private lazy var dbServiceManagerICEServer:DBServiceManagerICEServers = DBServiceManagerICEServers()
    
    //MARK:- Overridden Method(S)
    override init(){
        super.init()
        
        let configuration = YAConfiguration.initConfigurationWithClient(client: AppConfigurations())
        let socketAPI = configuration.socketAPIClient()
        socketEventICEServer = SocketEventICEServer()
        socketEventICEServer?.socketAPI = socketAPI
        socketEventICEServer?.delegate = self
        socketEventICEServer?.handleSocketEvents()
        
    }
    
    //MARK:- Public Method(s)
    
    func setupICEServers(){
        isICEServerListAvailableLocally { (isAvailable, array) in
            if isAvailable{
                self.decodeICEServers(array: array, completionHandler: { (iceServers) in
                    if Utilities.isArrayEmpty(iceServers){
                        //WAIT TO FETCH FROM SERVER
                        return
                    }
                    self.isICEServerListShared = true
                    self.delegate?.didReceiveICEServers(iceServers: iceServers!)
                    return
                })
            }
        }
        if !isICEServerListFetched{
            self.socketEventICEServer?.getICEServers()
        }
        
    }
    
    func cleanUpICEServers(){
        isICEServerListShared = false
        isICEServerListFetched = false
    }
    
    //MARK:- Private Method(s)
    private func processICEServerList(message:YASocketMessage?){
        
        guard message?.data != nil else {
            if !isICEServerListShared{
                self.delegate?.iceServerFailed(error:YAError.generateError(description: "Call Error", localizedDescription: "Call Failed", debugDescription: "NO ICE Server List Found from Server", code: YAError.CodeType.NetworkError.hashValue)!)
            }
            return
        }
        guard message?.data! is String else{
            if !isICEServerListShared{
                self.delegate?.iceServerFailed(error:YAError.generateError(description: "Call Error", localizedDescription: "Call Failed", debugDescription: "NO ICE Server List Found from Server", code: YAError.CodeType.NetworkError.hashValue)!)
            }
            return
        }
        
        let iceServers = mapICEServers(data:message?.data)
        if iceServers.count > 0{
            self.encodeToDictionary(servers: iceServers, completionHandler: { (array) in
                if !Utilities.isArrayEmpty(array){
                    dbServiceManagerICEServer.callDBService(data: array as AnyObject?, methodType: .INSERT, queryString: "ICESERVERS", sortQueryString: nil, sortType: nil, completionHandler: { (result, error) in
                        if error != nil{
                            YALog.print("‼️‼️‼️ICE SERVER SAVING ERROR -> \(String(describing: error?.debugDescription)) ‼️‼️‼️")
                            self.delegate?.iceServerFailed(error:YAError.generateError(description: "Call Error", localizedDescription: "Call Failed", debugDescription: "Error in savince ICE Server", code: YAError.CodeType.NetworkError.hashValue)!)

                        }
                        else{
                            self.isICEServerListFetched = true
                            if !self.isICEServerListShared{
                                self.setupICEServers()
                            }
                        }
                    })
                }
                else{
                    if !self.isICEServerListShared{
                        self.delegate?.iceServerFailed(error:YAError.generateError(description: "Call Error", localizedDescription: "Call Failed", debugDescription: "NO ICE Server List Found from Server", code: YAError.CodeType.NetworkError.hashValue)!)
                    }
                }
            })
            return
        }
        else{
            if !self.isICEServerListShared{
                self.delegate?.iceServerFailed(error:YAError.generateError(description: "Call Error", localizedDescription: "Call Failed", debugDescription: "NO ICE Server List Found from Server", code: YAError.CodeType.NetworkError.hashValue)!)
            }
        }
    }
    
    private func mapICEServers(data:AnyObject?)->[CallICEServer] {
        let resultData = Utilities.convertJsonStringToDictionary(text: data as! String)
        let result = resultData! as [String:AnyObject]
        var iceServer:[CallICEServer] = []
        iceServer = getServers(result:result, serverKeyType:"STUN", iceServerType:.STUN)
        iceServer.append(contentsOf: getServers(result:result, serverKeyType:"TURN", iceServerType:.TURN))
        return iceServer
    }
    
    private func getServers(result:[String:AnyObject], serverKeyType:String, iceServerType:CallICEServer.ICEServerType)->[CallICEServer]{
        if result[serverKeyType] == nil {
            return []
        }
        let stunServers:[[String:AnyObject]] = result[serverKeyType] as! [[String:AnyObject]]
        var iceServers:[CallICEServer] = []
        for (_, item) in stunServers.enumerated() {
            var iceServer = CallICEServer()
            iceServer.url = item["url"] as! String?
            
            let credential = item["credential"]
            if credential != nil{
                iceServer.password = (item["credential"] as! String?)!
            }
            
            let userName = item["username"]
            if userName != nil{
                iceServer.userName = (item["username"] as! String?)!
            }
            iceServer.serverType = iceServerType
            iceServers.append(iceServer)
        }
        return iceServers
    }
    
    private func isICEServerListAvailableLocally(completionHandler:@escaping (_ isAvailable:Bool, _ iceServers:Array<Dictionary<AnyHashable, Any>>?)->Void){
        dbServiceManagerICEServer.callDBService(data: nil, methodType: .FETCH, queryString: "ICESERVERS", sortQueryString: nil, sortType: nil, completionHandler: { (result, error) in
            if error != nil{
                YALog.print("‼️‼️‼️DB ICE SERVER ERROR -> \(String(describing: error?.debugDescription)) ‼️‼️‼️")
                completionHandler(false, nil)
                return
            }
            guard let iceServers = result as? Array<Dictionary<AnyHashable, Any>> else{
                YALog.print("‼️‼️‼️DB ICE SERVER ERROR -> \(String(describing: result)) ‼️‼️‼️")
                completionHandler(false, nil)
                return
            }
            completionHandler(true, iceServers)
        })
    }
    
    private func encodeToDictionary(servers:Array<CallICEServer>?, completionHandler:(Array<Dictionary<AnyHashable, Any>>?)->Void){
        if Utilities.isArrayEmpty(servers){
            completionHandler(nil)
            return
        }
        var array:[Dictionary<AnyHashable, Any>] = []
        for (_,iceServer) in (servers?.enumerated())!{
            var server = iceServer
            let dictionary = server.encodeToDictionary()
            array.append(dictionary)
        }
        completionHandler(array)
    }
    
    private func decodeICEServers(array:Array<Dictionary<AnyHashable, Any>>?, completionHandler:(Array<CallICEServer>?)->Void){
        if Utilities.isArrayEmpty(array){
            completionHandler(nil)
            return
        }
        var servers:[CallICEServer] = []
        for (_,dictionary) in (array?.enumerated())!{
            var server = CallICEServer()
            server.decodeFromDictionary(dictionary: dictionary)
            servers.append(server)
        }
        completionHandler(servers)
    }
    
    //MARK:- Socket Event Delegate Method(s)
    func socketEventReceived(socketEvent:YASocketEvent){
        let type = socketEvent.socketAPI?.message?.type
        guard type != nil else {
            YALog.print("##### No Type Found - \(String(describing: socketEvent.socketAPI?.message)) ####")
            if !isICEServerListShared{
                self.delegate?.iceServerFailed(error:YAError.generateError(description: "Call Error", localizedDescription: "Call Failed", debugDescription: "NO ICE Server List Found from Server", code: YAError.CodeType.NetworkError.hashValue)!)
            }
            return
        }
        let typeValue = type!
        switch typeValue {
        case "getIceServer":
            processICEServerList(message:socketEvent.socketAPI?.message)
        default:
            if !isICEServerListShared{
                self.delegate?.iceServerFailed(error:YAError.generateError(description: "Call Error", localizedDescription: "Call Failed", debugDescription: "NO ICE Server List Found from Server", code: YAError.CodeType.NetworkError.hashValue)!)
            }
        }
        
    }
    
    func socketEventFailed(socketEvent:YASocketEvent){
        if !isICEServerListShared{
            self.delegate?.iceServerFailed(error:YAError.generateError(description: "Call Error", localizedDescription: "Call Failed", debugDescription: "NO ICE Server List Found from Server", code: YAError.CodeType.NetworkError.hashValue)!)
        }
    }

}
