//
//  CallManager.swift
//  Nehao
//
//  Created by Abhimanu Jindal on 01/03/17.
//  Copyright © 2017 Amit Tripathi. All rights reserved.
//

extension (CallManager):CallICEServerManagerDelegate {
    
    func didReceiveICEServers(iceServers:[CallICEServer]){
        setUpPeerConnection(iceServers:iceServers)
    }
    
    func iceServerFailed(error:YAError){
        isPeerConnectionSet = false
        YALog.print("FAILED TO SETUP ICE SERVER")
    }
}
