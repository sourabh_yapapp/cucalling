//
//  CallManager.swift
//  Nehao
//
//  Created by Abhimanu Jindal on 01/03/17.
//  Copyright © 2017 Amit Tripathi. All rights reserved.
//

import Foundation
protocol CallInitiationManagerDelegate:class {
    func socketHandshakeFailed(error:YAError)
    func didReceiveCallRequest(fromID:String?, toID:String?, userName:String?, data:[String:AnyObject],isCallTypeVideo: Bool?)
    func didReceiveCallRquestResponse(fromID: String?, toID: String?, userName: String?, data: [String : AnyObject])
}

class CallInitiationManager: NSObject, YASocketEventDelegate {
    
    private var socketCallHandShake:SocketEventCallHandshake?
    weak var delegate:CallInitiationManagerDelegate?

    //MARK:- Overridden Method(S)
    override init(){
        super.init()

        let configuration = YAConfiguration.initConfigurationWithClient(client: AppConfigurations())
        let socketAPI = configuration.socketAPIClient()
        socketCallHandShake = SocketEventCallHandshake()
        socketCallHandShake?.socketAPI = socketAPI
        socketCallHandShake?.delegate = self
        socketCallHandShake?.handleSocketEvents()
        
    }
    
    func startNetworkHandshake(fromId: String, toId: String, data: [String:AnyObject]){
        socketCallHandShake?.startNetworkHandshake(fromId: fromId, toId: toId, data: data)
    }
    
    func completeNetworkHandshake(fromId: String, toId: String, data: [String:AnyObject]){
        socketCallHandShake?.completeNetworkHandshake(fromId: fromId, toId: toId, data: data)
    }
    
    func processPushMessage(message:PushMessage){
        if message.messageType == "requestToConnectToSocket"{
            processNewRequest(pushMessage: message)
        }
    }
    
    //MARK:- Private Method(s)
    private func processNewRequest(message:YASocketMessage?){
        let message = Utilities.convertJsonStringToDictionary(text: (message?.data)! as! String)
        var videoCall = false
        if let isVideoCall = message?["isVideoCall"] as? Bool{
        videoCall = isVideoCall
        }
        
        self.delegate?.didReceiveCallRequest(fromID: message?["senderId"] as? String, toID: message?["receiverId"] as? String, userName: message?["userName"] as? String, data: (message?["otherDetails"] as? [String:AnyObject])!,isCallTypeVideo: videoCall)
    }
    
    private func processNewRequest(pushMessage:PushMessage?){
        if let data = pushMessage?.message?["data"] as? String{
            let message = Utilities.convertJsonStringToDictionary(text: data)
            var videoCall = false
            if let isVideoCall = message?["isVideoCall"] as? Bool{
                videoCall = isVideoCall
            }
            self.delegate?.didReceiveCallRequest(fromID: message?["senderId"] as? String, toID: message?["receiverId"] as? String, userName: message?["userName"] as? String, data: (message?["otherDetails"] as? [String:AnyObject])!,isCallTypeVideo: videoCall)

        }
    }
    
    private func processRequestResponse(message:YASocketMessage?){
        let message = Utilities.convertJsonStringToDictionary(text: (message?.data)! as! String)
        self.delegate?.didReceiveCallRquestResponse(fromID: message?["senderId"] as? String, toID: message?["receiverId"] as? String, userName: message?["userName"] as? String, data: (message?["otherDetails"] as? [String:AnyObject])!)
    }
    
    
    //MARK:- Socket Event Delegate Method(s)
    func socketEventReceived(socketEvent:YASocketEvent){
        let type = socketEvent.socketAPI?.message?.type
        guard type != nil else {
            self.delegate?.socketHandshakeFailed(error:YAError.generateError(description: "Call Error", localizedDescription: "Call Failed", debugDescription: "NO HANDSHAKE TYPE FOUND", code: YAError.CodeType.NetworkError.hashValue)!)
            return
        }
        let typeValue = type!
        switch typeValue {
        case "requestToConnectToSocket":
            processNewRequest(message:socketEvent.socketAPI?.message)
        case "receiverConnectedToSocket":
            processRequestResponse(message:socketEvent.socketAPI?.message)
        case "receiverFailedToConnectToSocket":
            self.delegate?.socketHandshakeFailed(error:YAError.generateError(description: "Call Error", localizedDescription: "Call Failed", debugDescription: "receiverFailedToConnectToSocket", code: YAError.CodeType.NetworkError.hashValue)!)
        default:
            self.delegate?.socketHandshakeFailed(error:YAError.generateError(description: "Call Error", localizedDescription: "Call Failed", debugDescription: "NO HANDSHAKE TYPE FOUND", code: YAError.CodeType.NetworkError.hashValue)!)

        }
        
    }
    
    func socketEventFailed(socketEvent:YASocketEvent){
//        self.delegate?.socketHandshakeFailed(error:YAError.generateError(description: "Call Error", localizedDescription: "Call Failed", debugDescription: "NO HANDSHAKE TYPE FOUND", code: YAError.CodeType.NetworkError.hashValue)!)

    }
    
}
