//
//  CallManager.swift
//  Nehao
//
//  Created by Abhimanu Jindal on 01/03/17.
//  Copyright © 2017 Amit Tripathi. All rights reserved.
//

extension (CallManager):CallInitiationManagerDelegate {
   
    
    func startNetworkHandshake(){
        callInitiationManager.startNetworkHandshake(fromId: currentSenderID!, toId: currentReceiverID!, data: currentCallInitiationDetails!)
    }
    
    func completeNetworkHandshake(){
        callInitiationManager.completeNetworkHandshake(fromId: currentSenderID!, toId: currentReceiverID!, data: currentCallInitiationDetails!)
    }
    
    //MARK:- CallInititation Delegate Method(s)
    func socketHandshakeFailed(error:YAError){
                if currentCallType == .Outgoing || currentCallType == .OngoingOut{
//                    self.delegate?.didReceiveCallOperation(forType: .UserBusy, isMicOrVideoEnabled: false, callType: .Idle, isVideoEnabled: false, isLocalVideoEnabled: false, isPeerConnected: true)
        }
        if currentCallType == .Incoming || currentCallType == .OngoingIn{
//            self.delegate?.didReceiveCallOperation(forType: .UserBusy, isMicOrVideoEnabled: false, callType: .Idle, isVideoEnabled: false, isLocalVideoEnabled: false, isPeerConnected: true)
        }
        DispatchQueue.global(qos: .utility).async {
            self.endPeerConnection()
        }
        stopAllAudio()
    }
    

    func didReceiveCallRequest(fromID: String?, toID: String?, userName: String?, data: [String : AnyObject], isCallTypeVideo: Bool?) {
        
        var currentNetworkSpeed = self.currentBandwidthSpeed()
        if currentNetworkSpeed == .poor || currentNetworkSpeed == .unavailable
        {
            isSlowNetwork = true
        }
        
        self.delegate?.didReceiveIncomingCallRequest(senderID: fromID!, receiverID: toID!, userName: userName!, details: data, isCallTypeVideo: isCallTypeVideo!)

    }
    
    func didReceiveCallRquestResponse(fromID: String?, toID: String?, userName: String?, data: [String : AnyObject]){
        
        if let callSessionId = data["callId"] as? String {
            if callSessionId == self.currentCallSessionID
                {
                if let isSlowNet = data["isSlowNetwork"] as? Bool {
                    if isSlowNet == true {
                        isSlowNetwork = isSlowNet
                    }
                }


                if isSlowNetwork {
                    if self.userSelectedOutputPref != nil
                    {
                        playCallAudio(type: .Ringing)
                    }
                    else
                    {
                        playCallAudio(type: .Ringing, shouldToggleSpeakerForVideoCall: isCallTypeVideo)
                    }
                    self.delegate?.didUpdateCurrentCallStatus(type: .Ringing)
                    cancelAllTimers()
                    initateCallTimeOut(type: .OutGoingCallNoAnswer)

                    return

                }

                if currentCallType == .Outgoing {
                    initiateSignalling()
                }

                    else {
                        //SEND USER BUSY MESSAGE
//            sendUserBusyInOtherCall(senderId: toID!, receiverId: fromID!)
                }
            }
        }
    }

}
