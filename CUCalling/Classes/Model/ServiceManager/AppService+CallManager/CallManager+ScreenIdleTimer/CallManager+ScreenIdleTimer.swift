//
//  CallManager+AppIdleTimer.swift
//  Nehao
//
//  Created by Abhimanu Jindal on 25/04/17.
//  Copyright © 2017 Amit Tripathi. All rights reserved.
//

import Foundation

extension (CallManager){
    
    func updateScreenIdleTimer(shouldDisable:Bool){
       // screenIdleTimerManager.updateScreenIdleTimer(shouldDisable:shouldDisable)
    }
    
    func handleScreenIdleTimer(isLocalVideoEnabled: Bool, isRemoteVideoEnabled: Bool)
    {
        DispatchQueue.main.async {
            if isLocalVideoEnabled || isRemoteVideoEnabled{
                UIApplication.shared.isIdleTimerDisabled = true
            }
            else{
                UIApplication.shared.isIdleTimerDisabled = false
            }
        }
      
    }

}
