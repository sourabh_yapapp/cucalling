//
//  ScreenIdleTimerManager.swift
//  Nehao
//
//  Created by Abhimanu Jindal on 25/04/17.
//  Copyright © 2017 Amit Tripathi. All rights reserved.
//

import Foundation
import UIKit

class ScreenIdleTimerManager: NSObject{
    
    private var timer:Timer?
    private var idleTimerDisableValue:Bool = false
    private var maxValue = 5
    private var counter = 0
    
    func updateScreenIdleTimer(shouldDisable:Bool){
        self.idleTimerDisableValue = !shouldDisable
        DispatchQueue.main.async {
            self.timer?.invalidate()
            self.timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(self.updateValue), userInfo: nil, repeats: true)
        }
    }
    
    @objc private func updateValue(){
        if counter <= maxValue{
            UIApplication.shared.isIdleTimerDisabled = idleTimerDisableValue
            counter += 1
        }
        else{
            timer?.invalidate()
            counter = 0
        }
    }
    
}
