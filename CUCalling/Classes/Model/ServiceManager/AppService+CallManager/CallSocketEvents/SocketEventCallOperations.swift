//
//  SocketEventCallOperations.swift
//  Nehao
//
//  Created by Abhimanu Jindal on 09/03/17.
//  Copyright © 2017 Amit Tripathi. All rights reserved.
//

import Foundation

class SocketEventCallOperations: YASocketEvent, YASocketAPIDelegate {
    
    enum OperationType:String {
        case CallAccepted = "CallAccepted"
        case CallRejected = "CallRejected"
        case CallMissed = "CallMissed"
        case EndOngoingCall = "EndOngoingCall"
        case CallControlsVideoUpdated = "CallControlsVideoUpdated"
        case CallControlsMicUpdated = "CallControlsMicUpdated"
        case UserBusy = "UserBusy"
        case CallDisconnected = "CallDisconnected"
        case CallHold = "CallHold"
        case CallResume = "CallResume"
        case ContactDeleted
        case CallUserEnteredBackground
        case CallUserEnteredForeground
        case CallConnectionRateUpdated
    }
    
    //MARK:- Overridden Method(S)
    override init() {
        super.init()
        self.eventNames.append("CallAccepted")
        self.eventNames.append("CallRejected")
        self.eventNames.append("CallMissed")
        self.eventNames.append("EndOngoingCall")
        self.eventNames.append("CallControlsVideoUpdated")
        self.eventNames.append("CallControlsMicUpdated")
        self.eventNames.append("UserBusy")
        self.eventNames.append("CallDisconnected")
        self.eventNames.append("CallHold")
        self.eventNames.append("CallResume")
        self.eventNames.append(OperationType.ContactDeleted.rawValue)
        self.eventNames.append(OperationType.CallUserEnteredBackground.rawValue)
        self.eventNames.append(OperationType.CallUserEnteredForeground.rawValue)
        self.eventNames.append(OperationType.CallConnectionRateUpdated.rawValue)

    }
    
    func sendCallOperation(fromId:String, toId: String, data:[String:AnyObject], operationType:SocketEventCallOperations.OperationType){
        var dictionary:[String:AnyObject] = [:]
        dictionary["type"] = operationType.rawValue as AnyObject?
        dictionary["senderId"] = fromId as AnyObject?
        dictionary["receiverId"] = toId as AnyObject?
        dictionary["data"] = data as AnyObject?
        
        self.socketAPI?.sendEvent(event: "new_message",
                                  data: [dictionary])
    }
    
    override func handleSocketEvents(){
        for (_,eventName) in eventNames.enumerated(){
            self.socketAPI?.addEvent(eventName: eventName, delegate: self)
        }
    }
    
    override func removeSocketEvents() {
        for (_,eventName) in eventNames.enumerated(){
            self.socketAPI?.removeEvent(eventName: eventName, delegate: self)
        }
    }
    
    
    //MARK:- YASocketAPI Delegate Method(s)
    func didReceiveEvent(socketAPI: YASocketAPI) {
        
        for (_,eventName) in eventNames.enumerated(){
            if socketAPI.message?.type == eventName{
                self.delegate?.socketEventReceived(socketEvent: self)
            }
        }
    }
    
    func didReceiveEventFailure(socketAPI: YASocketAPI) {
        for (_,eventName) in eventNames.enumerated(){
            if socketAPI.message?.type == eventName{
                self.delegate?.socketEventFailed(socketEvent: self)
            }
        }
    }
}
