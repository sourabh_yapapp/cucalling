//
//  SocketEventSDP.swift
//  YapAppCallingModule
//
//  Created by Abhimanu Jindal on 13/12/16.
//  Copyright © 2016 YapApp India Pvt. Ltd. All rights reserved.
//

import UIKit

class SocketEventSDP: YASocketEvent, YASocketAPIDelegate {

    //MARK:- Overridden Method(S)
    //MARK:- Overridden Method(S)
    override init() {
        super.init()
        self.eventNames.append("SDP_OFFER")
        self.eventNames.append("SDP_ANSWER")
    }
    
    func sendOfferSDPEvent(fromId:String, toId: String, data:[String:AnyObject]) {
        var dictionary:[String:AnyObject] = [:]
        dictionary["type"] = "SDP_OFFER" as AnyObject?
        dictionary["senderId"] = fromId as AnyObject?
        dictionary["receiverId"] = toId as AnyObject?
        dictionary["data"] = data as AnyObject?
        
        self.socketAPI?.sendEvent(event: "new_message",
                                  data: [dictionary])
    }
    
    func sendAnswerSDPEvent(fromId:String, toId: String, data:[String:AnyObject]) {
        var dictionary:[String:AnyObject] = [:]
        dictionary["type"] = "SDP_ANSWER" as AnyObject?
        dictionary["senderId"] = fromId as AnyObject?
        dictionary["receiverId"] = toId as AnyObject?
        dictionary["data"] = data as AnyObject?
        
        self.socketAPI?.sendEvent(event: "new_message",
                                  data: [dictionary])
    }
    
    
    override func handleSocketEvents(){
        for (_,eventName) in eventNames.enumerated(){
            self.socketAPI?.addEvent(eventName: eventName, delegate: self)
        }
    }
    
    override func removeSocketEvents() {
        for (_,eventName) in eventNames.enumerated(){
            self.socketAPI?.removeEvent(eventName: eventName, delegate: self)
        }
    }
    
    
    //MARK:- YASocketAPI Delegate Method(s)
    func didReceiveEvent(socketAPI: YASocketAPI) {
        
        for (_,eventName) in eventNames.enumerated(){
            if socketAPI.message?.type == eventName{
                self.delegate?.socketEventReceived(socketEvent: self)
            }
        }
    }
    
    func didReceiveEventFailure(socketAPI: YASocketAPI) {
        for (_,eventName) in eventNames.enumerated(){
            if socketAPI.message?.type == eventName{
                self.delegate?.socketEventFailed(socketEvent: self)
            }
        }
    }
}
