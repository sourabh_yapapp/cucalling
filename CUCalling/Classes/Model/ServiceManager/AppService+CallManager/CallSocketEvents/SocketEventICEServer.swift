//
//  SocketEventICEServer.swift
//  YapAppCallingModule
//
//  Created by Abhimanu Jindal on 14/12/16.
//  Copyright © 2016 YapApp India Pvt. Ltd. All rights reserved.
//

import UIKit

class SocketEventICEServer: YASocketEvent, YASocketAPIDelegate {

    //MARK:- Overridden Method(S)
    override init() {
        super.init()
        self.eventNames.append("getIceServer")
    }
    
    func getICEServers() {
        var dictionary:[String:AnyObject] = [:]
        dictionary["type"] = "getIceServer" as AnyObject?
        self.socketAPI?.sendEvent(event: "new_message",
                                  data: [dictionary])
    }
    
    override func handleSocketEvents(){
        for (_,eventName) in eventNames.enumerated(){
            self.socketAPI?.addEvent(eventName: eventName, delegate: self)
        }
    }
    
    override func removeSocketEvents() {
        for (_,eventName) in eventNames.enumerated(){
            self.socketAPI?.removeEvent(eventName: eventName, delegate: self)
        }
    }
    
    
    //MARK:- YASocketAPI Delegate Method(s)
    func didReceiveEvent(socketAPI: YASocketAPI) {
        
        for (_,eventName) in eventNames.enumerated(){
            if socketAPI.message?.type == eventName{
                self.delegate?.socketEventReceived(socketEvent: self)
            }
        }
    }
    
    func didReceiveEventFailure(socketAPI: YASocketAPI) {
        for (_,eventName) in eventNames.enumerated(){
            if socketAPI.message?.type == eventName{
                self.delegate?.socketEventFailed(socketEvent: self)
            }
        }
    }
}
