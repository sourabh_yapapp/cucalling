//
//  SocketEventICECandidates.swift
//  Nehao
//
//  Created by Abhimanu Jindal on 07/03/17.
//  Copyright © 2017 Amit Tripathi. All rights reserved.
//

import Foundation

class SocketEventICECandidates: YASocketEvent, YASocketAPIDelegate {
    
    //MARK:- Overridden Method(S)
    override init() {
        super.init()
        self.eventNames.append("ICECandidate")
    }
    
    func sendICECandidateEvent(fromId:String, toId: String, data:[String:AnyObject]) {
        var dictionary:[String:AnyObject] = [:]
        dictionary["type"] = "ICECandidate" as AnyObject?
        dictionary["senderId"] = fromId as AnyObject?
        dictionary["receiverId"] = toId as AnyObject?
        dictionary["data"] = data as AnyObject?
        
        self.socketAPI?.sendEvent(event: "new_message",
                                  data: [dictionary])
    }
    
    override func handleSocketEvents(){
        for (_,eventName) in eventNames.enumerated(){
            self.socketAPI?.addEvent(eventName: eventName, delegate: self)
        }
    }
    
    override func removeSocketEvents() {
        for (_,eventName) in eventNames.enumerated(){
            self.socketAPI?.removeEvent(eventName: eventName, delegate: self)
        }
    }
    
    
    //MARK:- YASocketAPI Delegate Method(s)
    func didReceiveEvent(socketAPI: YASocketAPI) {
        
        for (_,eventName) in eventNames.enumerated(){
            if socketAPI.message?.type == eventName{
                self.delegate?.socketEventReceived(socketEvent: self)
            }
        }
    }
    
    func didReceiveEventFailure(socketAPI: YASocketAPI) {
        for (_,eventName) in eventNames.enumerated(){
            if socketAPI.message?.type == eventName{
                self.delegate?.socketEventFailed(socketEvent: self)
            }
        }
    }
}
