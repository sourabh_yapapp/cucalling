//
//  SocketEventCalPingHandshake.swift
//  Nehao
//
//  Created by Tina Gupta on 18/04/17.
//  Copyright © 2017 Amit Tripathi. All rights reserved.
//

import Foundation

class SocketEventCallPingHandshake: YASocketEvent, YASocketAPIDelegate {
    
    
    enum PingHandshakeType:String {
        case PingSender =   "PingSender"
        case PongSender =   "PongSender"
        case PingReceiver = "PingReceiver"
        case PongReceiver = "PongReceiver"
    }
    
    //MARK:- Overridden Method(S)
    override init() {
        super.init()
        self.eventNames.append("PingSender")
        self.eventNames.append("PongSender")
        self.eventNames.append("PingReceiver")
        self.eventNames.append("PongReceiver")
    }
    
        
    func sendPingEvent(fromId:String, toId: String, data:[String:AnyObject], pingHandshakeType:SocketEventCallPingHandshake.PingHandshakeType){
        var dictionary:[String:AnyObject] = [:]
        dictionary["type"] = pingHandshakeType.rawValue as AnyObject?
        dictionary["senderId"] = fromId as AnyObject?
        dictionary["receiverId"] = toId as AnyObject?
        dictionary["data"] = data as AnyObject?
        
        self.socketAPI?.sendEvent(event: "new_message",
                                  data: [dictionary])
    }
    
    override func handleSocketEvents(){
        for (_,eventName) in eventNames.enumerated(){
            self.socketAPI?.addEvent(eventName: eventName, delegate: self)
        }
    }
    
    override func removeSocketEvents() {
        for (_,eventName) in eventNames.enumerated(){
            self.socketAPI?.removeEvent(eventName: eventName, delegate: self)
        }
    }
    
    
    //MARK:- YASocketAPI Delegate Method(s)
    func didReceiveEvent(socketAPI: YASocketAPI) {
        
        for (_,eventName) in eventNames.enumerated(){
            if socketAPI.message?.type == eventName{
                self.delegate?.socketEventReceived(socketEvent: self)
            }
        }
    }
    
    func didReceiveEventFailure(socketAPI: YASocketAPI) {
        for (_,eventName) in eventNames.enumerated(){
            if socketAPI.message?.type == eventName{
                self.delegate?.socketEventFailed(socketEvent: self)
            }
        }
    }
}
