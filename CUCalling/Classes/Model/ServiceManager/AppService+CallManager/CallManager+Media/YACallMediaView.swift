//
//  YACallMediaView.swift
//  YapAppCallingModule
//
//  Created by Abhimanu Jindal on 19/12/16.
//  Copyright © 2016 YapApp India Pvt. Ltd. All rights reserved.
//

import UIKit
import WebRTC

class YACallMediaView:NSObject {
    var view:UIView?
    var type:ViewType?
    var rtcView:RTCEAGLVideoView?
    var cameraPreviewView:RTCCameraPreviewView?
    var isRendered:Bool?
    var isAspectFillRequired:Bool = true
}

