//
//  CallMediaControlStateModel.swift
//  Nehao
//
//  Created by Tina Gupta on 19/06/17.
//  Copyright © 2017 Amit Tripathi. All rights reserved.
//

import UIKit

class CallMediaControlStateModel: NSObject {

    var remoteVideoEnabled: Bool = true
    var remoteMicEnabled: Bool = true
    var localVideoEnabled:Bool = true
    var localMicEnabled: Bool = true
    
    override init()
    {
        super.init()
    }
    
    func setValues(localMic: Bool, localVideo: Bool, remoteMic: Bool, remoteVideo: Bool)
    {
         remoteVideoEnabled = remoteVideo
         remoteMicEnabled = remoteMic
         localVideoEnabled = localVideo
         localMicEnabled = localMic
    }
    
}
