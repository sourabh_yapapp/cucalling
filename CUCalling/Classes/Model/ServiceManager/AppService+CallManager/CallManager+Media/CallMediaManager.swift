//
//  CallManager.swift
//  Nehao
//
//  Created by Abhimanu Jindal on 01/03/17.
//  Copyright © 2017 Amit Tripathi. All rights reserved.
//

import Foundation
import UIKit
import WebRTC

protocol CallMediaManagerDelegate:class {
    func viewDidChangeSize(mediaView:YACallMediaView, size:CGSize)
    func mediaDidChangeTrack(isLocal:Bool, isAudio:Bool, isEnabled:Bool)
}

class CallMediaManager: NSObject, RTCEAGLVideoViewDelegate {
    
    weak var delegate:CallMediaManagerDelegate?
    private var cameraPreviewView:RTCCameraPreviewView?
    var mediaTracks:[YACallMediaTrack] = []
    var mediaViews:[YACallMediaView] = []
    
    override init(){
        super.init()
        setUpPreviewLayer()
    }
    
    //MARK:- Public Method(s)
    
    //MARK:... Media Stream Method(s)
    func setUpMediaViews(views: [YACallMediaView]) {
        for (_,mediaView) in views.enumerated(){
            setupMediaView(mediaView: mediaView)
        }
    }
    
    func removeMediaViews(views: [YACallMediaView]) {
        for (_,mediaView) in views.enumerated(){
            removeMediaView(mediaView: mediaView)
        }
    }
    
    func removeAllMediaViews() {
        //self.removeMediaViews(views: self.mediaViews)
        if self.mediaViews != nil {
            if self.mediaViews.count > 0{
                self.mediaViews.removeAll()
            }
        }
    }
    
    //MARK:... Media Track Method(s)
    
    func processRTCMediaTrackUpdate(mediaTrack:YACallMediaTrack?){
        if mediaTrack?.updateType == .add{
            if mediaTrack?.type == .local{
                processReceivedLocalMediaTrack(mediaTrack: mediaTrack)
            }
            if mediaTrack?.type == .remote{
                processReceivedRemoteMediaTrack(mediaTrack: mediaTrack)
            }
        }
        if mediaTrack?.updateType == .remove{
            if mediaTrack?.type == .local{
                processRemovedLocalMediaTrack(mediaTrack: mediaTrack)
            }
            if mediaTrack?.type == .remote{
                processRemovedRemoteMediaTrack(mediaTrack: mediaTrack)
            }
        }
    }

    
    func removeAllMediaTracks(){
        mediaTracks.removeAll()
    }
    
    func toggleTrack(isEnabled:Bool, isAudio:Bool, isLocal:Bool){
        
        var trackType:TrackType?
        var type:ViewType?
        
        if isAudio{
            trackType = .audio
        }
        else{
            trackType = .video
        }
        
        if isLocal{
            type = .local
        }
        else{
            type = .remote
        }
        
        let mediaTrack:YACallMediaTrack? = getMediaTrack(type: type!, trackType: trackType!)
        
        if mediaTrack != nil{
            enableDisableTrack(mediaTrack: mediaTrack!, isEnabled: isEnabled)
            delegate?.mediaDidChangeTrack(isLocal: isLocal, isAudio: isAudio, isEnabled: isEnabled)

        }
        else{
            delegate?.mediaDidChangeTrack(isLocal: isLocal, isAudio: isAudio, isEnabled: !isEnabled)
        }
        
    }
    
    func isTrackEnabled(isAudio:Bool, isLocal:Bool)->Bool{
        var trackType:TrackType?
        var type:ViewType?
        
        if isAudio{
            trackType = .audio
        }
        else{
            trackType = .video
        }
        
        if isLocal{
            type = .local
        }
        else{
            type = .remote
        }
        
        for (_,element) in self.mediaTracks.enumerated(){
            if element.trackType == trackType && element.type == type{
                if trackType == .video{
                    let videoTrack:RTCVideoTrack = element.videoTrack as! RTCVideoTrack
                    return videoTrack.isEnabled
                }
                else{
                    let audioTrack:RTCAudioTrack = element.audioTrack as! RTCAudioTrack
                    return audioTrack.isEnabled
                }
            }
        }
        return false
    }
    
    func toggleCamera(){
        for (_,element) in self.mediaTracks.enumerated(){
            if element.trackType == .video && element.type == .local{
                let videTrack = element.videoTrack as! RTCVideoTrack
                let source = videTrack.source as! RTCAVFoundationVideoSource
                if element.isFrontCamera{
                    if source.canUseBackCamera{
                        source.useBackCamera = true
                    }
                }
                else{
                    source.useBackCamera = false
                }
                element.isFrontCamera = !source.useBackCamera
            }
        }
    }
    
    //MARK:... Media Session Method(s)
    func endCallMediaSession(){
        self.removeAllMediaViews()
        self.removeAllMediaTracks()
        cleanupPreviewLayer()
    }
    
    //MARK:- Private Method(s)
    
    //MARK:... Media Track Method(s)
    
    //MARK:... RTC MediaTrack Process Method(s)
    
    private func processReceivedLocalMediaTrack(mediaTrack: YACallMediaTrack?) {
        YALog.print("didReceiveLocalMediaTrack")
        let index = self.mediaTracks.index { (mediaTrackObj) -> Bool in
            return mediaTrackObj === mediaTrack && mediaTrackObj.type == .local
        }
        if index != nil{
            return
        }
        if mediaTrack != nil{
            self.mediaTracks.append(mediaTrack!)
        }
        if mediaTrack?.trackType == .video{
            let videTrack = mediaTrack?.videoTrack as! RTCVideoTrack
            let source = videTrack.source as! RTCAVFoundationVideoSource
            DispatchQueue.main.async {
                self.cameraPreviewView?.captureSession = source.captureSession
            }
        }
        if mediaTrack != nil{
            self.startAllVideoRendering(mediaTrack: mediaTrack!)
        }
        
    }
    
    private func processRemovedLocalMediaTrack(mediaTrack: YACallMediaTrack?) {
        YALog.print("didRemoveRemoteMediaTrack")
        let index = self.mediaTracks.index { (mediaTrackObj) -> Bool in
            if(mediaTrack?.trackType == .video){
                return mediaTrackObj.videoTrack === mediaTrack?.videoTrack && mediaTrackObj.type == .local
            }
            return mediaTrackObj.audioTrack === mediaTrack?.audioTrack && mediaTrackObj.type == .local
            
        }
        if index != nil{
            stopAllVideoRendering(mediaTrack: self.mediaTracks[index!])
            self.mediaTracks.remove(at: index!)
        }
    }
    
    private func processReceivedRemoteMediaTrack(mediaTrack:YACallMediaTrack?){
        YALog.print("didReceiveRemoteVideoTrack")
        let index = self.mediaTracks.index { (mediaTrackObj) -> Bool in
            if(mediaTrack?.trackType == .video){
                return mediaTrackObj.videoTrack === mediaTrack?.videoTrack && mediaTrackObj.type == .remote
            }
            return mediaTrackObj.audioTrack === mediaTrack?.audioTrack && mediaTrackObj.type == .remote
        }
        if index != nil{
            return
        }
        self.mediaTracks.append(mediaTrack!)
        startAllVideoRendering(mediaTrack: mediaTrack!)
    }
    
    private func processRemovedRemoteMediaTrack(mediaTrack:YACallMediaTrack?){
        YALog.print("didRemoveRemoteMediaTrack")
        let index = self.mediaTracks.index { (mediaTrackObj) -> Bool in
            if(mediaTrack?.trackType == .video){
                return mediaTrackObj.videoTrack === mediaTrack?.videoTrack && mediaTrackObj.type == .remote
            }
            return mediaTrackObj.audioTrack === mediaTrack?.audioTrack && mediaTrackObj.type == .remote
            
        }
        if index != nil{
            stopAllVideoRendering(mediaTrack: self.mediaTracks[index!])
            self.mediaTracks.remove(at: index!)
        }
    }
    
    private func getMediaTrack(type:ViewType, trackType:TrackType) -> YACallMediaTrack?{
        for(_,mediaTrack) in self.mediaTracks.enumerated(){
            if mediaTrack.trackType == trackType && mediaTrack.type == type{
                return mediaTrack
            }
        }
        return nil
    }
    
    private func enableDisableTrack(mediaTrack:YACallMediaTrack, isEnabled:Bool){
        if mediaTrack.trackType == nil{return}
        
        if mediaTrack.trackType == .video{
            if mediaTrack.videoTrack == nil{return}
            if let vidTrack = mediaTrack.videoTrack as? RTCVideoTrack{
                let videoTrack:RTCVideoTrack = mediaTrack.videoTrack as! RTCVideoTrack
                videoTrack.isEnabled = isEnabled
            }
          
        }
        else{
            if mediaTrack.audioTrack == nil{return}
            if let audTrack = mediaTrack.audioTrack as? RTCAudioTrack{
                let audioTrack:RTCAudioTrack = mediaTrack.audioTrack as! RTCAudioTrack
                audioTrack.isEnabled = isEnabled
            }
        }
    }
    
    //MARK:... Media View Method(s)
    private func setupMediaView(mediaView: YACallMediaView){
        
        let mediaTrack = getMediaTrack(type: mediaView.type!, trackType: .video)
        for (_,currentMediaView) in self.mediaViews.enumerated(){
            if currentMediaView.type == mediaView.type{
                if let track = mediaTrack{
                    stopVideoRendering(mediaView: currentMediaView, mediaTrack: track)
                }
                removeRTCView(mediaView: currentMediaView)
            }
        }
        
        addRTCView(mediaView: mediaView)
        
        if let track = mediaTrack{
            startVideoRendering(mediaView: mediaView, mediaTrack: track)
        }
    }
    private func removeMediaView(mediaView: YACallMediaView){
        
        let mediaTrack = getMediaTrack(type: mediaView.type!, trackType: .video)
        if self.mediaViews.count == 0{
            return
        }
        for (_,currentMediaView) in self.mediaViews.enumerated(){
            if currentMediaView.type == mediaView.type{
                if let track = mediaTrack{
                    stopVideoRendering(mediaView: currentMediaView, mediaTrack: track)
                }
                removeRTCView(mediaView: currentMediaView)
            }
        }
    }
    
    private func setUpPreviewLayer(){
        if cameraPreviewView == nil{
            cameraPreviewView = RTCCameraPreviewView.init()
        }
    }
    
    private func cleanupPreviewLayer(){
        if let previewView = cameraPreviewView{
            let previewLayer = previewView.layer as! AVCaptureVideoPreviewLayer
            previewLayer.removeFromSuperlayer()
            cameraPreviewView?.subviews.forEach { $0.removeFromSuperview() }
//            cameraPreviewView?.removeSubviews()
            cameraPreviewView = nil
        }
    }
    
    //MARK:... Video Rendering Method(s)
    private func startVideoRendering(mediaView: YACallMediaView, mediaTrack: YACallMediaTrack){
        if mediaTrack.trackType != .video{
            return
        }
        if mediaTrack.type == .local{
            if let cameraPreviewView = cameraPreviewView{
                let videTrack = mediaTrack.videoTrack as! RTCVideoTrack
                let source = videTrack.source as! RTCAVFoundationVideoSource
                cameraPreviewView.captureSession = source.captureSession
                
                let layer = cameraPreviewView.layer as! AVCaptureVideoPreviewLayer
                if mediaView.isAspectFillRequired{
                    layer.videoGravity = AVLayerVideoGravity.resizeAspectFill
                }
                else{
                    layer.videoGravity = AVLayerVideoGravity.resizeAspect
                }
                
                if mediaView.view?.layer != nil{
                    if (mediaView.view?.layer.bounds) != nil
                    {
                        layer.bounds = (mediaView.view?.layer.bounds)!
                    }

                }
                mediaView.isRendered = true
                
            }
        }
        else if mediaTrack.type == .remote{
            let videoTrack = mediaTrack.videoTrack as! RTCVideoTrack
            if let rtcView = mediaView.rtcView{
                videoTrack.add(rtcView)
                mediaView.isRendered = true
            }
        }
    }
    
    private func stopVideoRendering(mediaView: YACallMediaView, mediaTrack:YACallMediaTrack){
        if mediaTrack.trackType != .video{
            return
        }
        if mediaTrack.type == .local{
            let videTrack = mediaTrack.videoTrack as! RTCVideoTrack
            let _ = videTrack.source as! RTCAVFoundationVideoSource
            //            source.captureSession.stopRunning()
            mediaView.isRendered = false
        }
        else if mediaTrack.type == .remote{
            let videoTrack = mediaTrack.videoTrack as! RTCVideoTrack
            if mediaView.rtcView != nil{
                //videoTrack.remove(mediaView.rtcView!)
                mediaView.isRendered = false
            }
        }
    }
    
    //MARK:... RTC View Method(s)
    func addRTCView(mediaView:YACallMediaView){
        setUpPreviewLayer()
        if mediaView.type == .remote{
            if !self.mediaViews.contains(mediaView){
                let rtcEAGLView = RTCEAGLVideoView.init(frame: CGRect(x: 0, y: 0, width: (mediaView.view?.frame.size.width)!, height: (mediaView.view?.frame.size.height)!))
                rtcEAGLView.delegate = self
                rtcEAGLView.contentMode = .scaleAspectFill
                YALog.print("RTEAGLView Frame - \(rtcEAGLView.frame)")
                mediaView.rtcView = rtcEAGLView
                mediaView.view?.addSubview(mediaView.rtcView!)
              
                self.mediaViews.append(mediaView)
            }
        }
        else{
            if !self.mediaViews.contains(mediaView){
                self.cameraPreviewView?.frame = CGRect(x: 0, y: 0, width: (mediaView.view?.frame.size.width)!, height: (mediaView.view?.frame.size.height)!)
                YALog.print("CamerPreview Frame Created - \(String(describing: self.cameraPreviewView?.frame))")
                // mediaView.cameraPreviewView = cameraPreviewView
                mediaView.view?.addSubview(self.cameraPreviewView!)
                self.mediaViews.append(mediaView)
            }
        }
    }
    
    func removeRTCView(mediaView:YACallMediaView){
        if mediaView.type == .remote{
            if let rtcView = mediaView.rtcView{
                rtcView.removeFromSuperview()
                mediaView.rtcView = nil
            }
        }
        if mediaView.type == .local{
            if self.cameraPreviewView != nil{
                self.cameraPreviewView?.removeFromSuperview()
                //self.cameraPreviewView = nil
            }
        }
        
        let index = self.mediaViews.index(where: { (view) -> Bool in
            return view === mediaView
        })
        if index != nil{
            if self.mediaViews.count - 1 >= index!{
                
                if self.mediaViews[index!] != nil{
                    self.mediaViews.remove(at: index!)
                }
            }
            
        }
    }
    
    private func startAllVideoRendering(mediaTrack:YACallMediaTrack){
        if mediaTrack.trackType != .video{
            return
        }
        for (_,view)in self.mediaViews.enumerated(){
            if view.isRendered == true{
                continue
            }
            startVideoRendering(mediaView: view, mediaTrack: mediaTrack)
        }
    }
    
    
    private func stopAllVideoRendering(mediaTrack:YACallMediaTrack){
        if mediaTrack.trackType != .video{
            return
        }
        for (_,view)in self.mediaViews.enumerated(){
            if view.isRendered == false{
                continue
            }
            stopVideoRendering(mediaView: view, mediaTrack: mediaTrack)
        }
    }
    
    
    //MARK:- RTCEAGLE VideoView Delegate Method(s)
    func videoView(_ videoView: RTCEAGLVideoView, didChangeVideoSize size: CGSize) {
        YALog.print("RTC Video View Size Changed")
        for (_,mediaView)in self.mediaViews.enumerated(){
            if mediaView.rtcView === videoView{
                self.delegate?.viewDidChangeSize(mediaView: mediaView, size: size)
            }
        }
    }

}
