//
//  YACallMediaView.swift
//  YapAppCallingModule
//
//  Created by Abhimanu Jindal on 19/12/16.
//  Copyright © 2016 YapApp India Pvt. Ltd. All rights reserved.
//

import UIKit

enum ViewType {
    case unknown
    case local
    case remote
}

enum TrackType {
    case unknown
    case video
    case audio
}

enum UpdateType{
    case unknown
    case add
    case remove
}

class YACallMediaTrack:NSObject{
    var videoTrack:AnyObject?
    var audioTrack:AnyObject?
    var updateType:UpdateType = .unknown
    var type:ViewType?
    var trackType:TrackType?
    var isFrontCamera = true
}
