//
//  CallManager.swift
//  Nehao
//
//  Created by Abhimanu Jindal on 01/03/17.
//  Copyright © 2017 Amit Tripathi. All rights reserved.
//

extension (CallManager):CallMediaManagerDelegate
{
        
    func setUpMediaStream(shouldCreateAudioStream: Bool, shouldCreateVideoStream: Bool){
        rtcManager.setupMediaStream(shouldCreateAudioStream: true, shouldCreateVideoStream: true)
    }
    
    func cleanUpMediaStreams(){
        //rtcManager.removeLocalMediaStream(shouldRemoveAudioStream: true, shouldRemoveVideoStream: true)
        callMediaManager.removeAllMediaViews()
        callMediaManager.removeAllMediaTracks()
    }
    
    func processRTCMediaTrackUpdate(mediaTrack:YACallMediaTrack?){
        callMediaManager.processRTCMediaTrackUpdate(mediaTrack: mediaTrack)
    }
    
    func toggleMic(isMicEnabled:Bool){
        callMediaManager.toggleTrack(isEnabled: isMicEnabled, isAudio: true, isLocal: true)
        updateCallOperation(operationType: .CallControlsMicUpdated, isVideoEnabled: nil, isMicEnabled: isMicEnabled, senderId: currentSenderID, receiverId: currentReceiverID, callType: nil)
    }
    
    func updateAudioVideoStream(isVideoEnabled:Bool){
        callMediaManager.toggleTrack(isEnabled: isVideoEnabled, isAudio: false, isLocal: true)
        updateCallOperation(operationType: .CallControlsVideoUpdated, isVideoEnabled: isVideoEnabled, isMicEnabled: nil, senderId: currentSenderID, receiverId: currentReceiverID, callType: nil)
    }   
    
    func updateRemoteAudioVideoStream(isVideoEnabled:Bool){
        callMediaManager.toggleTrack(isEnabled: isVideoEnabled, isAudio: false, isLocal: false)
    }
    
    func toggleLocalCamera(){
        callMediaManager.toggleCamera()
    }
    
    func setUpStreamCallViews(localView:UIView){
        var views:[YACallMediaView] = []
        let localMediaView = YACallMediaView.init()
        localMediaView.view = localView
        localMediaView.type = .local
        views.append(localMediaView)
        callMediaManager.setUpMediaViews(views: views)
        
    }
    
    func updateStreamCallViews(view:UIView, isLocal:Bool, shouldAdd:Bool){
        
//        callMediaManager.toggleTrack(isEnabled: true, isAudio: true, isLocal: true)
//        callMediaManager.toggleTrack(isEnabled: true, isAudio: true, isLocal: false)
//        callMediaManager.toggleTrack(isEnabled: true, isAudio: false, isLocal: false)
//        callMediaManager.toggleTrack(isEnabled: true, isAudio: false, isLocal: true)
        
        callMediaManager.toggleTrack(isEnabled: currentLocalMicEnableValue, isAudio: true, isLocal: true)
        callMediaManager.toggleTrack(isEnabled: currentRemoteMicEnableValue, isAudio: true, isLocal: false)
        callMediaManager.toggleTrack(isEnabled: currentRemoteVideoEnableValue, isAudio: false, isLocal: false)
        callMediaManager.toggleTrack(isEnabled: currentLocalVideoEnableValue, isAudio: false, isLocal: true)
        self.views.removeAll()
        self.localMediaView.view = view
        if isLocal{
            self.localMediaView.type = .local
        }else{
            self.localMediaView.type = .remote
        }
        self.views.append(self.localMediaView)
       

        if shouldAdd{
            callMediaManager.setUpMediaViews(views: self.views)
        }else{
            callMediaManager.removeMediaViews(views: self.views)
        }
    }
    
    func removeStreamCallViews(localView:UIView){
        var views:[YACallMediaView] = []
        let localMediaView = YACallMediaView.init()
        localMediaView.view = localView
        localMediaView.type = .local
        views.append(localMediaView)
        callMediaManager.removeMediaViews(views: views)
    }
    
    func removeAllStreamViews(){
        callMediaManager.removeAllMediaViews()
    }
    
    
    //MARK:- CallMediaManagerDelegate Method(s)
    func viewDidChangeSize(mediaView: YACallMediaView, size: CGSize) {
        self.delegate?.remoteViewDidChangeSize(size: size)
    }
    
    func mediaDidChangeTrack(isLocal: Bool, isAudio: Bool, isEnabled: Bool) {
        
    }
}


