  //
//  CallProvider.swift
//  Nehao
//
//  Created by Tina Gupta on 24/04/17.
//  Copyright © 2017 Amit Tripathi. All rights reserved.
//

import Foundation
import CallKit

enum CallFailureReason{
    case InvalidMediaAccess
}

  protocol CallkitManagerDelegate:class {
    func callKitActionPerformed(actionType : CallkitManager.CallActionActionType,boolForOtherOperation isSet : Bool)
    func callAnswerFailed(reason : CallFailureReason,callTypeVideo isVideo : Bool)
}


class CallkitManager: NSObject {
    
    enum CallTypeForCallKit {
        case NewIncomingStart
        case NewOutgoingStart
        case OutGoingCallAccepted
        case IncomingCallAccepted
        case IncomingCallRejected
        case EndOngoingCall
        case EndOutgoingNoAnswer
        case EndOutgoingReject
        case EndIncomingMissed
        case UpdateCallToAudio
        case UpdateCallToVideo
        case UpdateContactName
        }
    
    
    enum CallActionActionType {
        case CallAccepted
        case CallRejected
        case CallEnded
        case PutOnHold
        case MuteUpdeated
        case OutgoingCallStarted
        case AudioActivatedForCallKit
        case CallResumed
    }
    
    fileprivate var callProvider: CXProvider 
    weak var delegate: CallkitManagerDelegate?
    private var callController: CXCallController
    var startedCallAction : CXStartCallAction?
    private var currentCallUUID:UUID?
    fileprivate var incomingCallHasVideo : Bool = false
    fileprivate var isReportedByUser = false
    private var callUUIDs :[UUID] = [UUID]()
    fileprivate var isHold = false
    fileprivate var callObserver: CXCallObserver = CXCallObserver()
    
    override init() {
        let providerConfiguration = CXProviderConfiguration(localizedName: "CU")
        providerConfiguration.supportsVideo = true
        providerConfiguration.maximumCallsPerCallGroup = 1
        providerConfiguration.supportedHandleTypes = [.generic]
        
        self.callProvider = CXProvider(configuration: providerConfiguration)
        self.callController = CXCallController()
        super.init()
        self.callProvider.setDelegate(self, queue: DispatchQueue.main)
        callObserver.setDelegate(self, queue: nil)
    }
    
    //MARK:- Public Method(s)
    func reportCall(hasVideo: Bool, callType:CallkitManager.CallTypeForCallKit, currentCallIdentifier:String, currentCallLocalizedName:String){
        
        if callType == .NewIncomingStart{
            let update = getCXUpdate(uniqueIdentifier: currentCallIdentifier, localizedCallerName: currentCallLocalizedName, hasVideo: hasVideo)
            incomingCallHasVideo = hasVideo
            createNewCallUniqueIdentifier()
            callProvider.reportNewIncomingCall(with: currentCallUUID!, update: update) { error in
                YALog.print("Error in reporting New Call - \(error.debugDescription)")
            }
            isReportedByUser = false
            callUUIDs.append(currentCallUUID!)
        }
        
        if callType == .NewOutgoingStart{
            incomingCallHasVideo = hasVideo
            createNewCallUniqueIdentifier()
            requestCallStartAction(uniqueIdentifier: currentCallIdentifier, localizedCallerName: currentCallLocalizedName.decodeEmoji, hasVideo: hasVideo)
            isReportedByUser = false
            callUUIDs.append(currentCallUUID!)

        }
        
        if callType == .OutGoingCallAccepted{
            if self.currentCallUUID != nil{
            callProvider.reportOutgoingCall(with: currentCallUUID!, connectedAt: Date())
            }
        }
        
        if callType == .EndOngoingCall{
            let update = self.getCXUpdate(uniqueIdentifier: currentCallIdentifier, localizedCallerName: currentCallLocalizedName, hasVideo: incomingCallHasVideo)
            if self.currentCallUUID != nil{
            self.callProvider.reportCall(with: self.currentCallUUID!, updated: update)
            }
            requestCallEndAction(uniqueIdentifier: currentCallIdentifier, localizedCallerName: currentCallLocalizedName.decodeEmoji, hasVideo: incomingCallHasVideo)
            isReportedByUser = true
        }
        if callType == .EndOutgoingNoAnswer{
            callProvider.reportCall(with: currentCallUUID!, endedAt: Date(), reason: .unanswered)
            isReportedByUser = true
        }
        
        if callType == .UpdateCallToVideo || callType == .UpdateCallToAudio{
            if currentCallUUID != nil{
            let update = self.getCXUpdate(uniqueIdentifier: currentCallIdentifier, localizedCallerName: currentCallLocalizedName, hasVideo: hasVideo)
                incomingCallHasVideo = hasVideo
            callProvider.reportCall(with: currentCallUUID!, updated: update)
            }
        }
        if callType == .UpdateContactName{
            if currentCallUUID != nil{
                let update = self.getCXUpdate(uniqueIdentifier: currentCallIdentifier, localizedCallerName: currentCallLocalizedName, hasVideo: hasVideo)
                incomingCallHasVideo = hasVideo
                callProvider.reportCall(with: currentCallUUID!, updated: update)
            }
        }
        
    }
    
    func resetCallKit()
    {
        for callUUID in self.callUUIDs
        {
            let endCallAction = CXEndCallAction(call: callUUID)
            let transaction = CXTransaction()
            transaction.addAction(endCallAction)
            requestTransaction(transaction)
            self.callUUIDs.removeAll()
//            callUUIDs.removeAll(callUUID)
            YALog.print("***************CALL UUID HANDLED \(callUUID)******************************")
        }
    }
    
    
    //MARK:- Private Method(s)
    
    //MARK:.... CX Generic Method(s)
    private func getCXUpdate(uniqueIdentifier:String, localizedCallerName:String, hasVideo:Bool)->CXCallUpdate{
        let update = CXCallUpdate()
        update.remoteHandle = getCXHandle(uniqueIdentifier: uniqueIdentifier)
        update.localizedCallerName = localizedCallerName.decodeEmoji
        update.hasVideo = hasVideo
        update.supportsHolding = true
        return update
    }
    
    private func getCXHandle(uniqueIdentifier:String)->CXHandle{
        return CXHandle(type: .generic, value: uniqueIdentifier)
    }
    
    private func createNewCallUniqueIdentifier(){
        currentCallUUID = UUID()
    }
    
    private func requestTransaction(_ transaction: CXTransaction) {
        callController.request(transaction) { error in
            if let error = error {
                print("Error requesting transaction: \(error)")
            } else {
                print("Requested transaction successfully \(transaction.actions)")
            }
        }
    }
    
    //MARK:... Call Action Method(s)
    private func requestCallStartAction(uniqueIdentifier:String, localizedCallerName:String, hasVideo:Bool){
        if self.currentCallUUID != nil{
        let remoteHandle = getCXHandle(uniqueIdentifier: uniqueIdentifier)
        let startCallAction = CXStartCallAction(call: currentCallUUID!, handle: remoteHandle)
        startCallAction.contactIdentifier = uniqueIdentifier
        startCallAction.isVideo = hasVideo
        
        let transaction = CXTransaction()
        transaction.addAction(startCallAction)
        requestTransaction(transaction)
        
        self.startedCallAction = nil
        self.startedCallAction = startCallAction
        
        let update = self.getCXUpdate(uniqueIdentifier: uniqueIdentifier, localizedCallerName: localizedCallerName, hasVideo: hasVideo)
        self.callProvider.reportCall(with: self.currentCallUUID!, updated: update)
        }
        
        else{
        isReportedByUser = false
        }
    }
    
    private func requestCallEndAction(uniqueIdentifier:String, localizedCallerName:String, hasVideo:Bool){
        if self.currentCallUUID != nil{
            if callUUIDs.contains(currentCallUUID!)
            {
                callUUIDs.remove(at: callUUIDs.index(of: currentCallUUID!)!)
//                callUUIDs.remove(at: (callUUIDs.index(of: currentCallUUID!))[0])
            }
            YALog.print("***************CALL UUID HANDLED \(currentCallUUID)******************************")

        let endCallAction = CXEndCallAction(call: currentCallUUID!)
        let transaction = CXTransaction()
        transaction.addAction(endCallAction)
        requestTransaction(transaction)
            incomingCallHasVideo = false
            self.currentCallUUID = nil
        }
            }
    
    //MARK:- Checking and Handling Media Access
    func checkAccessForMedia(isVideoCall: Bool,completionBlock:@escaping(Bool,Utilities.MediaAccessType)->Void)
    {
        if isVideoCall
        {
            Utilities.checkMediaAccessForVideoCall(completionBlock: { (isGranted, accessType) in
                completionBlock(isGranted, accessType)
            })
        }
        else
        {
            Utilities.checkMediaAccessForAudioCall(completionBlock: { (isGranted, accessType) in
                completionBlock(isGranted, accessType)
            })
        }
    }
    
    
    
}

//MARK:- CXProviderDelegate Delegate Method(s)
extension (CallkitManager): CXProviderDelegate
{
    func providerDidReset(_ provider: CXProvider)
    {
    }
    
    //MARK:... Action Delegate Methods
    func provider(_ provider: CXProvider, perform action: CXAnswerCallAction) {

        checkAccessForMedia(isVideoCall: incomingCallHasVideo) { (isGranted, accessType) in
            if isGranted
            {
                DispatchQueue.main.async {

                self.delegate?.callKitActionPerformed(actionType: .CallAccepted, boolForOtherOperation: self.incomingCallHasVideo)
//                Analytics.sharedInstance.logEvent(eventName: AnalyticsConstants.EventName.CALL_RECEIVE)
                if self.incomingCallHasVideo{
//                    Analytics.sharedInstance.logEvent(eventName: AnalyticsConstants.EventName.VIDEO_CALL_RECEIVE)
                }
                else{
//                    Analytics.sharedInstance.logEvent(eventName: AnalyticsConstants.EventName.AUDIO_CALL_RECEIVE)
                }
                action.fulfill()
                }
            }
            else{
                 self.delegate?.callAnswerFailed(reason: .InvalidMediaAccess, callTypeVideo: self.incomingCallHasVideo)
                Utilities.showNoAccessAlert(isVideoCall: self.incomingCallHasVideo,accessType: accessType,isIncomingCall: true)
            }
        }
        
    }
    
    func provider(_ provider: CXProvider, perform action: CXStartCallAction) {
        self.delegate?.callKitActionPerformed(actionType: .OutgoingCallStarted, boolForOtherOperation: false)

        provider.reportOutgoingCall(with: action.uuid, startedConnectingAt: Date())
        action.fulfill()
    }
    
    func provider(_ provider: CXProvider, perform action: CXEndCallAction) {
        action.fulfill(withDateEnded: Date())
        if isReportedByUser == false{
            self.delegate?.callKitActionPerformed(actionType: .CallEnded, boolForOtherOperation: false)
        }
        isReportedByUser = false
    }
    
    func provider(_ provider: CXProvider, perform action: CXSetHeldCallAction){
        if action.isOnHold
        {
            self.delegate?.callKitActionPerformed(actionType: .PutOnHold, boolForOtherOperation: self.incomingCallHasVideo)
        }
        else{
            self.delegate?.callKitActionPerformed(actionType: .CallResumed, boolForOtherOperation: self.incomingCallHasVideo)
        }
            action.fulfill()
    }
    
    func provider(_ provider: CXProvider, perform action: CXSetMutedCallAction){
        self.delegate?.callKitActionPerformed(actionType: .MuteUpdeated, boolForOtherOperation: action.isMuted)
        action.fulfill()

    }
    
    //MARK:... Audio Methods
    
    func provider(_ provider: CXProvider, didActivate audioSession: AVAudioSession) {
        self.delegate?.callKitActionPerformed(actionType: .AudioActivatedForCallKit, boolForOtherOperation: false)
        YALog.print("####### ACTIVATED SESSION ######")
    }
    
    func provider(_ provider: CXProvider, didDeactivate audioSession: AVAudioSession) {
        YALog.print("####### DEACTIVATED SESSION ######")
    }
    
}


  extension (CallkitManager): CXCallObserverDelegate{
    func callObserver(_ callObserver: CXCallObserver, callChanged call: CXCall){
        if call.hasEnded == true && call.uuid != currentCallUUID && currentCallUUID != nil{
            print("Disconnected")
            self.delegate?.callKitActionPerformed(actionType: .CallResumed, boolForOtherOperation: self.incomingCallHasVideo)

        }
    }
  }


       
