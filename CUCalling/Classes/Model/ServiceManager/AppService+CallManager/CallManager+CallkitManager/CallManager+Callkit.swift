//
//  CallManager+CallProvider.swift
//  Nehao
//
//  Created by Tina Gupta on 25/04/17.
//  Copyright © 2017 Amit Tripathi. All rights reserved.
//

import Foundation
extension (CallManager): CallkitManagerDelegate {
    
    //MARK:- Public Method(s)
    
    func reportCall(hasVideo: Bool, callType:CallkitManager.CallTypeForCallKit, currentCallIdentifier:String, currentCallLocalizedName:String){
        DispatchQueue.main.async {
            self.callkitManager.reportCall(hasVideo: hasVideo, callType: callType, currentCallIdentifier: currentCallIdentifier, currentCallLocalizedName: currentCallLocalizedName)
            if callType == .EndOngoingCall
            {
                self.disableProximitySensor()
            }
            
            if callType == .NewOutgoingStart
            {
                self.handleProximitySensor(isLocalVideoEnabled: hasVideo, isRemoteVideoEnabled: false)
                self.handleScreenIdleTimer(isLocalVideoEnabled: hasVideo, isRemoteVideoEnabled: false)
            }
        }
    }
 
    
   
    //MARK:- CallKitManager Delegate Method(s)
    
    func callKitActionPerformed(actionType : CallkitManager.CallActionActionType, boolForOtherOperation isSet : Bool)
    {
        switch actionType {
        case .CallAccepted:
            
            self.stopAllAudio()
            callAudioManager.setUpAudioSessionCategory(category: AVAudioSessionCategoryPlayAndRecord)

            self.currentCallType = .Ongoing
            if self.isSlowNetwork{
                self.currentCallType = .OngoingIn
            }
            self.updateCallOperation(operationType: .CallAccepted, isVideoEnabled: isSet, isMicEnabled: nil, senderId: self.currentSenderID, receiverId: self.currentReceiverID, callType: nil)
             if UIApplication.shared.applicationState != .active{
            self.updateStreamCallViews(view: UIView(), isLocal: true, shouldAdd: true)
            }
        case .CallRejected:
            break
        case .CallEnded:
            self.endCallKitButtonTapped()
            self.disableProximitySensor()
        case .MuteUpdeated:
            self.callMuteUpdated(isMute: isSet)
            break
        case .PutOnHold:
            self.callSetOnHold(isVideoCall: isSet)
            break
        case .OutgoingCallStarted:
            YALog.print("")
        case .AudioActivatedForCallKit:
            self.sharedAudioSession.start()
            self.audioSessionActivatedByCallkit()
            
        case .CallResumed:
            self.callResumed(isVideoCall: isSet)
        }
    }
    
    func callAnswerFailed(reason : CallFailureReason,callTypeVideo isVideo : Bool) {
            self.incominCallAnsweringFailed(reason: reason, callTypeVideo: isVideo)
    }
}


