//
//  CallManager+NetBitRate.swift
//  Nehao
//
//  Created by Abhimanu Jindal on 29/05/17.
//  Copyright © 2017 Amit Tripathi. All rights reserved.
//

import Foundation

extension (CallManager):InternetBitRateManagerDelegate{
    
    func setUpNetworkBitRate(){
        internetBitRateManager.setUpNetworkBitRate()
    }
    
    func currentBandwidthSpeed()->InternetBitRateManager.BandwidthStrengthType{
        return internetBitRateManager.getCurrentBandWidthStrength()
    }
    
    //MARK: Delegat Method(s)
    func bandwidthDidChange(newBandWidth: InternetBitRateManager.BandwidthStrengthType) {
        YALog.print("############## NEW BANDWIDTH \(newBandWidth) ##########")
    }
    
}
