//
//  InternetBitRateWSManager.swift
//  Nehao
//
//  Created by Abhimanu Jindal on 29/05/17.
//  Copyright © 2017 Amit Tripathi. All rights reserved.
//
import UIKit

class InternetBitRateWSManager: NSObject, URLSessionDataDelegate{
    
    private var startTime: CFAbsoluteTime = 0
    private var stopTime: CFAbsoluteTime = 0
    private var bytesReceived: Int = 0
    private var completionHandler:((Float, YAError?)->Void)?
    private var dataTask:URLSessionDataTask?
    
    func getCurrentBitRate(completionHandler:@escaping (_ bitRate: Float, _ error:YAError?)->Void){
        if dataTask?.state == .running{
            return
        }
        self.completionHandler = completionHandler
        callReferenceWebSite()
    }
    
    //MARK:- Private Function(s)
    private func callReferenceWebSite(){
        
        startTime = CFAbsoluteTimeGetCurrent()
        stopTime = startTime
        bytesReceived = 0
        let configuration = URLSessionConfiguration.ephemeral
        configuration.allowsCellularAccess = true
        configuration.requestCachePolicy = .reloadIgnoringLocalAndRemoteCacheData
        configuration.timeoutIntervalForRequest = 7.0
        
        let defaultSession = URLSession(configuration: configuration, delegate: self, delegateQueue: OperationQueue.main)
        let url = NSURL(string: "https://www.google.com")
        dataTask = defaultSession.dataTask(with: url as URL!)
        dataTask?.resume()
    }
    
    //MARK:- Delegate Method(s)
    
    func urlSession(_ session: URLSession, task: URLSessionTask, didCompleteWithError error: Error?) {
        
        let elapsed = stopTime - startTime
        
        if elapsed == 0 || error != nil{
            self.completionHandler!(0, YAError.generateError(description: "Error", localizedDescription: "Error", debugDescription: "Error", code: 404))
            return
        }
        
        let speed = elapsed != 0 ? Double(bytesReceived * 8) / elapsed / 1000.00 : -1
        self.completionHandler!(Float(speed), nil)

    }
    
    public func urlSession(_ session: URLSession, dataTask: URLSessionDataTask, didReceive data: Data){
        bytesReceived += data.count
        stopTime = CFAbsoluteTimeGetCurrent()
    }

}
