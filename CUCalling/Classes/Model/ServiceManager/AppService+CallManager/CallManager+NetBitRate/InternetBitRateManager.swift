//
//  InternetBitRateManager.swift
//  Nehao
//
//  Created by Abhimanu Jindal on 29/05/17.
//  Copyright © 2017 Amit Tripathi. All rights reserved.
//

protocol InternetBitRateManagerDelegate: class {
    func bandwidthDidChange(newBandWidth:InternetBitRateManager.BandwidthStrengthType)
}

class InternetBitRateManager{
    
    //MARK: Public Variables
    enum BandwidthStrengthType{
        case strong, high, medium, low, poor, unavailable
    }
    
    weak var delegate:InternetBitRateManagerDelegate?
    
    //MARK: Private Variables
    private lazy var wsManager = InternetBitRateWSManager()
    
    private var maxReAttemptCount = 3
    private var reAttemptCount = 0
    
    private var maxTuneCount = 2
    private var tuneCount = 0
    
    private var currentBandwidth:BandwidthStrengthType = .unavailable
    
    private var updateTimer:Timer?
    
    //MARK: Public Method(s)
    func setUpNetworkBitRate(){
       wsManager.getCurrentBitRate { (bitRate, error) in
            if error != nil{
                YALog.print("##### Error in Fetching Bit Rate ####")
                self.processReceivedBitRate(bitRate: 1)
                return
            }
            self.processReceivedBitRate(bitRate: bitRate)
        }
    }
    
    func getCurrentBandWidthStrength()->BandwidthStrengthType{
        return currentBandwidth
    }
    
    //MARK:- Private Method(s)
    
    //MARK:.... Process Method(s)
    private func processReceivedBitRate(bitRate:Float){
        if bitRate == 0.0{
            YALog.print("##### Bit Rate recevied is 0 ####")
            reAttemptBitRateSetup()
            return
        }
        resetReAttemptBitRate()
        setupCurrentBandwidthRange(bitRate:bitRate)
        tuneBitRate()
    }
    
    private func setupCurrentBandwidthRange(bitRate:Float){
        var newBandwidth:BandwidthStrengthType = .unavailable
        
        if bitRate >= 43{
            newBandwidth = .strong
        }
        if bitRate >= 32 && bitRate <= 42{
            newBandwidth = .high
        }
        if bitRate >= 21 && bitRate <= 31{
            newBandwidth = .medium
        }
        if bitRate >= 10 && bitRate <= 20{
            newBandwidth = .low
        }
        if bitRate <= 9{
            newBandwidth = .poor
        }
        
        if currentBandwidth != newBandwidth{
            currentBandwidth = newBandwidth
            self.delegate?.bandwidthDidChange(newBandWidth: currentBandwidth)
        }
        
    }
    
    //MARK:.... Tune Method(s)
    private func tuneBitRate(){
        if tuneCount < maxTuneCount{
            tuneCount = tuneCount + 1
            setUpNetworkBitRate()
        }
        else{
            resetTuneBitRate()
            updateBitRateTimer()
        }
    }
    
    private func resetTuneBitRate(){
        tuneCount = 0
    }
    
    //MARK:... ReAttempt Method(s)
    private func reAttemptBitRateSetup(){
        if reAttemptCount < maxReAttemptCount{
            reAttemptCount = reAttemptCount + 1
            setUpNetworkBitRate()
        }
        else{
            resetReAttemptBitRate()
            currentBandwidth = .unavailable
        }
    }
    
    private func resetReAttemptBitRate(){
        reAttemptCount = 0
    }
    
    //MARK:... Timer
    private func updateBitRateTimer(){
        DispatchQueue.main.async {
            self.updateTimer?.invalidate()
            self.updateTimer = Timer.scheduledTimer(withTimeInterval: 60, repeats: false, block: { (timer) in
                self.setUpNetworkBitRate()
            })
        }
    }
    
}
