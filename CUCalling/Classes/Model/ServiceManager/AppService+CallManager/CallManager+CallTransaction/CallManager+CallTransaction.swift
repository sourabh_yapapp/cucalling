//
//  CallManager+CallTransaction.swift
//  Nehao
//
//  Created by Tina Gupta on 01/06/17.
//  Copyright © 2017 Amit Tripathi. All rights reserved.
//

import Foundation

extension (CallManager)
{
    func generateUniqueCallSessionId() -> String
    {
        return UUID().uuidString
    }
}
