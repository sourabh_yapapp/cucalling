//
//  CallManager+ProximitySensor.swift
//  Nehao
//
//  Created by Tina Gupta on 23/05/17.
//  Copyright © 2017 Amit Tripathi. All rights reserved.
//

import Foundation

extension(CallManager)
{
    func handleProximitySensor(isLocalVideoEnabled: Bool, isRemoteVideoEnabled: Bool)
    {
        if isLocalVideoEnabled || isRemoteVideoEnabled
        {
           disableProximitySensor()
        }
        else {
            enableProximitySensor()
        }
    }
    
    func disableProximitySensor()
    {
        DispatchQueue.main.async {
            UIDevice.current.isProximityMonitoringEnabled = false
        }
    }
    
    private func enableProximitySensor()
    {
        DispatchQueue.main.async {
            UIDevice.current.isProximityMonitoringEnabled = true
        }
    }


}
