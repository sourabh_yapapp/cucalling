

//
//  CallAudioManager.swift
//  Nehao
//
//  Created by Abhimanu Jindal on 08/03/17.
//  Copyright © 2017 Amit Tripathi. All rights reserved.
//

import Foundation

protocol CallAudioManagerDelegate:class{
    func outputSpeakerDidChange(isSpeaker:Bool,routeChangeReason : AudioManager.RouteChangeReason, speakerType:AudioManager.SpeakerType)
    func availableOutputsDidChange(isAdded: Bool, type: String)
    func didUpdateSpeakerType(speakerType: CurrentSelectedAudioOutput)
    func avaiableAudioOutputPortChanged()

}

enum CallAudioOutputType{
    case Bluetooth
    case Headphones
    case None
}

enum CurrentSelectedAudioOutput{
    case SpeakerOn
    case SpeakerOff
    case Bluetooth
    case Headphones
    case InEarPhone
}

class CallAudioManager:NSObject, AudioManagerDelegate{
    
    enum CallAudioType {
        case Incoming
        case Starting
        case Connecting
        case Ringing
        case OnHold
        case Waiting
        case OtherCallInOngoingCall
        case Ending
    }
    
    struct AudioOutputDescription {
        var portName : String = ""
        var portType : String = ""
    }

    
    
    lazy var audioManager = AudioManager()
    weak var delegate:CallAudioManagerDelegate?
    
    override init(){
        super.init()
        audioManager.delegate = self
    }
    
    func playCallAudio(type:CallAudioType, shouldToggleSpeakerForVideoCall: Bool = false){
        
        if type == .Incoming{
            audioManager.stopAllSounds()
            toggleOutputSpeaker(isSpeaker: true)
            audioManager.setAppSoundVolume(volume: 1.0)
            audioManager.playAppAudio(fileName: "BellRingtone", fileExtension: .MP3, shouldRepeat: true, shouldVibrate: true, shouldEnableMicPermission: true)
        }
        if type == .Starting || type == .Connecting{
            
            
            audioManager.stopAllSounds()
            audioManager.playAppAudio(fileName: "CallStarting", fileExtension: .MP3, shouldRepeat: true, shouldVibrate: false, shouldEnableMicPermission: true)
          }
        
        
        if type == .Ringing{
            audioManager.stopAllSounds()
            audioManager.playAppAudio(fileName: "CallRinging", fileExtension: .MP3, shouldRepeat: true, shouldVibrate: false, shouldEnableMicPermission: true)
            
            if AVAudioSession.sharedInstance().currentRoute.outputs[0].portType == AVAudioSessionPortBuiltInSpeaker || shouldToggleSpeakerForVideoCall
            {
                toggleOutputSpeaker(isSpeaker: true)
            }
            else{
                toggleOutputSpeaker(isSpeaker: false)
            }

            /*
            if let speakerType = audioManager.getCurrentlySelectedOutput()
            {
                audioManager.switchOutputSpeaker(speakerType: audioManager.getCurrentlySelectedOutput()!)
            }
            else
            {
                if shouldToggleSpeakerForVideoCall
                {
                    toggleOutputSpeaker(isSpeaker: true)
                }
                else{
                    toggleOutputSpeaker(isSpeaker: false)
                }
            }
 */
            

        }
        
        if type == .OnHold{
            audioManager.stopAllSounds()
            audioManager.playAppAudio(fileName: "CallWaiting", fileExtension: .MP3, shouldRepeat: true, shouldVibrate: false, shouldEnableMicPermission: true)
        }
    }
    
    func setUpAudioSessionCategory(category : String){
        switch category {
        case AVAudioSessionCategoryPlayback:
            audioManager.setupAudioSessionCategory(category: category, withOptions: [.duckOthers], sessionMode: AVAudioSessionModeVoiceChat, shouldUpdatePreferredInput: false)
            
        case AVAudioSessionCategoryPlayAndRecord:
            audioManager.setupAudioSessionCategory(category: category, withOptions: [.allowBluetooth,.allowBluetoothA2DP], sessionMode: AVAudioSessionModeVoiceChat, shouldUpdatePreferredInput: false)

        case AVAudioSessionCategoryAmbient:
            audioManager.setupAudioSessionCategory(category: category, withOptions: nil, sessionMode: AVAudioSessionModeVoiceChat, shouldUpdatePreferredInput: false)
        case AVAudioSessionCategorySoloAmbient:
            audioManager.setupAudioSessionCategory(category: category, withOptions: nil, sessionMode: nil, shouldUpdatePreferredInput: true)
       
        case AVAudioSessionCategoryMultiRoute:
            audioManager.setupAudioSessionCategory(category: category, withOptions: nil, sessionMode: nil, shouldUpdatePreferredInput: true)
     
        default:
            audioManager.setupAudioSessionCategory(category: category, withOptions: [.duckOthers], sessionMode: nil, shouldUpdatePreferredInput: false)

        }
    }

    func stopCallAudio(type:CallAudioType){
        audioManager.stopAllSounds()
    }
    
    func stopAllAudio(){
        audioManager.stopAllSounds()
    }
    
    func toggleOutputSpeaker(isSpeaker:Bool, isInitiatedByUser: Bool = false){
        
        /** Extra Audio Code
//    if (audioManager.checkIfHeadphonesConnected() || audioManager.checkIfBluetoothConnected()) && ( AVAudioSession.sharedInstance().currentRoute.outputs[0].portType != AVAudioSessionPortBuiltInMic || AVAudioSession.sharedInstance().currentRoute.outputs[0].portType != AVAudioSessionPortBuiltInReceiver || AVAudioSession.sharedInstance().currentRoute.outputs[0].portType != AVAudioSessionPortHeadphones)
//        {
//            //If user has connected any wireless route or toggled output explicitly then just update current route
//            
////          audioManager.switchOutputSpeaker(speakerType: .Default,uid: "", isUserSelected: isInitiatedByUser)
////            return
//        }
        
//        if (audioManager.checkIfHeadphonesConnected() || audioManager.checkIfBluetoothConnected())
//        {
//            audioManager.switchOutputSpeaker(speakerType: .Default,uid: "", isUserSelected: isInitiatedByUser)
//            return
//
//        }
 
 **/
        
        if isSpeaker{
           audioManager.switchOutputSpeaker(speakerType: .PhoneLoudSpeaker,uid: "", isUserSelected: isInitiatedByUser)
            return
        }
        else{
            audioManager.switchOutputSpeaker(speakerType: .None,uid: "", isUserSelected: isInitiatedByUser)
            return
        }
    }
    
    
    func toggleOutputSpeakerOnVideoUpdatedOperation(isSpeaker:Bool, isInitiatedByUser: Bool = false){
         /** Extra Audio Code
//         if (audioManager.checkIfHeadphonesConnected() || audioManager.checkIfBluetoothConnected()) && ( AVAudioSession.sharedInstance().currentRoute.outputs[0].portType != AVAudioSessionPortBuiltInMic || AVAudioSession.sharedInstance().currentRoute.outputs[0].portType != AVAudioSessionPortBuiltInReceiver || AVAudioSession.sharedInstance().currentRoute.outputs[0].portType != AVAudioSessionPortHeadphones)
//         {
//            //If user has connected any wireless route or toggled output explicitly then just update current route
//            audioManager.switchOutputSpeaker(speakerType: .Default,uid: "", isUserSelected: isInitiatedByUser)
//            return
//        }
//        
 */
        
        if isSpeaker{
            audioManager.switchOutputSpeaker(speakerType: .PhoneLoudSpeaker,uid: "", isUserSelected: isInitiatedByUser)
            return
        }
    }
    
    func toggleOutputSetByuser(outputType: CurrentSelectedAudioOutput, isInitiatedByUser: Bool = false){
        switch outputType
        {
        case .Bluetooth:
            audioManager.switchOutputSpeaker(speakerType: .BluetoothSpeaker,uid: "", isUserSelected: isInitiatedByUser)
            
        case .Headphones:
            audioManager.switchOutputSpeaker(speakerType: .EarPhones,uid: "", isUserSelected: isInitiatedByUser)

        case .InEarPhone:
            audioManager.switchOutputSpeaker(speakerType: .PhoneInEarSpeaker,uid: "", isUserSelected: isInitiatedByUser)

        case .SpeakerOn:
            audioManager.switchOutputSpeaker(speakerType: .PhoneLoudSpeaker,uid: "", isUserSelected: isInitiatedByUser)

        case .SpeakerOff:
            audioManager.switchOutputSpeaker(speakerType: .None,uid: "", isUserSelected: isInitiatedByUser)

        }
    }

    func getCurrentlySelectedOutput() -> AudioManager.SpeakerType?
    {
        return audioManager.getCurrentlySelectedOutput()
    }
    
    //MARK: Audio Manager delegate Method(s)
    func audioPlayedSuccessfully(soundFileName:String){
        
    }
    
    func outputSpeakerDidChange(speakerType:AudioManager.SpeakerType, routeChangeReason : AudioManager.RouteChangeReason){
        if speakerType == .PhoneLoudSpeaker{
            self.delegate?.outputSpeakerDidChange(isSpeaker: true,routeChangeReason : routeChangeReason, speakerType: speakerType)
        }
        else{
            self.delegate?.outputSpeakerDidChange(isSpeaker: false,routeChangeReason : routeChangeReason, speakerType: speakerType)
        }
    }
    
    func outputSpeakerFailedToChange(speakerType:AudioManager.SpeakerType){
        if speakerType != .PhoneLoudSpeaker{
            //No change in output
            self.delegate?.outputSpeakerDidChange(isSpeaker: true,routeChangeReason : .Others, speakerType: speakerType)
        }
        else{
            self.delegate?.outputSpeakerDidChange(isSpeaker: false,routeChangeReason : .Others, speakerType: speakerType)
        }
    }
    
    func availableAudioOutputDidChange(isAdded: Bool, type: String)
    {
            self.delegate?.availableOutputsDidChange(isAdded: isAdded, type: type)
    }

    func avaiableAudioOutputPortChanged()
    {
        self.delegate?.avaiableAudioOutputPortChanged()
    }
    
    func didUpdateSpeakerType(speakerType: CurrentSelectedAudioOutput)
    {
        self.delegate?.didUpdateSpeakerType(speakerType: speakerType)
    }
    
    //MARK:- Output Audio Ports
    
    func getAudioOutputCategory() -> CallAudioOutputType
    {
        //Check current audio output and update Call View accordingly
        var availableOutputs = audioManager.getAvailableOutputTypes()
        
        let filteredOutputBluetoothAdded =  availableOutputs.filter{$0.portType == AVAudioSessionPortBluetoothHFP || $0.portType == AVAudioSessionPortBluetoothLE || $0.portType == AVAudioSessionPortBluetoothA2DP}
        
        if filteredOutputBluetoothAdded.count > 0 {
            return .Bluetooth
        }
        
        let filteredOutputHeadphone =  availableOutputs.filter{$0.portType == AVAudioSessionPortHeadphones || $0.portType == AVAudioSessionPortHeadsetMic }
        
        if filteredOutputHeadphone.count > 0 {
            return .Headphones
        }
            
            return .None
    }
    
    func getAllAvailableOutputPorts() -> [AudioPortDescription]
    {
         return audioManager.getAvailableOutputTypes()
    }
    
    func checkIfBTConnected()-> Bool{
        return audioManager.checkIfBluetoothConnected()
    }
    
   
    
}
