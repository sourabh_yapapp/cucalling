//
//  CallManager+Audio.swift
//  Nehao
//
//  Created by Abhimanu Jindal on 08/03/17.
//  Copyright © 2017 Amit Tripathi. All rights reserved.
//

import Foundation

struct AudioSessionPortDescription{
    var Portname : String = ""
    var PortType : String = ""
    var UID : String = ""
}

extension(CallManager):CallAudioManagerDelegate{
    
    func playCallAudio(type:CallAudioManager.CallAudioType, shouldToggleSpeakerForVideoCall: Bool = false){
        callAudioManager.playCallAudio(type: type, shouldToggleSpeakerForVideoCall: shouldToggleSpeakerForVideoCall)
    }
    
    func stopCallAudio(type:CallAudioManager.CallAudioType){
        callAudioManager.stopCallAudio(type: type)
    }
    
    func stopAllAudio(){
        callAudioManager.stopAllAudio()
    }
    
    func toggleSpeaker(isSpeaker:Bool, isInitiatedByUser: Bool = false){

        if !isInitiatedByUser
        {
            let userSelectedPref = checkIfUserToggledOutput()
            if userSelectedPref.isSet
            {
                callAudioManager.toggleOutputSetByuser(outputType: userSelectedPref.selectdOutput!, isInitiatedByUser: isInitiatedByUser)
                return
            }
        }
                    callAudioManager.toggleOutputSpeaker(isSpeaker: isSpeaker, isInitiatedByUser: isInitiatedByUser)
    }
    
    func toggleSpeakerOnVideoUpdatedOperation(isSpeaker:Bool, isInitiatedByUser: Bool = false){
        let userSelectedPref = checkIfUserToggledOutput()
        if userSelectedPref.isSet
        {
            callAudioManager.toggleOutputSetByuser(outputType: userSelectedPref.selectdOutput!, isInitiatedByUser :isInitiatedByUser)
        }
        else
        {
            callAudioManager.toggleOutputSpeakerOnVideoUpdatedOperation(isSpeaker:isSpeaker, isInitiatedByUser: isInitiatedByUser)
        }
    }
    
    func getCurrentlySelectedOutputPort() -> AudioManager.SpeakerType?{
        return callAudioManager.getCurrentlySelectedOutput()
    }
    
    
    func getOutputPortType() -> CallAudioOutputType
    {
        return callAudioManager.getAudioOutputCategory()
    }
    
    func checkIfBTConnected() -> Bool{
        return callAudioManager.checkIfBTConnected()
    }
          
    private func checkIfUserToggledOutput()-> ( isSet: Bool, selectdOutput: CurrentSelectedAudioOutput?)
    {
        if self.userSelectedOutputPref != nil
        {
            return (true,self.userSelectedOutputPref)
        }
        
        else
        {
            return (false,nil)
        }
    }
    
    //MARK:- Call Audio Manager Delegate Method(s)
    
    func outputSpeakerDidChange(isSpeaker: Bool,routeChangeReason : AudioManager.RouteChangeReason, speakerType: AudioManager.SpeakerType) {
        self.delegate?.speakerToggled(isSpeaker: isSpeaker,reason:  routeChangeReason)
    }
    
    
    func availableOutputsDidChange(isAdded: Bool, type: String)
    {
        self.delegate?.availableOutputChanged(isAdded: isAdded, type: type)
    }
    
    func avaiableAudioOutputPortChanged()
    {
        self.delegate?.availableOutputPortChanged(isBTConnected: self.checkIfBTConnected())
    }
    
    func didUpdateSpeakerType(speakerType: CurrentSelectedAudioOutput)
    {
        self.userSelectedOutputPref  = speakerType
    }
    
}
