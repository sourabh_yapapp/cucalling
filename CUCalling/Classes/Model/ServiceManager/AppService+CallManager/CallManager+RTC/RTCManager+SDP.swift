//
//  RTCManager+SDP.swift
//  Nehao
//
//  Created by Abhimanu Jindal on 05/03/17.
//  Copyright © 2017 Amit Tripathi. All rights reserved.
//

extension(RTCManager){
    
    func startSignalling(){
        guard isPeerConnectionSet() else{
            self.delegate?.sdpSetupFailed(error:YAError.generateError(description: "Connection Failed", localizedDescription: "Connection Failed", debugDescription: "Unable to create peer connection", code: YAError.CodeType.NetworkError.rawValue)!)
            return
        }
        createSDPOffer()
    }
    
    func processSDP(sdpDescription:String?, sdpType:String?){
        guard isPeerConnectionSet() else{
            self.delegate?.sdpSetupFailed(error:YAError.generateError(description: "Connection Failed", localizedDescription: "Connection Failed", debugDescription: "Unable to create peer connection", code: YAError.CodeType.NetworkError.rawValue)!)
            return
        }
        
        let sdpTypeString = sdpType?.lowercased()
        let sdpSessionDescription = RTCSessionDescription.init(type: getSDPTypeFromString(sdpTypeString: sdpTypeString!)!, sdp: sdpDescription!)
        setRemoteDescription(sessionDescription: sdpSessionDescription)
    }
    
    //MARK:- Private Method(s)
    private func createSDPOffer(){
        peerConnection.offer(for: defaultOfferConstraints(), completionHandler: {(sessionDescription, error) in
            if (error != nil){
                self.delegate?.sdpSetupFailed(error:YAError.generateError(description: "Connection Failed", localizedDescription: "Connection Failed", debugDescription: "Unable to create peer connection", code: YAError.CodeType.NetworkError.rawValue)!)
                return
            }
            self.setLocalDescription(sessionDescription: sessionDescription!)
        })
    }
    
    private func createSDPAnswer(){
        if peerConnection != nil{
        peerConnection.answer(for: defaultAnswerConstraints(), completionHandler: {(sessionDescription, error) in
            if (error != nil){
                YALog.print("Failed to create session description. Error: \(String(describing: error))")
                return
            }
            YALog.print("didCreateSessionDescription")
            self.setLocalDescription(sessionDescription: sessionDescription!)
            
        })
        }
    }
    
    private func setLocalDescription(sessionDescription:RTCSessionDescription){
        if self.peerConnection == nil{
            self.delegate?.sdpSetupFailed(error:YAError.generateError(description: "Connection Failed", localizedDescription: "Connection Failed", debugDescription: "Unable to create peer connection", code: YAError.CodeType.NetworkError.rawValue)!)
            return
        }
        self.peerConnection.setLocalDescription(sessionDescription, completionHandler: { (error) in
            if (error != nil){
                self.delegate?.sdpSetupFailed(error:YAError.generateError(description: "Connection Failed", localizedDescription: "Connection Failed", debugDescription: "Failed to set local session description. Error: \(String(describing: error))", code: YAError.CodeType.NetworkError.rawValue)!)
                return
            }
            YALog.print("Did Set Local Description \(sessionDescription.type.hashValue)")
            self.delegate?.sdpCreatedSuccessfully(sdpDescription: sessionDescription.sdp, sdpType: self.getSDPTypeInString(sdpType: sessionDescription.type))
        })
    }
    
    private func setRemoteDescription(sessionDescription:RTCSessionDescription){
        self.peerConnection.setRemoteDescription(sessionDescription, completionHandler:{(error) in
            if (error != nil){
                YALog.print("Failed to set remote session description. Error: \(String(describing: error))")
                self.delegate?.sdpSetupFailed(error:YAError.generateError(description: "Connection Failed", localizedDescription: "Connection Failed", debugDescription: "Failed to set remote session description. Error: \(String(describing: error))", code: YAError.CodeType.NetworkError.rawValue)!)
                return
            }
            self.processPendingICECandidates()
            YALog.print("Did Set remote Description \(sessionDescription.type.hashValue)")
            if sessionDescription.type == .offer{
                self.createSDPAnswer()
            }
        })
    }
    
    //MARK:.... SDP Type Modification Method(S)
    private func getSDPTypeInString(sdpType:RTCSdpType)->String{
        switch sdpType {
        case .answer:
            return "answer"
        case .offer:
            return "offer"
        case .prAnswer:
            return "prAnswer"
        }
    }
    
    private func getSDPTypeFromString(sdpTypeString:String)->RTCSdpType?{
        switch sdpTypeString {
        case "answer":
            return .answer
        case "offer":
            return .offer
        case "prAnswer":
            return .prAnswer
        default:
            YALog.print("This is exception, Need to check")
            return .offer
        }
    }
}

