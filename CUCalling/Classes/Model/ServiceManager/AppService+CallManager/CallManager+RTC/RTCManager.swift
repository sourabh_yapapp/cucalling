//
//  CallManager.swift
//  Nehao
//
//  Created by Abhimanu Jindal on 01/03/17.
//  Copyright © 2017 Amit Tripathi. All rights reserved.
//

import Foundation
import WebRTC

protocol RTCManagerDelegate:class {
    func sdpCreatedSuccessfully(sdpDescription:String?, sdpType:String?)
    func sdpSetupFailed(error:YAError)
    
    func didReceiveIceCandidate(sdpMId:String?, sdpMLineIndex:Int32?, sdp:String?)
    
    func didUpdateMediaTrack(mediaTrack:YACallMediaTrack?)
    func onRenegotiationNeeded()
    
    func didSetupRTCSuccessfully()
    func failedToSetupRTC(error:YAError?)
    
    func didChangeRTCICEConnectionState(state:RTCIceConnectionState)
    
      

}

class RTCManager: NSObject {
    //MARK:- Variables/ Constants
    weak var delegate:RTCManagerDelegate?
    
    var peerConnectionFactory: RTCPeerConnectionFactory! = nil
    var peerConnection: RTCPeerConnection! = nil
    var defaultConstraints: RTCMediaConstraints! = nil
    var videoStreamConstraints: RTCMediaConstraints! = nil
    var audioStreamConstraints: RTCMediaConstraints! = nil
    var rtcConfig:RTCConfiguration! = nil
    lazy var iceServers:[CallICEServer] = []
    
    var arrReceivedICECandidates:[Dictionary<String,Any>] = []
    
    var isPeerConnectionCompleted :Bool = false
    var isPeerConnectionConnected :Bool = false
    var isPeerConnectionClosing :Bool = false

    var localVideoTrack:RTCVideoTrack?
    var localAudioTrack:RTCAudioTrack?
    
    override init() {
        super.init()
        peerConnectionFactory = RTCPeerConnectionFactory()
        videoStreamConstraints =  RTCMediaConstraints(
            mandatoryConstraints: nil,
            optionalConstraints: nil)
        defaultConstraints =  RTCMediaConstraints(
            mandatoryConstraints: ["OfferToReceiveAudio":"true","OfferToReceiveVideo":"true"
            ],
            optionalConstraints: ["internalSctpDataChannels":"true","DtlsSrtpKeyAgreement":"true"])
        
    }
    
    //MARK:- Public Method(s)
    //MARK: ....LifeCycle Method(s)
    
    func setUpPeerConnection(iceServers:[CallICEServer]){
        self.iceServers = iceServers
        if isPeerConnectionSet(){
            self.delegate?.didSetupRTCSuccessfully()
            return
        }
        self.delegate?.failedToSetupRTC(error: YAError.generateError(description: "Call Error", localizedDescription: "Call Error", debugDescription: "Call Error", code: 404))
    }
    
    func endPeerConnection(){
        isPeerConnectionCompleted = false
        isPeerConnectionConnected = false
        if !isPeerConnectionClosing
            {
                isPeerConnectionClosing = true
                if peerConnection != nil{
                    localAudioTrack = nil
                    localVideoTrack = nil
                    peerConnection = nil
                }
                DispatchQueue.main.async {
                    self.resetPeerConnection()
                    self.isPeerConnectionClosing = false
                }
                
        }
        
    }
    
    func resetPeerConnection(){
        if self.iceServers.count > 0{
            setUpPeerConnection(iceServers: self.iceServers)
        }
        else{
            self.delegate?.failedToSetupRTC(error: YAError.generateError(description: "Call Error", localizedDescription: "Call Error", debugDescription: "Call Error", code: 404))

        }
    }
    
}
