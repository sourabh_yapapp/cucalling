//
//  RTCManager+Constraints.swift
//  Nehao
//
//  Created by Abhimanu Jindal on 03/06/17.
//  Copyright © 2017 Amit Tripathi. All rights reserved.
//

extension (RTCManager){
    
    func defaultPeerConnectionConstraints() -> RTCMediaConstraints{
        let optionalConstraints =  ["DtlsSrtpKeyAgreement" : "true"]
        let constratins = RTCMediaConstraints.init(mandatoryConstraints: nil, optionalConstraints: optionalConstraints)
        return constratins
    }
    
    func defaultMediaAudioConstraints() -> RTCMediaConstraints{
        let mandatoryConstraints =  [kRTCMediaConstraintsLevelControl : kRTCMediaConstraintsValueTrue]
        let constratins = RTCMediaConstraints.init(mandatoryConstraints: mandatoryConstraints, optionalConstraints: nil)
        return constratins
    }
    
    func defaultAnswerConstraints() -> RTCMediaConstraints{
        
        return defaultOfferConstraints()
    }
    
    func defaultOfferConstraints() -> RTCMediaConstraints{
        let mandatoryConstraints =  ["OfferToReceiveAudio" : "true", "OfferToReceiveVideo" : "true"]
        let constratins = RTCMediaConstraints.init(mandatoryConstraints: mandatoryConstraints, optionalConstraints: nil)
        return constratins
    }
    
}
