//
//  RTCManager+PeerConnection.swift
//  Nehao
//
//  Created by Abhimanu Jindal on 05/03/17.
//  Copyright © 2017 Amit Tripathi. All rights reserved.
//

extension (RTCManager):RTCPeerConnectionDelegate{
    
    
    //MARK: ....Peer Connection Method(s)
    func isPeerConnectionSet() -> Bool{
        if peerConnection == nil{
            peerConnection = self.getPeerConnection()
        }
        guard peerConnection != nil else {
            YALog.print("Unable to create peer connection")
            return false
        }
        return true
    }
    
    func getPeerConnection() -> RTCPeerConnection? {
        if getICEServers().count == 0 {
            YALog.print("No ICE SERVERS AVAILABLE")
            return nil
        }
        peerConnection = peerConnectionFactory.peerConnection(with: getRTCConfiguration(), constraints: defaultPeerConnectionConstraints(), delegate: self)
        return peerConnection;
    }
    
    func getRTCConfiguration() -> RTCConfiguration{
        if (rtcConfig == nil){
            rtcConfig = RTCConfiguration()
            //rtcConfig.iceTransportPolicy = .all
            rtcConfig.iceServers = self.getICEServers()
            //rtcConfig.bundlePolicy = .maxBundle
            //rtcConfig.rtcpMuxPolicy = .negotiate
            //rtcConfig.tcpCandidatePolicy = .enabled
            //rtcConfig.candidateNetworkPolicy = .all
            //rtcConfig.continualGatheringPolicy = .gatherContinually
            
        }
        return rtcConfig
    }
    
    func getICEServers() -> [RTCIceServer]{
        var rtcICEservers:[RTCIceServer] = []
        for (_,item) in self.iceServers.enumerated(){
            let iceServer = RTCIceServer.init(urlStrings: [item.url!], username: item.userName, credential: item.password)
            rtcICEservers.append(iceServer)
        }
        return rtcICEservers
    }
    
    //MARK:- RTCPeerConncection Delegate Method(s)
    // Triggered when the SignalingState changed.
    func peerConnection(_ peerConnection: RTCPeerConnection, didChange stateChanged: RTCSignalingState) {
        switch stateChanged {
        case .closed:
            YALog.print("Signal State Changed - Closed")
        case .haveLocalOffer:
            YALog.print("Signal State Changed - have local offer")
        case .haveLocalPrAnswer:
            YALog.print("Signal State Changed - have pr answer")
        case .haveRemoteOffer:
            YALog.print("Signal State Changed - have remote offer")
        case .haveRemotePrAnswer:
            YALog.print("Signal State Changed - haveRemotePrAnswer")
        case .stable:
            YALog.print("Signal State Changed - stable")
        }
    }
    
    
    // Triggered when media is received on a new stream from remote peer.
    func peerConnection(_ peerConnection: RTCPeerConnection, didAdd stream: RTCMediaStream) {
        
//        YALog.print("Added Stream - \(stream)")
//        YALog.print("Received video tracks and %lu audio tracks \(stream.videoTracks.count), \(stream.audioTracks.count)");
        if stream.videoTracks.count > 0 {
            let videoTrack:RTCVideoTrack = stream.videoTracks[0]
            videoTrack.isEnabled = false
            let mediaTrack = YACallMediaTrack.init()
            mediaTrack.videoTrack = videoTrack
            mediaTrack.type = .remote
            mediaTrack.trackType = .video
            mediaTrack.updateType = .add
            self.delegate?.didUpdateMediaTrack(mediaTrack: mediaTrack)
        }
        if stream.audioTracks.count > 0 {
            let audioTrack:RTCAudioTrack = stream.audioTracks[0]
            audioTrack.isEnabled = false
            let mediaTrack = YACallMediaTrack.init()
            mediaTrack.audioTrack = audioTrack
            mediaTrack.type = .remote
            mediaTrack.trackType = .audio
            mediaTrack.updateType = .add
            self.delegate?.didUpdateMediaTrack(mediaTrack: mediaTrack)
        }
    }
    
    // Triggered when a remote peer close a stream.
    
    func peerConnection(_ peerConnection: RTCPeerConnection, didRemove stream: RTCMediaStream){
        
//        YALog.print("Removed Stream - \(stream)")
//        YALog.print("Removed video tracks \(stream.videoTracks.count) and audio tracks \(stream.audioTracks.count)");
        if stream.videoTracks.count > 0 {
            let videoTrack:RTCVideoTrack = stream.videoTracks[0]
            let mediaTrack = YACallMediaTrack.init()
            mediaTrack.audioTrack = videoTrack
            mediaTrack.type = .remote
            mediaTrack.trackType = .video
            mediaTrack.updateType = .remove
            self.delegate?.didUpdateMediaTrack(mediaTrack: mediaTrack)
        }
        if stream.audioTracks.count > 0 {
            let audioTrack:RTCAudioTrack = stream.audioTracks[0]
            let mediaTrack = YACallMediaTrack.init()
            mediaTrack.audioTrack = audioTrack
            mediaTrack.type = .remote
            mediaTrack.trackType = .audio
            mediaTrack.updateType = .remove
            self.delegate?.didUpdateMediaTrack(mediaTrack: mediaTrack)
        }
    }
    
    // Triggered when renegotiation is needed, for example the ICE has restarted.
    func peerConnectionShouldNegotiate(_ peerConnection: RTCPeerConnection) {
        YALog.print("onRenegotiationNeeded")
        self.delegate?.onRenegotiationNeeded()
    }
    
    // Called any time the ICEConnectionState changes.
    func peerConnection(_ peerConnection: RTCPeerConnection, didChange newState: RTCIceConnectionState) {
        switch newState {
        case .checking:
            YALog.print("iceConnectionChanged - checking")
        case .closed:
            isPeerConnectionCompleted = false
            YALog.print("iceConnectionChanged - closed")
        case .completed:
            isPeerConnectionCompleted = true
            YALog.print("iceConnectionChanged - completed")
        case .connected:
            if isPeerConnectionConnected {
                return
            }
            isPeerConnectionConnected = true
            YALog.print("iceConnectionChanged - connected")
        case .count:
            YALog.print("iceConnectionChanged - count")
        case .disconnected:
            if  !isPeerConnectionCompleted {
                return
            }
            isPeerConnectionCompleted = false
            YALog.print("iceConnectionChanged - disconnected")
        case .failed:
            isPeerConnectionCompleted = false
            YALog.print("iceConnectionChanged - failed")
        case .new:
            YALog.print("iceConnectionChanged - new")
        }
        self.delegate?.didChangeRTCICEConnectionState(state: newState)
    }
    
    // Called any time the ICEGatheringState changes.
    func peerConnection(_ peerConnection: RTCPeerConnection, didChange newState: RTCIceGatheringState) {
        switch newState {
        case .complete:
            YALog.print("iceGatheringChanged - complete")
        case .gathering:
            YALog.print("iceGatheringChanged - gathering")
        case .new:
            YALog.print("iceGatheringChanged - new")
        }
    }
    
    // New Ice candidate have been found.
    func peerConnection(_ peerConnection: RTCPeerConnection, didGenerate candidate: RTCIceCandidate) {
        YALog.print("gotICECandidate")
        self.delegate?.didReceiveIceCandidate(sdpMId: candidate.sdpMid, sdpMLineIndex: candidate.sdpMLineIndex, sdp: candidate.sdp)

    }
    func peerConnection(_ peerConnection: RTCPeerConnection, didRemove candidates: [RTCIceCandidate]) {
        YALog.print("RemovedICECandidates")
    }
    
    // New data channel has been opened.
    func peerConnection(_ peerConnection: RTCPeerConnection, didOpen dataChannel: RTCDataChannel) {
        YALog.print("Did Open Channel")
    }
}
