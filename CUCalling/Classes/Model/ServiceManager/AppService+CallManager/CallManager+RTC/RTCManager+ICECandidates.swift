//
//  RTC+ICECandidates.swift
//  Nehao
//
//  Created by Abhimanu Jindal on 05/03/17.
//  Copyright © 2017 Amit Tripathi. All rights reserved.
//

extension(RTCManager){
    
    func processICECandidate(sdpMId:String?, sdpMLineIndex:Int?, sdp:String?){
        if self.peerConnection == nil{
            self.delegate?.sdpSetupFailed(error:YAError.generateError(description: "Connection Failed", localizedDescription: "Connection Failed", debugDescription: "Unable to create peer connection", code: YAError.CodeType.NetworkError.rawValue)!)

            return
        }
        /*
        if self.peerConnection.remoteDescription == nil{
            arrReceivedICECandidates.append(generateICECandidateDictionary(sdpMId: sdpMId, sdpMLineIndex: sdpMLineIndex, sdp: sdp))
            return
        }
        else{
            processPendingICECandidates()
        }*/
        peerConnection.add(RTCIceCandidate.init(sdp: sdp!, sdpMLineIndex: Int32(sdpMLineIndex!), sdpMid: sdpMId!))
    }
    
    func processPendingICECandidates(){
        for (_,dic) in arrReceivedICECandidates.enumerated(){
            let sdpmid = dic["sdpMId"]
            let sdplineindex = dic["sdpMLineIndex"] as! Int
            let _sdp = dic["sdp"]
            peerConnection.add(RTCIceCandidate.init(sdp: _sdp! as! String, sdpMLineIndex: Int32(sdplineindex) , sdpMid: sdpmid! as? String))
        }
        arrReceivedICECandidates.removeAll()
    }
    
    func generateICECandidateDictionary(sdpMId:String?, sdpMLineIndex:Int?, sdp:String?)->Dictionary<String, Any>{
        var dic = Dictionary<String, Any>()
        dic["sdpMId"] = sdpMId
        dic["sdpMLineIndex"] = sdpMLineIndex
        dic["sdp"] = sdp
        return dic
    }
    
}
