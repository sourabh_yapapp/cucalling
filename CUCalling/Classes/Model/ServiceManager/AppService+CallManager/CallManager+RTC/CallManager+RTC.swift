//
//  CallManager.swift
//  Nehao
//
//  Created by Abhimanu Jindal on 01/03/17.
//  Copyright © 2017 Amit Tripathi. All rights reserved.
//

extension (CallManager):RTCManagerDelegate {
    
    //MARK:- Public Mryhod(s)
    func setUpPeerConnection(iceServers:[CallICEServer]){
        rtcManager.setUpPeerConnection(iceServers: iceServers)
    }
    
    func endPeerConnection(){
        self.cleanUpMediaStreams()
        rtcManager.endPeerConnection()
    }
    
    
    func getPeerConnectionIsConnected()-> Bool
    {
        return true
    }

    
    func getPeerCOnncetionIsCompleted()-> Bool
    {
    return true
    }
    //MARK:- RTC DELEGATE METHOD(S)
    
    //MARK:...RTC SETUP
    func didSetupRTCSuccessfully(){
        isPeerConnectionSet = true
        processPendingPushMessages()

        if isCallInititationPendingForSender{
            if currentSenderID != nil && currentReceiverID != nil && currentUserName != nil && currentCallInitiationDetails != nil{
            initiateOutGoingCall(senderID: currentSenderID!, receiverID: currentReceiverID!, userName: currentUserName!, details: currentCallInitiationDetails!, isCallTypeVideo: self.isCallTypeVideo, callerRoomId: self.currentCallerRoomId!)
           isCallInititationPendingForSender = false
            }
        }
        if isCallInititationPendingForReceiver{
            if currentSenderID != nil && currentReceiverID != nil && currentUserName != nil && currentCallInitiationDetails != nil{
            initiateIncomingCall(senderID: currentSenderID!, receiverID: currentReceiverID!, userName: currentUserName!, details: currentCallInitiationDetails!, isCallTypeVideo: self.isCallTypeVideo, callerRoomID:  self.currentCallerRoomId!)
            }
            isCallInititationPendingForReceiver = false
        }
    }
    
    func failedToSetupRTC(error:YAError?){
        isPeerConnectionSet = false
    }
    
    //MARK:...SDP
    func sdpCreatedSuccessfully(sdpDescription:String?, sdpType:String?){
        var dictionary:[String:String] = [:]
        dictionary["sdpDescription"] = sdpDescription
        dictionary["sdpType"] = sdpType
//        YALog.print(dictionary)
       // if self.currentCallType == .Outgoing && sdpType == "offer"{

        if sdpType == "offer" && (self.currentCallType == .Outgoing || self.currentCallType == .OngoingOut){
            sendSDPToServer(fromId: currentSenderID!, toId: currentReceiverID!, data:dictionary as [String : AnyObject] , isOffer: true)
            return
        }
       // if self.currentCallType == .Incoming && sdpType == "answer"{

        if sdpType == "answer"  && (self.currentCallType == .Incoming || self.currentCallType == .OngoingIn){
            sendSDPToServer(fromId: currentSenderID!, toId: currentReceiverID!, data:dictionary as [String : AnyObject], isOffer: false)

            return
        }
    }
    
    func sdpSetupFailed(error:YAError){
        
    }
    
       
    
    //MARK:...ICE CANDIDATES
    func didReceiveIceCandidate(sdpMId:String?, sdpMLineIndex:Int32?, sdp:String?){
        var dictionary:[String:AnyObject] = [:]
        dictionary["sdpMId"] = sdpMId as AnyObject
        dictionary["sdpMLineIndex"] = sdpMLineIndex! as AnyObject
        dictionary["sdp"] = sdp as AnyObject
        
        if currentSenderID == nil{
            return
        }
        if currentReceiverID == nil{
            return
        }
        sendICECandidatesToServer(fromId: currentSenderID!, toId: currentReceiverID!, data: dictionary)
    }
    
    //MARK:...MEDIA
    func onRenegotiationNeeded(){
        isRenegotiationInitiated = true
        //        if self.currentCallType == .Outgoing{

        if self.currentCallType == .OngoingOut || self.currentCallType == .Outgoing{
            if isSignallingInitiated{
                initiateSignalling()
            }
        }
        //        if self.currentCallType == .Incoming{

        if self.currentCallType == .OngoingIn || self.currentCallType == .Incoming{
            if isSignallingInitiated{
                didReceiveSDP(sdp: currentSDP, type: currentSDPType)
            }
        }
    }
    
    func didUpdateMediaTrack(mediaTrack:YACallMediaTrack?){
        processRTCMediaTrackUpdate(mediaTrack: mediaTrack)
    }
    
    //MARK:....ICE CONNECTION STATE
    func didChangeRTCICEConnectionState(state:RTCIceConnectionState){
        
        if state == .connected{
            rtcConnectedSuccessfully()
        }
       
        if state == .disconnected || state == .closed || state == .failed
        {
    
            if self.currentCallSessionID.characters.count > 0{
                endCallInAllStates(with: self.currentCallType)
                endPeerConnection()
            }
            
        }
    }
    
}
