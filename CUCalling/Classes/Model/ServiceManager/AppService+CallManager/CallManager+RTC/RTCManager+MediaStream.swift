//
//  RTC+Media.swift
//  Nehao
//
//  Created by Abhimanu Jindal on 05/03/17.
//  Copyright © 2017 Amit Tripathi. All rights reserved.
//

extension(RTCManager){
    
    //MARK: ....Media Stream Method(s)
    func setupMediaStream(shouldCreateAudioStream:Bool, shouldCreateVideoStream:Bool) {
        YALog.print("setUpMediaStream call process called")
        guard isPeerConnectionSet() else{
            YALog.print("Unable to create peer connection")
            return
        }
        
        self.createLocalMediaStream(shouldCreateAudioStream: shouldCreateAudioStream, shouldCreateVideoStream: shouldCreateVideoStream)

        /*
        if shouldCreateAudioStream && shouldCreateVideoStream{
            
            if isLocalStreamAvailable(){
                return
            }
        }
        else if shouldCreateVideoStream{
            if isLocalVideoStreamAvailable(){
                return
            }
        }
        else{
            if isLocalAudioStreamAvailable(){
                return
            }
        }
        let localStream = self.createLocalMediaStream(shouldCreateAudioStream: shouldCreateAudioStream, shouldCreateVideoStream: shouldCreateVideoStream)
        if self.peerConnection == nil{
            return
        }
        self.peerConnection.add(localStream)*/
    }
    
    func removeLocalMediaStream(shouldRemoveAudioStream:Bool, shouldRemoveVideoStream:Bool){
        
        guard isPeerConnectionSet() else{
            YALog.print("Unable to create peer connection")
            return
        }
        if peerConnection.localStreams.count == 0{
            return
        }
        let localStream = peerConnection.localStreams[0]
        
        
        if shouldRemoveVideoStream{
            if isLocalVideoStreamAvailable(){
                for (_, localVideoTrack) in localStream.videoTracks.enumerated(){

                    if localStream.videoTracks.contains(localVideoTrack){
                    localStream.removeVideoTrack(localVideoTrack)
                    let mediaTrack = YACallMediaTrack.init()
                    mediaTrack.videoTrack = localVideoTrack
                    mediaTrack.type = .local
                    mediaTrack.trackType = .video
                    mediaTrack.updateType = .remove
                    self.delegate?.didUpdateMediaTrack(mediaTrack: mediaTrack)
                    }
                }
            }
        }
        if shouldRemoveAudioStream{
            if isLocalAudioStreamAvailable(){
                for (_, localAudioTrack) in localStream.audioTracks.enumerated(){
                    localStream.removeAudioTrack(localAudioTrack)
                    let mediaTrack = YACallMediaTrack.init()
                    mediaTrack.videoTrack = localAudioTrack
                    mediaTrack.type = .local
                    mediaTrack.trackType = .audio
                    mediaTrack.updateType = .remove
                    self.delegate?.didUpdateMediaTrack(mediaTrack: mediaTrack)
                }
            }
        }
        if shouldRemoveAudioStream && shouldRemoveVideoStream{
            if peerConnection != nil{
                peerConnection.remove(localStream)
            }
        }
    }
    
    //MARK:- PRIVATE METHOD(S)
    
    //MARK: ....Media Stream Method(s)
    private func isLocalStreamAvailable()->Bool{
        return isLocalAudioStreamAvailable() && isLocalVideoStreamAvailable()
    }
    
    private func isLocalVideoStreamAvailable()->Bool{
        if peerConnection == nil{
            return false
        }
        if peerConnection.localStreams.count == 0{
            return false
        }
        let localStream = peerConnection.localStreams[0]
        if localStream.videoTracks.count == 0{
            return false
        }
        return true
    }
    
    private func isLocalAudioStreamAvailable()->Bool{
        if peerConnection == nil{
            return false
        }
        if peerConnection.localStreams.count == 0{
            return false
        }
        if (peerConnection.localStreams.count > 0)
        {
        let localStream = peerConnection.localStreams[0]
        if localStream.audioTracks.count == 0{
            return false
        }
        }
        return true
    }
    
    
    private func createLocalMediaStream(shouldCreateAudioStream:Bool, shouldCreateVideoStream:Bool){
        
        //let localStream:RTCMediaStream = RTCMediaStream()
        if peerConnection.localStreams.count > 0{
            for (_, stream) in peerConnection.localStreams.enumerated(){
                peerConnection.remove(stream)
            }
        }
        //##TEMP COMMENT
        //localStream = peerConnectionFactory.mediaStream(withStreamId: "ARDAMS")
        
        if shouldCreateAudioStream{
            if !isLocalAudioStreamAvailable(){
                createAudioSender()
                if localAudioTrack != nil{
                    localAudioTrack?.isEnabled = true
                    let mediaTrack = YACallMediaTrack.init()
                    mediaTrack.audioTrack = localAudioTrack
                    mediaTrack.type = .local
                    mediaTrack.trackType = .audio
                    mediaTrack.updateType = .add
                    self.delegate?.didUpdateMediaTrack(mediaTrack: mediaTrack)
                }
                /*
                //##TEMP COMMENT
                if let localAudioTrack = createLocalAudioTrack(){
                    localAudioTrack.isEnabled = true
                    localStream?.addAudioTrack(localAudioTrack)
                    let mediaTrack = YACallMediaTrack.init()
                    mediaTrack.audioTrack = localAudioTrack
                    mediaTrack.type = .local
                    mediaTrack.trackType = .audio
                    mediaTrack.updateType = .add
                    self.delegate?.didUpdateMediaTrack(mediaTrack: mediaTrack)
                }*/
            }
        }
        
        if shouldCreateVideoStream{
            if !isLocalVideoStreamAvailable(){
                //##TEMP COMMENT
                //let localVideoTrack = self.createLocalVideoTrackFrontCamera()
                
                createVideoSender()
                
                if localVideoTrack != nil {
                    //##TEMP COMMENT
                    //localStream?.addVideoTrack(localVideoTrack!)
                    let mediaTrack = YACallMediaTrack.init()
                    mediaTrack.videoTrack = localVideoTrack
                    mediaTrack.type = .local
                    mediaTrack.trackType = .video
                    mediaTrack.updateType = .add
                    self.delegate?.didUpdateMediaTrack(mediaTrack: mediaTrack)
                }
            }
        }
        //return localStream
    }
    
    private func createLocalVideoTrackFrontCamera()->RTCVideoTrack? {
        var localVideoTrack:RTCVideoTrack?
        #if !TARGET_IPHONE_SIMULATOR
            
            let videoSource = peerConnectionFactory.avFoundationVideoSource(with: videoStreamConstraints)
            localVideoTrack = peerConnectionFactory.videoTrack(with: videoSource, trackId: "ARDAMSv0")
        #endif
        return localVideoTrack;
    }
    
    private func createLocalAudioTrack()->RTCAudioTrack? {
        let localAudioTrack = peerConnectionFactory.audioTrack(withTrackId: "ARDAMSa0")
        return localAudioTrack
    }
    
    private func createAudioSender() {
        if peerConnection == nil{
            return
        }
        let constraints = defaultMediaAudioConstraints()
        let audioSource = peerConnectionFactory.audioSource(with: constraints)
        let track = peerConnectionFactory.audioTrack(with: audioSource, trackId: "ARDAMSa0")
        localAudioTrack = track
        let sender = peerConnection.sender(withKind: kRTCMediaStreamTrackKindAudio, streamId: "ARDAMS")
        sender.track = track;
    }
    
    private func createVideoSender(){
        if peerConnection == nil{
            return
        }
        let sender = peerConnection.sender(withKind: kRTCMediaStreamTrackKindVideo, streamId: "ARDAMS")
        localVideoTrack = createVideoTrack()
        if localVideoTrack != nil{
            sender.track = localVideoTrack
        }
    }
    
    private func createVideoTrack()-> RTCVideoTrack?{
        let videoTrack:RTCVideoTrack?
        // The iOS simulator doesn't provide any sort of camera capture
        // support or emulation (http://goo.gl/rHAnC1) so don't bother
        // trying to open a local stream.
        #if !TARGET_IPHONE_SIMULATOR
            let videoSource = peerConnectionFactory.avFoundationVideoSource(with: videoStreamConstraints)
            videoTrack = peerConnectionFactory.videoTrack(with: videoSource, trackId: "ARDAMSv0")
        #endif
        return videoTrack
    }
    
}
