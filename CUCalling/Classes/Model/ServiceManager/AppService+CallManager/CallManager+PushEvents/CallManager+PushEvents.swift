//
//  CallManager.swift
//  Nehao
//
//  Created by Abhimanu Jindal on 01/03/17.
//  Copyright © 2017 Amit Tripathi. All rights reserved.
//

extension (CallManager) {
    
    func setUpPushEvents(){
        pushEventTypes.append("requestToConnectToSocket")
        pushEventTypes.append("CallMissed")
        pushEventTypes.append("CallDisconnected")

    }
    
    func processPushMessage(message:PushMessage){
        if isMessageForCall(message: message){
            if isSocketConnected{
                callInitiationManager.processPushMessage(message: message)
                callOperationManager.processPushMessage(message: message)
            }
            else{
                pendingPushMessages.append(message)
            }
        }
    }
    
    func processPendingPushMessages(){
        for (_,message) in pendingPushMessages.enumerated(){
            print("Push Message processed")

            callInitiationManager.processPushMessage(message: message)
            callOperationManager.processPushMessage(message: message)
        }
        print("Push Message emty")

        pendingPushMessages.removeAll()
    }
    
    //MARK:- Private Function Method(s)
    private func isMessageForCall(message:PushMessage)->Bool{
        for (_,eventName) in pushEventTypes.enumerated(){
            if message.messageType == eventName{
                return true
            }
        }
        return false
    }
    
}
