//
//  AppConfigurations.swift
//  Nehao
//
//  Created by Abhimanu Jindal on 22/02/17.
//  Copyright � 2017 Amit Tripathi. All rights reserved.
//

import Foundation

class AppConfigurations: YAConfiguration {
    
    enum Environment:String {
        case devNew, releaseNewMumbai, liveNewAuth2
    }
    var environment:Environment = .liveNewAuth2
    
    override func socketServerHost() -> String? {
        let env =  environment
        switch  env {
        case .devNew:
            return "https://devsoc1.paycuapp.com"
        case .releaseNewMumbai:
            return "https://betasoc1.paycuapp.com"
            
        case .liveNewAuth2:
            return "https://securesocone.paycuapp.com"
            
        }
    }
    
    override func socketAPIClient() -> YASocketAPI {
        return YASocketAPI.initSocketAPIWithClient(client: SocketAPIClientSocketIO.sharedInstance)
    }
    
}
