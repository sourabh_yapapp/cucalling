//
//  DBServiceConfiguratorClient.swift
//  WebServiceTestApp
//
//  Created by Abhimanu Jindal on 09/02/17.
//  Copyright © 2017 YapApp India Pvt. Ltd. All rights reserved.
//

import UIKit

class DBServiceConfiguratorClient: YADBServiceConfigurator {
    
    //MARK:- Variables
    static let sharedInstance = DBServiceConfiguratorClient()
    
    //MARK:- Overridden Method(s)
    
    private override init(){
        super.init()
    }
    
    override func call(data:AnyObject?, methodType: YADBServiceRequest.MethodType, queryString:String?, sortQueryString:String?, sortType:YADBServiceRequest.SortType?, manager:YADBServiceManager, completionHandler: @escaping(_ result:AnyObject?,_ error:YAError?)->Void){
        
        manager.result = nil
        manager.error = nil
        //1.
        guard manager.serviceProtocol != nil else{
            manager.result = nil
            manager.error = YAError.generateError(description: "NO DBserviceManager Protocol Set", localizedDescription: "NO DBserviceManager Protocol Set", debugDescription: "NO DBserviceManager Protocol Set \(manager)", code: 404)
            completionHandler(nil, manager.error)
            return
        }
        var sort:YADBServiceRequest.SortType = .ASCENDING
        if sortType != nil{
            sort = sortType!
        }
        let serviceProtocol = manager.serviceProtocol!
        
        //2.
        let storageEncodingType = serviceProtocol.dbServiceStorageTypeEncoding()
        
        //3.
        let apiClient = serviceProtocol.dbServiceAPIClient()
        
        var request = YADBServiceRequest.initWithClient(client: DBServiceRequestClient())
        request = request.createRequest(query: queryString, methodType: methodType, data: data, sortQuery: sortQueryString, sortType: sort, storageEncodingType: storageEncodingType, completionHandler: completionHandler)
        
        apiClient.call(request: request, delegate: manager)
        
    }
    
}
