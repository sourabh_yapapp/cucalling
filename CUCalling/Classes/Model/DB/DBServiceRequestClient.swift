//
//  DBServiceRequestClient.swift
//  WebServiceTestApp
//
//  Created by Abhimanu Jindal on 09/02/17.
//  Copyright © 2017 YapApp India Pvt. Ltd. All rights reserved.
//

import UIKit

class DBServiceRequestClient: YADBServiceRequest {
    
    override func createRequest(query:String?, methodType:YADBServiceRequest.MethodType, data:AnyObject?, sortQuery:String?, sortType:YADBServiceRequest.SortType?, storageEncodingType:YADBServiceRequest.StorageTypeEncoding, completionHandler:@escaping (_ result:AnyObject?,_ error:YAError?)->Void) -> YADBServiceRequest {
        let serviceRequest  = YADBServiceRequest()
        serviceRequest.queryString = query
        serviceRequest.selectedMethodType = methodType
        serviceRequest.data = data
        serviceRequest.selectedSortType = sortType!
        serviceRequest.sortQueryString = sortQuery
        serviceRequest.selectedStorageTypeEncoding = storageEncodingType
        serviceRequest.selectedCompletionHandler = completionHandler
        
        return serviceRequest
    }
}
