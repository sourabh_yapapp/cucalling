//
//  DBServiceManagerICEServers.swift
//  Nehao
//
//  Created by Abhimanu Jindal on 02/03/17.
//  Copyright © 2017 Amit Tripathi. All rights reserved.
//

import Foundation

class DBServiceManagerICEServers: YADBServiceManager, YADBServiceManagerProtocol{
    
    override func callDBService(data: AnyObject?, methodType: YADBServiceRequest.MethodType, queryString: String?, sortQueryString: String?, sortType: YADBServiceRequest.SortType?, completionHandler: @escaping(_ result:AnyObject?,_ error:YAError?)->Void) {
        let dbServiceConfigurator = YADBServiceConfigurator.initConfiguratorWithClient(client: DBServiceConfiguratorClient.sharedInstance)
        self.serviceProtocol = self
        dbServiceConfigurator.call(data: data, methodType: methodType, queryString: queryString, sortQueryString: sortQueryString, sortType: sortType, manager:self, completionHandler:completionHandler)
    }
    
    //MARK:- YADBServiceManagerProtocol Method(s)
    func dbServiceAPIClient()->YADBServiceAPI{
        return YADBServiceAPI.initServiceAPIWithClient(client: YADBServiceAPIUserDefaults())
    }
    
    func dbServiceStorageTypeEncoding()->YADBServiceRequest.StorageTypeEncoding{
        return .PARAMETERS
    }
    
    //MARK:- ServiceManager Delegate Method(s)
    override func didCompleteAPI(sender: YAServiceAPI) {
        self.error = nil
        self.result = sender.result
        self.request = sender.request
        let request = sender.request as! YADBServiceRequest
        request.selectedCompletionHandler(sender.result, self.error)
        self.delegate?.didCompleteService(sender: self)
        
    }
    
    override func failedToCompleteAPI(sender: YAServiceAPI) {
        self.error = sender.error
        self.result = nil
        self.request = sender.request
        let request = sender.request as! YADBServiceRequest
        request.selectedCompletionHandler(sender.result, self.error)
        self.delegate?.failedToCompleteService(sender: self)
    }
}
