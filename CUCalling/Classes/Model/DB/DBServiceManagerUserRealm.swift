//
//  DBServiceManagerUserRealm.swift
//  Nehao
//
//  Created by Abhimanu Jindal on 14/02/17.
//  Copyright © 2017 Amit Tripathi. All rights reserved.
//

import UIKit

class DBServiceManagerUserRealm: YADBServiceManager, YADBServiceManagerProtocol {
    
    override func callDBService(data: AnyObject?, methodType: YADBServiceRequest.MethodType, queryString: String?, sortQueryString: String?, sortType: YADBServiceRequest.SortType?, completionHandler: @escaping(_ result:AnyObject?,_ error:YAError?)->Void) {
        let dbServiceConfigurator = YADBServiceConfigurator.initConfiguratorWithClient(client: DBServiceConfiguratorClient.sharedInstance)
        self.serviceProtocol = self
        dbServiceConfigurator.call(data: data, methodType: methodType, queryString: queryString, sortQueryString: sortQueryString, sortType: sortType, manager:self, completionHandler:completionHandler)
    }
    
    //MARK:- YADBServiceManagerProtocol Method(s)
    func dbServiceAPIClient()->YADBServiceAPI{
        return YADBServiceAPI.initServiceAPIWithClient(client: YADBServiceAPIRealm())
    }
    
    func dbServiceStorageTypeEncoding()->YADBServiceRequest.StorageTypeEncoding{
        return .MODEL
    }
    
    //MARK:- ServiceManager Delegate Method(s)
    override func didCompleteAPI(sender: YAServiceAPI) {
        self.error = nil
        self.result = sender.result
        self.request = sender.request
        let request = sender.request as! YADBServiceRequest
        request.selectedCompletionHandler(sender.result, self.error)
        self.delegate?.didCompleteService(sender: self)
        
    }
    
    override func failedToCompleteAPI(sender: YAServiceAPI) {
        self.error = sender.error
        self.result = nil
        self.request = sender.request
        let request = sender.request as! YADBServiceRequest
        request.selectedCompletionHandler(sender.result, self.error)
        self.delegate?.failedToCompleteService(sender: self)
    }
    /*
    public func fetchDatafromUserDB(queryString: String?, sortQueryString: String?, sortType: YADBServiceRequest.SortType?, successHandler: @escaping (_ result: User?, _ error: YAError?) -> ()) {
        self.callDBService(data: CustomDataModelUser(), methodType: .FETCH, queryString: queryString, sortQueryString: sortQueryString, sortType: sortType) { (resultData, error) in
            let tempResult = resultData as? [CustomDataModelUser]
            if (tempResult?.count == nil ? -1 : (tempResult?.count)!) > 0 {
                let tempUser = User()
                tempUser.customDataModel =  tempResult?[0]
                tempUser.mapCustomDataModelToModel()
                successHandler(tempUser, nil)
            }
            else {
                YALog.print("######Error in READLM DBSERVICE MANAGER#####")
                successHandler(nil, self.generateDBError())
            }
        }
    }
    
    public func insertDataInUserDB(user: User, successHandler: @escaping (_ result: User?, _ error: YAError?) -> ()) {
        var tempUser = User()
        tempUser = user
        tempUser.mapModelToCustomDataModel()
        self.callDBService(data: tempUser.customDataModel, methodType: .INSERT, queryString: nil, sortQueryString: nil, sortType: nil) { (resultData, error) in
            let tempResult = resultData as? [CustomDataModelUser]
            if (tempResult?.count == nil ? -1 : (tempResult?.count)!) > 0 {
                let tempUser = User()
                tempUser.customDataModel =  tempResult?[0]
                tempUser.mapCustomDataModelToModel()
                successHandler(tempUser, nil)
            }
            else {
                YALog.print("######Error in READLM DBSERVICE MANAGER#####")
                successHandler(nil, self.generateDBError())
            }
        }
    }
    */
    private func generateDBError() -> YAError{
        var tempError = YAError()
        tempError.description = "Error in READLM DBSERVICE MANAGER"
        return tempError
    }

    
}
