//
//  CallViewController.swift
//  CUCalling
//
//  Created by yapapp on 3/20/18.
//  Copyright © 2018 yapapp.com. All rights reserved.
//

import UIKit

protocol OngoingCallVCDelegate:class {
    func didRequestToUpdateMediaView(view:UIView, isLocal:Bool, shouldAdd:Bool)
    func ongoingEndCallButtonTapped()
    func ongoingMicButtonTapped(isMicEnabled:Bool)
    func ongoingSpeakerButtonTapped(isSpeakerEnabled:Bool)
    func ongoingAudioVideoButtonTapped(isVideoEnabled:Bool)
    func ongoingCameraButtonTapped()
//    func ongoingChatButtonTapped(contact:Contact)
    func updateCurrentStatus()
    func sendVideoStatusAfterCallAccept(isVideoEnabled: Bool)
    func ongoingCallVC( _ viewController: CallViewController, checkNetworkRangeIsPoor completionBlock: @escaping (Bool) -> Void)
//    func didUpdateCallContact(contact:Contact)
}

 class CallViewController: UIViewController, OngoingCallViewDelegate {

    //MARK:- Constants/Variabless
    weak var delegate:OngoingCallVCDelegate?
    
    fileprivate var ongoingCallView:CallView!
    
    var ischatTapped:Bool = false
    private var isVideoScreen:Bool = false
    private var isLocalVideo = false
//    var contact:Contact?
    var initialBTAvaiable: Bool?
    var initialValueForSpeaker: Bool = true
    
    //MARK:- Overriden Method(S)
    override func viewDidLoad() {
        super.viewDidLoad()
        ischatTapped = false
        ongoingCallView = self.view as! CallView
        ongoingCallView?.delegate = self
//        ongoingCallView?.setupInitialViews()
        
        if isVideoScreen{
            ongoingCallView?.isRemoteVideo = true
            ongoingCallView?.displayVideoScreen()
            
            //ongoingSpeakerButtonTapped(isSpeakerEnabled: true)
        }
        else{
            ongoingCallView?.displayAudioScreen(onView: (self.ongoingCallView?.remoteUserView)!)
            //    ongoingSpeakerButtonTapped(isSpeakerEnabled: false)
            // ongoingCallView?.setSpeakerControl(isSpeakerEnabled: isLocalVideo)
            
        }
        
        ongoingCallView?.setLocalVideoControl(isLocalVideo: isLocalVideo)
        ongoingCallView?.setAudioVideoControl(isVideoEnabled:isLocalVideo)
        handleCallCurrentStatus(state: "")
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    /*
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    */
    override func viewWillAppear(_ animated: Bool) {
        if !ischatTapped{
//            let imageURL = contact?.remoteThumbImageURL
//            ongoingCallView?.setConatactImage(imageURL: imageURL)
        }else{
            YALog.print("Display ongoing time")
        }
//        ongoingCallView?.updateContactLabel(contact?.name)
//        receivedNewChatMessage(unreadCount: (contact?.unreadMessagesCount)!)
        if initialBTAvaiable != nil
        {
            setInitialSpeakerButtonView(isBTConnected: initialBTAvaiable!)
            initialBTAvaiable = nil
        }
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        if !ischatTapped{
//            self.delegate?.didRequestToUpdateMediaView(view: (self.ongoingCallView?.getOngoingVideoMediaViewRemote())!, isLocal: false, shouldAdd: false)
            self.delegate?.didRequestToUpdateMediaView(view: (self.ongoingCallView?.getOngoingMediaViewLocal())!, isLocal: true, shouldAdd: false)
        }else{
            YALog.print("Display ongoing time")
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if !ischatTapped{
            DispatchQueue.main.async {
//                self.delegate?.didRequestToUpdateMediaView(view: (self.ongoingCallView?.getOngoingVideoMediaViewRemote())!, isLocal: false, shouldAdd: true)
                self.delegate?.didRequestToUpdateMediaView(view: (self.ongoingCallView?.getOngoingMediaViewLocal())!, isLocal: true, shouldAdd: true)
            }
            self.delegate?.updateCurrentStatus()
            self.updateMediaStreamViewsAfterRTCConnection()
            self.delegate?.sendVideoStatusAfterCallAccept(isVideoEnabled: isLocalVideo)
        }
        else{
            ischatTapped = false
        }
        
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:- Public Method(s)
    func contactNameUpdated(contact: Contact)
    {
//        self.contact = contact
        ongoingCallView?.updateContactLabel(contact.name)
//        self.delegate?.didUpdateCallContact(contact: contact)
    }
    
    func receivedNewChatMessage(unreadCount :Int)
    {
        ongoingCallView?.recievedNewChatMessage(unreadCount: unreadCount)
    }
    
    
    func setUpAudioVideoScreen(contact:Contact?, isVideo:Bool, isLocalVideo:Bool){
        isVideoScreen = isVideo
        self.isLocalVideo = isLocalVideo
//        self.contact = contact
    }
    
    func updateRemoteViewSize(size: CGSize){
//        ongoingCallView?.setRemoteViewSize(size: size)
    }
    
    func updateSpeakerView(isSpeaker:Bool){
        ongoingCallView?.setSpeakerControl(isSpeakerEnabled: isSpeaker)
    }
    
    
    func updateRemoteVideoCallScreen(isVideoEnabled:Bool){
//        self.showLoader(title: "Loading", progressView: YAProgressView.initProgressViewWithClient(client: YAProgressViewMBProgressHUD()))
        self.ongoingCallView?.isRemoteVideo = isVideoEnabled
//        self.ongoingCallView?.swapCallViews(isSelfViewLarge: (self.ongoingCallView?.isSelfViewLarge)!, remoteVideo: (self.ongoingCallView?.isRemoteVideo)!, myVido: self.isLocalVideo)
        
    }
    
    func updateRemoteAudioCallScreen(isAudioMuted:Bool){
        
    }
    
    func updateMediaStreamViewsAfterRTCConnection()
    {
        DispatchQueue.main.asyncAfter(deadline: .now()+2, execute: {
            if self.ongoingCallView?.getOngoingMediaViewLocal() != nil{
                self.delegate?.didRequestToUpdateMediaView(view: (self.ongoingCallView?.getOngoingMediaViewLocal())!, isLocal: true, shouldAdd: true)
            }
//            if self.ongoingCallView?.getOngoingVideoMediaViewRemote() != nil{
//                self.delegate?.didRequestToUpdateMediaView(view: (self.ongoingCallView?.getOngoingVideoMediaViewRemote())!, isLocal: false, shouldAdd: true)
//            }
        })
        
    }
    
    func updateReconnectingViewVisibility(isVisible: Bool)
    {
        
        DispatchQueue.main.async {
//            self.ongoingCallView?.setVisibilityOfReconnectingView(isVisible: isVisible)
        }
    }
    
    //MARK:- OngoingCallView Delegate Methods
    func endCallButtonTapped() {
        self.delegate?.ongoingEndCallButtonTapped()
    }
    
    
    func ongoingChatButtonTapped(){
//        if contact != nil{
//            self.delegate?.ongoingChatButtonTapped(contact:contact!)
//        }
    }
    
    func ongoingMicRecordButtonTapped(isMicEnabled:Bool){
        self.delegate?.ongoingMicButtonTapped(isMicEnabled: isMicEnabled)
    }
    
    func ongoingSpeakerButtonTapped(isSpeakerEnabled:Bool){
        self.delegate?.ongoingSpeakerButtonTapped(isSpeakerEnabled: isSpeakerEnabled)
    }
    
    
    
    
    func setInitialSpeakerButtonView(isBTConnected: Bool)
    {
        if ongoingCallView == nil{
            
            //TODO Tina
            initialBTAvaiable = isBTConnected
            return
        }
        self.updateAudioControlButton(isBTConnected: isBTConnected)
        initialBTAvaiable = nil
    }
    
    func updateAudioControlButton(isBTConnected: Bool)
    {
        ongoingCallView?.setSpeakerButton(isBTConnected: isBTConnected)
    }
    
    func ongoingAudioVideoButtonTapped(isVideoEnabled:Bool){
        isLocalVideo = isVideoEnabled
//        ongoingCallView?.swapCallViews(isSelfViewLarge: (ongoingCallView?.isSelfViewLarge)!, remoteVideo: (ongoingCallView?.isRemoteVideo)!, myVido: isLocalVideo)
        
        self.delegate?.ongoingAudioVideoButtonTapped(isVideoEnabled: isVideoEnabled)
    }
    
    func ongoingCall(_ view: CallView, chekcIfNetworkIsPoor completionBlock: @escaping (Bool) -> Void) {
        self.delegate?.ongoingCallVC(self, checkNetworkRangeIsPoor:{ (isPoor) in
            completionBlock(isPoor)
        })
    }
    
    func ongoingCameraButtonTapped(){
        self.delegate?.ongoingCameraButtonTapped()
    }
    
    func ongoingswapVideosButtontapped(_ swapValue:Bool){
        
    }
    
    func updateOngoingCallTime(time:Int)
    {
        ongoingCallView?.handleUpdatedCallTime(time : time)
    }
    
    func updateOngoingVideoButtonEnabled()
    {
//        ongoingCallView?.enableVideoButton()
    }
    
    func setInitialValueForSpeakerControl(isEnabled: Bool)
    {
        if self.ongoingCallView == nil
        {
            initialValueForSpeaker = isEnabled
        }
        initialValueForSpeaker = isEnabled
        
        ongoingCallView?.setSpeakerControl(isSpeakerEnabled: isEnabled)
    }
    
    func setInitialValueForMicControl(isEnabled: Bool)
    {
        ongoingCallView?.setMicControl(isMicEnabled: isEnabled)
    }
    
    func handleNetworkConnection(isSlow: Bool){
//        ongoingCallView?.handleNetworkConnection(isSlow: isSlow)
    }
    
    //MARK:- Video Paused View
    func handleRemoteVideoPaused(shouldPause: Bool){
//        ongoingCallView?.setVisibilityOfRemoteVideoPauseView(isVisible: shouldPause)
    }
    
    func handleCalleeAppState(isBackground: Bool){
//        ongoingCallView?.setAppStateOfCallee(isPauseVisible: isBackground)
    }
    
    //MARK:- Call State Delegate
    func handleCallCurrentStatus(state: String) {
        self.ongoingCallView?.updateCallCurrentStatus(state: state)
    }
    
}

