//
//  YADBServiceAPIRealm.swift
//  WebServiceTestApp
//
//  Created by Abhimanu Jindal on 09/02/17.
//  Copyright © 2017 YapApp India Pvt. Ltd. All rights reserved.
//

import UIKit
import RealmSwift

protocol DetachableObject: AnyObject {
    
    func detached() -> Self
    
}

extension Object: DetachableObject {
    
    func detached() -> Self {
        let detached = type(of: self).init()
        for property in objectSchema.properties {
            guard let value = value(forKey: property.name) else { continue }
            if let detachable = value as? DetachableObject {
                detached.setValue(detachable.detached(), forKey: property.name)
            } else {
                detached.setValue(value, forKey: property.name)
            }
        }
        return detached
    }
    
}

extension List: DetachableObject {
    
    func detached() -> List<Element> {
        let result = List<Element>()
        forEach {
            result.append($0.detached())
        }
        return result
    }
    
}

class YADBServiceAPIRealm: YADBServiceAPI {
    
    private var dbRequest:YADBServiceRequest?
    
    override func purgeDB(){
        let realm = try! Realm()
        try! realm.write {
            realm.deleteAll()
        }
    }

    override func purgeDBBeforeMigration(){
        let configuration = Realm.Configuration(deleteRealmIfMigrationNeeded: true)
        Realm.Configuration.defaultConfiguration = configuration
        do { _ = try Realm() } catch {}
    }
    
    
    override func initiateDBMigration() {
        // SCHEMA MIGRATION
        // copy over old data files for migration
        var tempVesionNumber : String =  Bundle.main.infoDictionary?["CFBundleShortVersionString"] as! String
        tempVesionNumber.append(Bundle.main.infoDictionary?["CFBundleVersion"] as! String)
        tempVesionNumber = tempVesionNumber.replacingOccurrences(of: " ", with: "")
        tempVesionNumber = tempVesionNumber.replacingOccurrences(of: ".", with: "")
        var schemaVesionToSet:UInt64  = 0
        if let tempSchemaVesionToSet:Int = Int(tempVesionNumber) {
            schemaVesionToSet = UInt64(tempSchemaVesionToSet)
        }
        DispatchQueue.global(qos: .userInitiated).async {
        let config = Realm.Configuration(
            // Set the new schema version. This must be greater than the previously used
            // version (if you've never set a schema version before, the version is 0).
            schemaVersion: schemaVesionToSet,
            
            // Set the block which will be called automatically when opening a Realm with
            // a schema version lower than the one set above
            migrationBlock: { migration, oldSchemaVersion in
                // We haven’t migrated anything yet, so oldSchemaVersion == 0
                if (oldSchemaVersion < schemaVesionToSet) {
                    
                    // Nothing to do!
                    // Realm will automatically detect new properties and removed properties
                    // And will update the schema on disk automatically
                    
                }
                
        })
        
        // Tell Realm to use this new configuration object for the default Realm
        Realm.Configuration.defaultConfiguration = config
        
        // Now that we've told Realm how to handle the schema change, opening the file
        // will automatically perform the migration
        let _ = try! Realm()
        }
        
    }


    override func call(request:YADBServiceRequest, delegate:YAServiceAPIDelegate) {
        self.result = nil
        self.error = nil
        
        self.request = request
        dbRequest = request
        self.delegate = delegate
        DispatchQueue.global(qos: .userInitiated).async {
            switch request.selectedMethodType {
            case .INSERT:
                self.insertData()
            case .FETCH:
                self.fetchData()
            case .UPDATE:
                self.updateData()
            case .DELETE:
                self.deleteData()
            }
        }
    }
    
    //MARK:- Private Method(s)
    private func insertData(){
        var data = dbRequest?.data as? Object
        data = data?.detached()
        if data == nil{
            self.result = nil
            DispatchQueue.main.async {
                self.error = YAError.generateError(description: "No Realm Data To Insert", localizedDescription: "No Realm Data To Insert", debugDescription: "No Realm Data To Insert", code: YAError.CodeType.DBError.rawValue)
                self.delegate?.failedToCompleteAPI(sender: self)
            }
            return
        }
        if !((data?.isKind(of: Object.self))!){
            self.result = nil
            DispatchQueue.main.async {
                self.error = YAError.generateError(description: "Data is not of Realm Object", localizedDescription: "Data is not of Realm Object", debugDescription: "Data is not of Realm Object", code: YAError.CodeType.DBError.rawValue)
                self.delegate?.failedToCompleteAPI(sender: self)
            }
            return
        }
        do {
            let realm = try Realm()
            try realm.write() {
                realm.add(data!, update: true)
            }
            var finalResult:[Object] = []
            finalResult.append((data?.detached())!)
            self.result = finalResult as AnyObject?
            DispatchQueue.main.async {
                self.delegate?.didCompleteAPI(sender: self)
            }
        } catch let error as NSError {
            self.result = nil
            self.error = YAError.generateError(description: error.description, localizedDescription: error.description, debugDescription: error.debugDescription, code: error.code)
            self.delegate?.failedToCompleteAPI(sender: self)
        }
    }
    
    private func fetchData(){
        var data = dbRequest?.data as? Object
        data = data?.detached()
        if data == nil{
            self.result = nil
            DispatchQueue.main.async {
                self.error = YAError.generateError(description: "No Realm Data To Fetch", localizedDescription: "No Realm Data To Fetch", debugDescription: "No Realm Data To Fetch", code: YAError.CodeType.DBError.rawValue)
                self.delegate?.failedToCompleteAPI(sender: self)
            }
            return
        }
        if !(data?.isKind(of: Object.self))!{
            self.result = nil
            DispatchQueue.main.async {
                self.error = YAError.generateError(description: "Data is not of Realm Object", localizedDescription: "Data is not of Realm Object", debugDescription: "Data is not of Realm Object", code: YAError.CodeType.DBError.rawValue)
                self.delegate?.failedToCompleteAPI(sender: self)
            }
            return
        }
        
        do {
            let realm = try Realm()
            var result:Results<Object>?
            if dbRequest?.queryString?.isEmpty ?? true{
                //print(request?.data)
                if dbRequest?.sortQueryString?.isEmpty ?? true{
                    result = realm.objects(type(of: data!))
                }
                else{
                    result = realm.objects(type(of: data!)).sorted(byKeyPath: (dbRequest?.sortQueryString)!, ascending:dbRequest?.selectedSortType == .ASCENDING ? true : false)
                }
                
            }
            else{
                if dbRequest?.sortQueryString?.isEmpty ?? true{
                    result = realm.objects(type(of: data!)).filter((dbRequest?.queryString)!)
                }
                else{
                    result = realm.objects(type(of: data!)).filter((dbRequest?.queryString)!).sorted(byKeyPath: (dbRequest?.sortQueryString)!, ascending:dbRequest?.selectedSortType == .ASCENDING ? true : false)
                }
            }
            
            if let result = result{
                var finalResult:[Object] = []
                for (_,object) in result.enumerated(){
                    finalResult.append(object.detached())
                }
                self.result = finalResult as AnyObject?
            }
            else{
                self.result = nil
            }
            DispatchQueue.main.async {
                self.delegate?.didCompleteAPI(sender: self)
            }
            
        } catch let error as NSError {
            self.result = nil
            self.error = YAError.generateError(description: error.description, localizedDescription: error.description, debugDescription: error.debugDescription, code: error.code)
            self.delegate?.failedToCompleteAPI(sender: self)
        }
    }
    
    private func updateData(){
        insertData()
    }
    private func deleteData(){
        let data = dbRequest?.data as? Object
        if data == nil{
            self.result = nil
            DispatchQueue.main.async {
                self.error = YAError.generateError(description: "No Realm Data To Delete", localizedDescription: "No Realm Data To Delete", debugDescription: "No Realm Data To Delete", code: YAError.CodeType.DBError.rawValue)
                self.delegate?.failedToCompleteAPI(sender: self)
            }
            return
        }
        if !(data?.isKind(of: Object.self))!{
            self.result = nil
            DispatchQueue.main.async {
                self.error = YAError.generateError(description: "Data is not of Realm Object", localizedDescription: "Data is not of Realm Object", debugDescription: "Data is not of Realm Object", code: YAError.CodeType.DBError.rawValue)
                self.delegate?.failedToCompleteAPI(sender: self)
            }
            return
        }
        do {
            let realm = try Realm()
            try realm.write() {
                let primaryKeyProperty = data?.objectSchema.primaryKeyProperty
                let primaryKeyName = primaryKeyProperty?.name
                if primaryKeyName?.isEmpty ?? true{
                    DispatchQueue.main.async {
                        self.error = YAError.generateError(description: "No Primary Key associated to delete Realm Object", localizedDescription: "No Primary Key associated to delete Realm Object", debugDescription: "No Primary Key associated to delete Realm Object", code: YAError.CodeType.DBError.rawValue)
                        self.delegate?.failedToCompleteAPI(sender: self)
                    }
                    return
                }
                
                let primaryKeyValue = data?.value(forKeyPath: primaryKeyName!)
                if primaryKeyValue == nil{
                    DispatchQueue.main.async {
                        self.error = YAError.generateError(description: "No Primary Key associated to delete Realm Object", localizedDescription: "No Primary Key associated to delete Realm Object", debugDescription: "No Primary Key associated to delete Realm Object", code: YAError.CodeType.DBError.rawValue)
                        self.delegate?.failedToCompleteAPI(sender: self)
                    }
                    return
                }
                
                realm.delete(realm.object(ofType: type(of: data!), forPrimaryKey: primaryKeyValue)!)
            }
            self.result = data?.detached()
            DispatchQueue.main.async {
                self.delegate?.didCompleteAPI(sender: self)
            }
            
        } catch let error as NSError {
            DispatchQueue.main.async {
                self.result = nil
                self.error = YAError.generateError(description: error.description, localizedDescription: error.description, debugDescription: error.debugDescription, code: error.code)
                self.delegate?.failedToCompleteAPI(sender: self)
            }
        }
    }
    
}
