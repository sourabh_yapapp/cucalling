//
//  YADBServiceAPIUserDefaults.swift
//  WebServiceTestApp
//
//  Created by Abhimanu Jindal on 09/02/17.
//  Copyright © 2017 YapApp India Pvt. Ltd. All rights reserved.
//

import UIKit

class YADBServiceAPIUserDefaults: YADBServiceAPI {
    
    private var dbRequest:YADBServiceRequest?

    override func call(request:YADBServiceRequest, delegate:YAServiceAPIDelegate) {
        self.request = request
        dbRequest = request
        self.delegate = delegate
        DispatchQueue.global(qos: .userInitiated).async {
            switch request.selectedMethodType {
            case .INSERT:
                self.insertData()
            case .FETCH:
                self.fetchData()
            case .UPDATE:
                self.updateData()
            case .DELETE:
                self.deleteData()
            }
        }
    }
    
    //MARK:- Private Method(s)
    private func insertData(){
        if dbRequest?.selectedStorageTypeEncoding == .MODEL{
            self.error = YAError.generateError(description: "Data is not in Parameter Format", localizedDescription: "Data is not in Parameter Format", debugDescription: "Data is not in Parameter Format", code: YAError.CodeType.DBError.rawValue)
            self.delegate?.failedToCompleteAPI(sender: self)
            return
        }
        if dbRequest?.queryString?.isEmpty ?? true{
            self.error = YAError.generateError(description: "Query String is empty", localizedDescription: "Query String is empty", debugDescription: "Query String is empty", code: YAError.CodeType.DBError.rawValue)
            self.delegate?.failedToCompleteAPI(sender: self)
            return
        }
        if !isDataTypeValid(){
            self.error = YAError.generateError(description: "Data is not of Valid DataType - Only Data, Array, Dictionary, Number, String, Date type can be stored", localizedDescription: "Data is not of Valid DataType  - Only Data, Array, Dictionary, Number, String, Date type can be stored", debugDescription: "Data is not of Valid DataType  - Only Data, Array, Dictionary, Number, String, Date type can be stored", code: YAError.CodeType.DBError.rawValue)
            self.delegate?.failedToCompleteAPI(sender: self)
            return
        }
        let data = NSKeyedArchiver.archivedData(withRootObject: dbRequest?.data as Any)
        UserDefaults.standard.setValue(data, forKey: (dbRequest?.queryString)!)
        DispatchQueue.main.sync {
            self.result = dbRequest?.data
            self.delegate?.didCompleteAPI(sender: self)
        }
        
    }
    private func fetchData(){
        if dbRequest?.queryString?.isEmpty ?? true{
            self.error = YAError.generateError(description: "Query String is empty", localizedDescription: "Query String is empty", debugDescription: "Query String is empty", code: YAError.CodeType.DBError.rawValue)
            self.delegate?.failedToCompleteAPI(sender: self)
            return
        }
        let data = UserDefaults.standard.value(forKey: (dbRequest?.queryString)!)
        guard (data != nil) else {
            DispatchQueue.main.sync {
                self.result = nil
                self.delegate?.failedToCompleteAPI(sender: self)
            }
            return
        }
        let object = NSKeyedUnarchiver.unarchiveObject(with: data as! Data) as AnyObject
        DispatchQueue.main.sync {
            self.result = object
            self.delegate?.didCompleteAPI(sender: self)
        }
    }
    private func updateData(){
        insertData()
    }
    private func deleteData(){
        if dbRequest?.queryString?.isEmpty ?? true{
            self.error = YAError.generateError(description: "Query String is empty", localizedDescription: "Query String is empty", debugDescription: "Query String is empty", code: YAError.CodeType.DBError.rawValue)
            self.delegate?.failedToCompleteAPI(sender: self)
            return
        }
        UserDefaults.standard.removeObject(forKey: (dbRequest?.queryString)!)
        DispatchQueue.main.sync {
            self.result = nil
            self.delegate?.failedToCompleteAPI(sender: self)
        }
    }
    
    private func isDataTypeValid()->Bool{
        if self.dbRequest?.data is Data || self.dbRequest?.data is NSData ||   self.dbRequest?.data is Date || self.dbRequest?.data is NSDate || self.dbRequest?.data is String || self.dbRequest?.data is NSString || self.dbRequest?.data is NSNumber || self.dbRequest?.data is Array<Any> || self.dbRequest?.data is NSArray || self.dbRequest?.data is Dictionary<AnyHashable,Any> || self.dbRequest?.data is NSDictionary{
            return true
        }
        return false
    }
    func purgeUserDefaults(){
        let appDomain = Bundle.main.bundleIdentifier!
        UserDefaults.standard.removePersistentDomain(forName: appDomain)
    }
}
