//
//  YADBServiceRequest.swift
//  YapAppCallingModule
//
//  Created by Abhimanu Jindal on 23/11/16.
//  Copyright © 2016 YapApp India Pvt. Ltd. All rights reserved.
//

import UIKit

open class YADBServiceRequest: NSObject {
    
    public enum MethodType {
        case FETCH
        case INSERT
        case UPDATE
        case DELETE
    }
    
    public enum SortType {
        case ASCENDING
        case DESCENDING
        case NONE
    }
    
    public enum StorageTypeEncoding {
        case PARAMETERS
        case MODEL
    }
    
    open var selectedMethodType:MethodType = .FETCH
    open var selectedSortType:SortType = .ASCENDING
    open var selectedStorageTypeEncoding:StorageTypeEncoding = .MODEL
    open var data:AnyObject?
    open var queryString:String?
    open var sortQueryString:String?
    open var selectedCompletionHandler:(_ result:AnyObject?,_ error:YAError?)->Void = {_,_ in }
    static func initWithClient(client:YADBServiceRequest) -> YADBServiceRequest{
        return client
    }
    
    open func createRequest(query:String?, methodType:YADBServiceRequest.MethodType, data:AnyObject?, sortQuery:String?, sortType:YADBServiceRequest.SortType, storageEncodingType:YADBServiceRequest.StorageTypeEncoding, completionHandler: @escaping(_ result:AnyObject?,_ error:YAError?)->Void) -> YADBServiceRequest {
        fatalError("This function should can only be called by its concrete class")
    }
}
