//
//  YADBServiceAPI.swift
//  YapAppCallingModule
//
//  Created by Abhimanu Jindal on 23/11/16.
//  Copyright © 2016 YapApp India Pvt. Ltd. All rights reserved.
//

import UIKit

open class YADBServiceAPI: YAServiceAPI {

    open static func initServiceAPIWithClient(client:YADBServiceAPI) -> YADBServiceAPI{
        return client
    }
    
    open func call(request:YADBServiceRequest, delegate:YAServiceAPIDelegate) {
        print("This should not be called. Only the inherited class should be called")
    }
    
    open func initiateDBMigration(){
    
    }
    open func purgeDB(){
        
    }
    open func purgeDBBeforeMigration() {
        
    }
    
}
