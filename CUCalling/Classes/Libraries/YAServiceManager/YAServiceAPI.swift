//
//  YAServiceAPI.swift
//  YapAppCallingModule
//
//  Created by Abhimanu Jindal on 24/11/16.
//  Copyright © 2016 YapApp India Pvt. Ltd. All rights reserved.
//

import UIKit

public protocol YAServiceAPIDelegate: class {
    func didCompleteAPI(sender: YAServiceAPI)
    func failedToCompleteAPI(sender: YAServiceAPI)
}

open class YAServiceAPI: NSObject {
    open weak var delegate:YAServiceAPIDelegate?
    open var result:AnyObject?
    open var responseHeader:AnyObject?
    open var request:AnyObject?
    open var error:YAError?
}
