//
//  SocketEventAPIAuthorization.swift
//  YapAppCallingModule
//
//  Created by Abhimanu Jindal on 01/12/16.
//  Copyright © 2016 YapApp India Pvt. Ltd. All rights reserved.
//

import UIKit

protocol YASocketEventAuthorizationDelegate: YASocketEventDelegate  {
    func didAuthorizeSuccessfully(socketEvent:YASocketEvent)
    func failedToAuthorizeSocketConnection(socketEvent:YASocketEvent)    
}

class YASocketEventAuthorization: YASocketEvent {

    static func initSocketEventAPIWithClient(client:YASocketEventAuthorization, delegate:YASocketEventDelegate, socketAPI:YASocketAPI) -> YASocketEventAuthorization{
        client.delegate = delegate
        client.socketAPI = socketAPI
        return client
    }
    
    //MARK:- Public Method(S)
    func authorizeConnection(accessToken:String) {
        fatalError("This function should can only be called by its concrete class")
        
    }
    
    func authorizationStatus()->Bool {
        fatalError("This function should can only be called by its concrete class")
    }
    
    func setAuthorizationStatus(value:Bool) {
        fatalError("This function should can only be called by its concrete class")
    }
    
    override func handleSocketEvents(){
        fatalError("This function should can only be called by its concrete class")
    }
    
    override func removeSocketEvents() {
        fatalError("This function should can only be called by its concrete class")
    }
}
