//
//  YASocketMessage.swift
//  YapAppCallingModule
//
//  Created by Abhimanu Jindal on 02/12/16.
//  Copyright © 2016 YapApp India Pvt. Ltd. All rights reserved.
//

import UIKit

struct YASocketMessage{
    var type:String?
    var statusCode:Int?
    var data:AnyObject?
    var statusMessage:String?
    var errorType:String?
}
