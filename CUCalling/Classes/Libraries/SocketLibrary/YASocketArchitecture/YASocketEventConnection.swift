//
//  YASocketEventAPIConnection.swift
//  YapAppCallingModule
//
//  Created by Abhimanu Jindal on 01/12/16.
//  Copyright © 2016 YapApp India Pvt. Ltd. All rights reserved.
//

import UIKit

protocol YASocketEventConnectionDelegate: YASocketEventDelegate  {
    func didConnectSocket(socketEvent:YASocketEvent)
    func failedToConnectSocket(socketEvent:YASocketEvent)
    
    func didDisconnectSocket(socketEvent:YASocketEvent)
    
}

class YASocketEventConnection: YASocketEvent {
    
    static func initSocketEventAPIWithClient(client:YASocketEventConnection, delegate:YASocketEventDelegate, socketAPI:YASocketAPI) -> YASocketEventConnection{
        client.delegate = delegate
        client.socketAPI = socketAPI
        return client
    }
    
    
    //MARK:- Public Method(S)
    func establishConnection() {
        fatalError("This function should can only be called by its concrete class")

    }
    
    func closeConnection() {
        fatalError("This function should can only be called by its concrete class")
    }
    
    override func handleSocketEvents(){
        fatalError("This function should can only be called by its concrete class")
    }
    
    override func removeSocketEvents() {
        fatalError("This function should can only be called by its concrete class")
    }
    
}
