//
//  YASocketEvent.swift
//  YapAppCallingModule
//
//  Created by Abhimanu Jindal on 01/12/16.
//  Copyright © 2016 YapApp India Pvt. Ltd. All rights reserved.
//

import UIKit

protocol YASocketEventDelegate:class {
    func socketEventReceived(socketEvent:YASocketEvent)
    func socketEventFailed(socketEvent:YASocketEvent)
}

class YASocketEvent: NSObject {
    weak var delegate:YASocketEventDelegate?
    var socketAPI:YASocketAPI?
    lazy var eventNames:[String] = []

    func handleSocketEvents(){
        fatalError("This function should can only be called by its concrete class")
    }
    
    func removeSocketEvents() {
        fatalError("This function should can only be called by its concrete class")
    }
    
}
