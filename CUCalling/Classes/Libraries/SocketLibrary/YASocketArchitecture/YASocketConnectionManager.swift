//
//  YASocketConnectionManager.swift
//  Nehao
//
//  Created by Abhimanu Jindal on 06/02/17.
//  Copyright © 2017 Amit Tripathi. All rights reserved.
//

import UIKit

enum SocketConnectionType {
    case Connected
    case Disconnected
    case Connecting
    case NotConnected
}

protocol YASocketConnectionManagerDelegate:class {
    func connectionStatusChanged(type:SocketConnectionType)
}

class YASocketConnectionManager: NSObject {

    var delegate:YASocketConnectionManagerDelegate?

    static func initManagerWithClient(client:YASocketConnectionManager) -> YASocketConnectionManager{
        return client
    }
    
    func connectToSocket(accessToken:String?, delegate:YASocketConnectionManagerDelegate?){
        fatalError("This function should can only be called by its concrete class")
    }
    
    func disconnectToSocket(){
        fatalError("This function should can only be called by its concrete class")
    }
    
    func getSocketConnectionState()-> SocketConnectionType{
        fatalError("This function should can only be called by its concrete class")
    }
    
    

}
