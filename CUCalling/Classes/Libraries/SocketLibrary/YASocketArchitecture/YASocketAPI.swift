//
//  YASocketAPI.swift
//  YapAppCallingModule
//
//  Created by Abhimanu Jindal on 28/11/16.
//  Copyright © 2016 YapApp India Pvt. Ltd. All rights reserved.
//

import UIKit
protocol YASocketAPIDelegate:class {
    func didReceiveEvent(socketAPI:YASocketAPI)
    func didReceiveEventFailure(socketAPI:YASocketAPI)

}

struct DelegateModel{
    var delegate:YASocketAPIDelegate
    var eventName:String
}

enum YAConnectionStatus {
    case Connected
    case Connecting
    case NotConnected
}

class YASocketAPI: NSObject {
    
    var message:YASocketMessage?
    var isSocketAuthorized:Bool = false
    
    static func initSocketAPIWithClient(client:YASocketAPI) -> YASocketAPI{
        return client
    }
    
    //MARK:- Public Method(S)
    func connect() {
        fatalError("This function should can only be called by its concrete class")
    }
    
    func isConnected() -> Bool{
        fatalError("This function should can only be called by its concrete class")
    }
    
    func connectionStatus() -> YAConnectionStatus{
        fatalError("This function should can only be called by its concrete class")
    }
    
    func disconnect() {
        fatalError("This function should can only be called by its concrete class")
    }
    
    func sendEvent(event:String, data:[Any]?){
        fatalError("This function should can only be called by its concrete class")
    }
    
    func addEvent(eventName:String, delegate:YASocketAPIDelegate){
        fatalError("This function should can only be called by its concrete class")
    }
    
    func removeEvent(eventName: String, delegate: YASocketAPIDelegate) {
        fatalError("This function should can only be called by its concrete class")
    }
}
