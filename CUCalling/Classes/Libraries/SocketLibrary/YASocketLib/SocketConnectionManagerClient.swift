//
//  SocketConnectionManagerClient.swift
//  Nehao
//
//  Created by Abhimanu Jindal on 06/02/17.
//  Copyright © 2017 Amit Tripathi. All rights reserved.
//

import UIKit

class SocketConnectionManagerClient: YASocketConnectionManager, YASocketEventConnectionDelegate, YASocketEventAuthorizationDelegate {
    
    //MARK:- Variables/ Constants
    
    var connectionStatus:SocketConnectionType{
        return status
    }
    
    private var accessToken:String?
    private var isSocketAuthorized:Bool = false
    private var status:SocketConnectionType = .NotConnected{
        didSet {
            self.delegate?.connectionStatusChanged(type: status)
        }
    }

    //Socket Events
    private var socketEventConnection:YASocketEventConnection?
    private var socketEventAuthorization:YASocketEventAuthorization?
    
    private func authorizeConnection(){
        if let accessToken = self.accessToken{
            socketEventAuthorization?.authorizeConnection(accessToken: accessToken)
            return
        }
        YALog.print("No Access Token to Authorize")
        self.status = .Disconnected
    }
    
    //MARK:- Public Method(s)
    override init(){
        super.init()
        let socketConfigurationClient = YAConfiguration.initConfigurationWithClient(client:AppConfigurations())
        
        let socketAPI = socketConfigurationClient.socketAPIClient()
        
        socketEventConnection = YASocketEventConnection.initSocketEventAPIWithClient(client: SocketEventConnectionClient(), delegate: self, socketAPI: socketAPI)
        socketEventConnection?.handleSocketEvents()
        
        socketEventAuthorization = YASocketEventAuthorization.initSocketEventAPIWithClient(client:SocketEventAuthorizationClient(), delegate: self, socketAPI: socketAPI)
        socketEventAuthorization?.handleSocketEvents()
        
        let connectionStatus = socketEventConnection?.socketAPI?.connectionStatus()
        if connectionStatus == .Connected{
            self.status = .Connected
        }
        else if connectionStatus == .Connecting{
            self.status = .Connecting
        }
        else if connectionStatus == .NotConnected{
            self.status = .NotConnected
        }
        else{
            self.status = .Disconnected
        }
    }
    
    override func connectToSocket(accessToken:String?, delegate:YASocketConnectionManagerDelegate?){
        self.accessToken = accessToken
        self.delegate = delegate
        let connectionStatus = socketEventConnection?.socketAPI?.connectionStatus()
        isSocketAuthorized = (socketEventAuthorization?.authorizationStatus())!
        if connectionStatus == .NotConnected{
            self.status = .Connecting
            socketEventConnection?.establishConnection()
        }
        else{
            if !isSocketAuthorized{
                authorizeConnection()
            }
            else{
                self.status = .Connected
            }
        }
    }
    
    override func disconnectToSocket(){
        socketEventConnection?.closeConnection()
    }
    
    override func getSocketConnectionState()-> SocketConnectionType {
        let connectionStatus = socketEventConnection?.socketAPI?.connectionStatus()
        if connectionStatus == .Connected{
            self.status = .Connected
        }
        else if connectionStatus == .Connecting{
            self.status = .Connecting
        }
        else if connectionStatus == .NotConnected{
            self.status = .NotConnected
        }
        else{
            self.status = .Disconnected
        }
        return self.status
    }
    
    //MARK:- Socket Event Connection Delegate Method(S)
    
    func didConnectSocket(socketEvent:YASocketEvent){
        YALog.print("Socket Connected")
        authorizeConnection()
    }
    
    func failedToConnectSocket(socketEvent:YASocketEvent){
        YALog.print("Failed To Connect To Socket")
        self.status = .Disconnected
        isSocketAuthorized = false
        socketEventAuthorization?.setAuthorizationStatus(value: isSocketAuthorized)
    }
    
    func didDisconnectSocket(socketEvent:YASocketEvent){
        YALog.print("Socket Disconnected")
        self.status = .Disconnected
        isSocketAuthorized = false
        socketEventAuthorization?.setAuthorizationStatus(value: isSocketAuthorized)
    }
    
    func socketEventFailed(socketEvent: YASocketEvent) {
        
    }
    
    func socketEventReceived(socketEvent: YASocketEvent) {
        
    }
    
    //MARK:- Socket Event Authorization Method(S)
    func didAuthorizeSuccessfully(socketEvent:YASocketEvent){
        YALog.print("Authorised successfully")
        self.status = .Connected
    }
    
    func failedToAuthorizeSocketConnection(socketEvent:YASocketEvent){
        YALog.print("Authorization Failed")
        self.status = .Disconnected
    }
}
