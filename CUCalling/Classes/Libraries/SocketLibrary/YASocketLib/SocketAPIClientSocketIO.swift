//
//  SocketAPIClientSocketIO.swift
//  YapAppCallingModule
//
//  Created by Abhimanu Jindal on 29/11/16.
//  Copyright © 2016 YapApp India Pvt. Ltd. All rights reserved.
//

import UIKit
import SocketIO

class SocketAPIClientSocketIO: YASocketAPI {
    
    //MARK:- Constants/Variable
    static let sharedInstance = SocketAPIClientSocketIO()
    
    var manager:SocketManager?
    private var socketClient:SocketIOClient?
    private var delegates:Array<DelegateModel> = []
    
    
    //MARK:- Private Method(S)
    private override init() {
        super.init()
        manager = SocketManager(socketURL: URL(string: (YAConfiguration.initConfigurationWithClient(client: AppConfigurations()).socketServerHost())!)!, config: socketIOConfiguration())
      //  manager?.handleQueue = DispatchQueue.global(qos: .userInteractive)
        socketClient = manager?.defaultSocket
        self.initializeMessageEventHandler()
        self.initializeErrorEventHandler()
        self.initializSocketDisconnectEventHandler()
    }
    
    //MARK:- Overridden Method(S)
    override func connect() {
        socketClient?.connect()
    }
    
    override func disconnect() {
        socketClient?.disconnect()
    }
    
    override func isConnected() -> Bool{
        if socketClient?.status == SocketIOStatus.connected{
            return true
        }
        return false
    }
    
    override func connectionStatus() -> YAConnectionStatus{
        if socketClient?.status == SocketIOStatus.connected{
            return YAConnectionStatus.Connected
        }
        if socketClient?.status == SocketIOStatus.connecting{
            return YAConnectionStatus.Connecting
        }
        if socketClient?.status == SocketIOStatus.notConnected{
            return YAConnectionStatus.NotConnected
        }
        return YAConnectionStatus.NotConnected
    }
    
    
    override func addEvent(eventName: String, delegate: YASocketAPIDelegate) {
        let delegateModel = DelegateModel.init(delegate: delegate, eventName: eventName)
        let index = self.delegates.index { (delegateModel) -> Bool in
            return (delegateModel.delegate === delegate && eventName == delegateModel.eventName)
        }
        if index == nil{
            self.delegates.append(delegateModel)
        }
        
    }
    
    override func removeEvent(eventName: String, delegate: YASocketAPIDelegate) {
        let index = self.delegates.index { (delegateModel) -> Bool in
            return (delegateModel.delegate === delegate && eventName == delegateModel.eventName)
        }
        if index != nil{
            self.delegates.remove(at: index!)
        }
    }
    
    override func sendEvent(event: String, data: [Any]?) {
        if socketClient?.status == SocketIOStatus.connected{
            socketClient?.emit(event, with: data!)
        }
    }
    
    //    override func sendEventWithAcknowledgement(event:String, data:[Any]?){
    //        let ack = socketClient?.emitWithAck(event, with: data!)
    //        ack?.timingOut(after: 1, callback: {(data:[Any]) in
    //            print(data)
    //        }
    //        )
    //    }
    
    
    
    //MARK:- Private Method(S)
    private func socketIOConfiguration() -> SocketIOClientConfiguration{
        
        return [.forceNew(true), .reconnects(true),.log(false)]
    }
    private func isReceivedMessageValid(data:[Any])->Bool{
        guard data.count>0 else{
            return false
        }
        var dictionary:[String:AnyObject]? = data[0] as? [String : AnyObject]
        guard (dictionary?["message"] != nil) else{
            return false
        }
        var messageDictionary:[String:AnyObject]? = dictionary?["message"] as? [String:AnyObject]
        let messageType:String? = messageDictionary?["type"] as? String
        guard (messageType != nil) else{
            return false
        }
        return true
    }
    
    private func getMessageResponse(data:[Any]) ->YASocketMessage{
        var dictionary:[String:AnyObject]? = data[0] as? [String : AnyObject]
        var messageDictionary:[String:AnyObject]? = dictionary?["message"] as? [String:AnyObject]
        let statusCode = messageDictionary?["statusCode"] as! NSNumber
        return YASocketMessage.init(type: messageDictionary?["type"] as? String, statusCode: statusCode.intValue, data: messageDictionary?["data"] , statusMessage: messageDictionary?["statusMessage"] as? String, errorType: messageDictionary?["errorType"] as? String)
    }
    
    
    private func initializeMessageEventHandler(){
        socketClient?.on("message", callback:{(data:[Any],socketackEmitter:SocketAckEmitter) in
            guard self.isReceivedMessageValid(data: data) else{
                return
            }
            let socketMessage = self.getMessageResponse(data: data)
            let filteredDelegates = self.delegates.filter( { return $0.eventName == socketMessage.type } )
            self.message = socketMessage
            for (_, item) in filteredDelegates.enumerated() {
                item.delegate.didReceiveEvent(socketAPI: self)
            }
        }
        )
        socketClient?.on("message_failure", callback:{(data:[Any],socketackEmitter:SocketAckEmitter) in
            guard self.isReceivedMessageValid(data: data) else{
                return
            }
            let socketMessage = self.getMessageResponse(data: data)
            let filteredDelegates = self.delegates.filter( { return $0.eventName == socketMessage.type } )
            self.message = socketMessage
            for (_, item) in filteredDelegates.enumerated() {
                item.delegate.didReceiveEventFailure(socketAPI: self)
            }
        }
        )
    }
    
    private func initializeErrorEventHandler(){
        socketClient?.on("error", callback:{(data:[Any],socketackEmitter:SocketAckEmitter) in
            let filteredDelegates = self.delegates.filter( { return $0.eventName == "connectionError" } )
            self.message = YASocketMessage.init(type: "connectionError", statusCode: 404 , data: nil, statusMessage: nil,errorType: nil)
            for (_, item) in filteredDelegates.enumerated() {
                item.delegate.didReceiveEvent(socketAPI: self)
            }
        }
        )
    }
    
    private func initializSocketDisconnectEventHandler(){
        socketClient?.on("disconnect", callback:{(data:[Any],socketackEmitter:SocketAckEmitter) in
            let filteredDelegates = self.delegates.filter( { return $0.eventName == "disconnect" } )
            self.message = YASocketMessage.init(type: "disconnect", statusCode: 200 , data: nil, statusMessage: nil,errorType: nil)
            for (_, item) in filteredDelegates.enumerated() {
                item.delegate.didReceiveEvent(socketAPI: self)
            }
        }
        )
    }
    
}
