//
//  SocketEventAPIConnectionClient.swift
//  YapAppCallingModule
//
//  Created by Abhimanu Jindal on 30/11/16.
//  Copyright © 2016 YapApp India Pvt. Ltd. All rights reserved.
//

import UIKit

class SocketEventConnectionClient: YASocketEventConnection, YASocketAPIDelegate {
    
    //MARK:- Public Method(S)
    override func establishConnection() {
        self.socketAPI?.connect()
    }
    
    override func closeConnection() {
        self.socketAPI?.disconnect()
    }
    
    override func handleSocketEvents(){
        self.socketAPI?.addEvent(eventName: "connection", delegate: self)
        self.socketAPI?.addEvent(eventName: "connectionError", delegate: self)
        self.socketAPI?.addEvent(eventName: "disconnect", delegate: self)
    }
    
    override func removeSocketEvents() {
        self.socketAPI?.removeEvent(eventName: "connection", delegate: self)
        self.socketAPI?.removeEvent(eventName: "connectionError", delegate: self)
        self.socketAPI?.removeEvent(eventName: "disconnect", delegate: self)
    }
    
    
    //MARK:- YASocketAPI Delegate Method(s)
    func didReceiveEvent(socketAPI: YASocketAPI) {
        
        let delegate = self.delegate as! YASocketEventConnectionDelegate
        
        if socketAPI.message?.type == "connection"{
            delegate.didConnectSocket(socketEvent: self)
        }
        if socketAPI.message?.type == "connectionError"{
            delegate.failedToConnectSocket(socketEvent: self)
        }
        if socketAPI.message?.type == "disconnect"{
            delegate.didDisconnectSocket(socketEvent: self)
        }
    }
    
    func didReceiveEventFailure(socketAPI: YASocketAPI) {
        
    }
}
