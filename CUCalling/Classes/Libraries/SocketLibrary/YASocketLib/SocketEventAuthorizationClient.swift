//
//  SocketAuthorizationEvent.swift
//  YapAppCallingModule
//
//  Created by Abhimanu Jindal on 30/11/16.
//  Copyright © 2016 YapApp India Pvt. Ltd. All rights reserved.
//

import UIKit

class SocketEventAuthorizationClient: YASocketEventAuthorization, YASocketAPIDelegate {
    
    //MARK:- Public Method(S)
    override func authorizeConnection(accessToken:String) {
        var versionNumber = ""
        if Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString") as? String != nil {
         versionNumber = Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString") as! String
        }
        self.socketAPI?.sendEvent(event: "login", data: [["auth_token":accessToken,"appVersion":versionNumber]])
    }
    
    override func authorizationStatus()->Bool {
        return (self.socketAPI?.isSocketAuthorized)!
    }
    
    override func setAuthorizationStatus(value:Bool) {
        self.socketAPI?.isSocketAuthorized = value
    }
    
    override func handleSocketEvents(){
        self.socketAPI?.addEvent(eventName: "login", delegate: self)
    }
    
    override func removeSocketEvents() {
        self.socketAPI?.removeEvent(eventName: "login", delegate: self)
    }
    
    //MARK:- YASocketAPI Delegate Method(s)
    func didReceiveEvent(socketAPI: YASocketAPI) {
        
        let delegate = self.delegate as! YASocketEventAuthorizationDelegate
        
        if socketAPI.message?.type == "login"{
            if socketAPI.message?.statusCode == 200 {
                self.socketAPI?.isSocketAuthorized = true
                delegate.didAuthorizeSuccessfully(socketEvent: self)
                return
            }
            self.socketAPI?.isSocketAuthorized = false
            delegate.failedToAuthorizeSocketConnection(socketEvent: self)
        }
    }
    
    func didReceiveEventFailure(socketAPI: YASocketAPI) {
        let delegate = self.delegate as! YASocketEventAuthorizationDelegate
        
        if socketAPI.message?.type == "login"{
            self.socketAPI?.isSocketAuthorized = false
            delegate.failedToAuthorizeSocketConnection(socketEvent: self)
        }
    }
}
