//
//  AudioManager.swift
//  Nehao
//
//  Created by Abhimanu Jindal on 08/03/17.
//  Copyright © 2017 Amit Tripathi. All rights reserved.
//

import Foundation
import AudioToolbox
import JSQSystemSoundPlayer
import MediaPlayer

struct AudioPortDescription{
    var portName : String
    var portType : String
    var uid : String
}
protocol AudioManagerDelegate:class {
    func audioPlayedSuccessfully(soundFileName:String)
    func outputSpeakerDidChange(speakerType:AudioManager.SpeakerType, routeChangeReason : AudioManager.RouteChangeReason)
    func outputSpeakerFailedToChange(speakerType:AudioManager.SpeakerType)
    func availableAudioOutputDidChange(isAdded: Bool, type: String)
    func didUpdateSpeakerType(speakerType: CurrentSelectedAudioOutput)
    func avaiableAudioOutputPortChanged()
}

struct AudioManagerModel {
    var fileName:String?
    var fileExtension:AudioManager.SoundExtensionType?
    var shouldRepeat:Bool?
    var shouldVibrate:Bool?
    var speakerType:AudioManager.SpeakerType?
}



class AudioManager:NSObject{
    
    //MARK:- Variables/ Constants
    
    enum SoundExtensionType {
        case CAF
        case AIF
        case AIFF
        case WAV
        case MP3
        case OTHER
    }
    
    enum SpeakerType {
        case PhoneInEarSpeaker
        case PhoneLoudSpeaker
        case BluetoothSpeaker
        case EarPhones
        case CarAudio
        case USBAudio
        case None
        case Default

    }
    
    enum RouteChangeReason {
        case HeadphoneDetached
        case Others
    }
    
    weak var delegate:AudioManagerDelegate?
    private var systemAudioPlayer:JSQSystemSoundPlayer?
    private var appAudioPlayer:AVAudioPlayer?
    
    private var ringtoneVibrateTimer:Timer?
    
    private var isAppAudioPlayingValue:Bool = false
    private var isSystemAudioPlayingValue:Bool = false
    private var currentCallAudioModel = AudioManagerModel()
    
    private var availableOutputs : [AudioPortDescription] = []
    private var isInitiatedByuser = false
    
    private var sharedAudioSession : AVAudioSession = AVAudioSession.sharedInstance()
    
    //MARK:- Overridden Method(s)
    
    override init(){
        super.init()
        self.setUpSystemAudioPlayer()
        self.setUpAudioSessionNotification()
        setupAudioSessionCategory(category: AVAudioSessionCategoryAmbient, withOptions: nil, sessionMode: AVAudioSessionModeVoiceChat, shouldUpdatePreferredInput: false)
        setAudioSessionActive(isActive: true)
        appendDefaultOutputPorts()
        setupMPVolumeViewNotification()
    }
    
    //MARK:- Setup Audio Player Method(s)

    func setUpSystemAudioPlayer(){
        systemAudioPlayer = JSQSystemSoundPlayer.shared()
    }
    
    func setupAudioSessionCategory(category : String, withOptions options:AVAudioSessionCategoryOptions?, sessionMode mode:String?, shouldUpdatePreferredInput updatePreferredInput: Bool)
    {
        do{
            if options != nil{
                try AVAudioSession.sharedInstance().setCategory(category, with: options!)
            }
            else
            {
                try AVAudioSession.sharedInstance().setCategory(category)
            }
            
            if mode != nil{
                try AVAudioSession.sharedInstance().setMode(mode!)
            }
            
            
        }catch{
            YALog.print("Unable to set audio session category")
        }
        setAudioSessionActive(isActive: true)
        
        if updatePreferredInput{
            let session = AVAudioSession.sharedInstance()
            do {
                // It's important to set preferred input *after* ensuring properAudioSession
                // because some sources are only valid for certain category/option combinations.
                let existingPreferredInput = session.preferredInput
                try session.setPreferredInput(existingPreferredInput)
                try session.overrideOutputAudioPort(.none)
            } catch {
            }

        }

    }
    
    /*
    func setUpAudioSessionDefaultCategory(){
        do{
            try sharedAudioSession.setCategory(AVAudioSessionCategoryAmbient)
            try sharedAudioSession.setMode(AVAudioSessionModeVoiceChat)

        }catch{
            YALog.print("Unable to set audio session category")
        }
        setAudioSessionActive(isActive: true)
    }
    
    func setUpAudioSessionPlayAndRecordCategory(){
        do{
            try sharedAudioSession.setCategory(AVAudioSessionCategoryPlayAndRecord, with: [.allowBluetooth,.allowBluetoothA2DP])
            try sharedAudioSession.setMode(AVAudioSessionModeVoiceChat)
            
        }catch{
            YALog.print("Unable to set audio session category")
        }
        setAudioSessionActive(isActive: true)
    }
    
    func setUpAudioSessionPlaybackCategory(){
        do{
            try sharedAudioSession.setCategory(AVAudioSessionCategoryPlayback, with: [.duckOthers])
            try sharedAudioSession.setMode(AVAudioSessionModeVoiceChat)

        }catch{
            YALog.print("Unable to set audio session category")
        }
        setAudioSessionActive(isActive: true)
    }
    
    func setUpAudioSessionMultiRouteCategory(){
        do{
            try sharedAudioSession.setCategory(AVAudioSessionCategoryMultiRoute, with: [.duckOthers])
        }catch{
            YALog.print("Unable to set audio session category")
        }
        setAudioSessionActive(isActive: true)

    }
    
    func setUpAudioSessionSoloAmbientCategory(){
        do{
            try sharedAudioSession.setCategory(AVAudioSessionCategorySoloAmbient)
            
        }catch{
            YALog.print("Unable to set audio session category")
        }
        setAudioSessionActive(isActive: true)
        let session = AVAudioSession.sharedInstance()
        do {
            // It's important to set preferred input *after* ensuring properAudioSession
            // because some sources are only valid for certain category/option combinations.
            let existingPreferredInput = session.preferredInput
                try session.setPreferredInput(existingPreferredInput)
                try session.overrideOutputAudioPort(.none)
        } catch {
        }

    }
 */

    
    private func setAudioSessionActive(isActive : Bool){
        do {
            try AVAudioSession.sharedInstance().setActive(isActive)
        } catch {
            YALog.print("Unable to make audio session active")
        }
    }
    
    
    //MARK:- Setup Notificaions
    private func setupMPVolumeViewNotification()
    {
        let nc = NotificationCenter.default
        nc.addObserver(self, selector:
        #selector(self.volumeViewWirelessRouteChanged), name: NSNotification.Name.MPVolumeViewWirelessRouteActiveDidChange, object: nil)
    }
    
    private func setUpAudioSessionNotification(){
        let nc = NotificationCenter.default
        nc.addObserver(self, selector:
            #selector(self.routeChanged), name: NSNotification.Name.AVAudioSessionRouteChange, object: nil)
        nc.addObserver(self, selector:
            #selector(self.interruptionOccurred(userInfo:)), name: NSNotification.Name.AVAudioSessionInterruption, object: nil)
    }
    
    //MARK: - Notification Methods
    @objc func volumeViewWirelessRouteChanged(notification: Notification)
    {
        isInitiatedByuser = true
    }
    
    @objc func routeChanged(userInfo:Notification){
         /** Extra Audio Code
       /*
        let routeChangeDict = userInfo.userInfo
        
        let routeChangeReasonNumber = routeChangeDict?[AVAudioSessionRouteChangeReasonKey] as! NSNumber
        let routeChangeReason:AVAudioSessionRouteChangeReason = AVAudioSessionRouteChangeReason(rawValue: routeChangeReasonNumber.uintValue)!
        if (routeChangeDict?[AVAudioSessionRouteChangePreviousRouteKey] as? AVAudioSessionRouteDescription) != nil
        {
            let previousRouteKey = routeChangeDict?[AVAudioSessionRouteChangePreviousRouteKey] as! AVAudioSessionRouteDescription
        }
        
        
        if routeChangeReason == AVAudioSessionRouteChangeReason.unknown{
            YALog.print("Audio Session Changed with unknown reason - \(userInfo)")
        }
            
        else if routeChangeReason == AVAudioSessionRouteChangeReason.newDeviceAvailable{
            YALog.print("Audio Session Changed with new Device available - \(userInfo)")
            
//            if (routeChangeDict?[AVAudioSessionRouteChangePreviousRouteKey] as? AVAudioSessionRouteDescription) != nil
//            {
//                let portDesc = sharedAudioSession.currentRoute.outputs[0]
//                let portType = portDesc.portType
//                
//                if isNewBTAttachedOrDetached(forRoute: sharedAudioSession.currentRoute){
//                    self.delegate?.availableAudioOutputDidChange(isAdded: true, type: portType)
//                }
//            }
          //  setAndUpdateCurrentOutputRoute()
            
        }
            
        else if routeChangeReason == AVAudioSessionRouteChangeReason.oldDeviceUnavailable{
            YALog.print("Audio Session Changed with old Device unavailable - \(userInfo)")
            
//            if (routeChangeDict?[AVAudioSessionRouteChangePreviousRouteKey] as? AVAudioSessionRouteDescription) != nil
//            {
//                let previousRoute = routeChangeDict?[AVAudioSessionRouteChangePreviousRouteKey] as! AVAudioSessionRouteDescription
//                let portDesc = previousRoute.outputs[0]
//                let portType = portDesc.portType
//                if isNewBTAttachedOrDetached(forRoute: previousRoute){
//                    self.delegate?.availableAudioOutputDidChange(isAdded: false, type: portType)
//                }
//            }
           // setAndUpdateCurrentOutputRoute()
            
        }
            
        else if routeChangeReason == AVAudioSessionRouteChangeReason.categoryChange{
            YALog.print("Audio Session Changed with categoryChange Previous Info - \(userInfo)")
            // setAndUpdateCurrentOutputRoute()
        }
            
        else if routeChangeReason == AVAudioSessionRouteChangeReason.override{
            YALog.print("Audio Session Changed with override - \(userInfo)")
            setAndUpdateCurrentOutputRoute()
        }
            
        else if routeChangeReason == AVAudioSessionRouteChangeReason.wakeFromSleep{
            YALog.print("Audio Session Changed with wakeFromSleep - \(userInfo)")
            setAndUpdateCurrentOutputRoute()
        }
            
        else if routeChangeReason == AVAudioSessionRouteChangeReason.noSuitableRouteForCategory{
            YALog.print("Audio Session Changed with noSuitableRouteForCategory - \(userInfo)")
        }
            
        else if routeChangeReason == AVAudioSessionRouteChangeReason.routeConfigurationChange{
            YALog.print("Audio Session Changed with routeConfigurationChange - \(userInfo)")
            setAndUpdateCurrentOutputRoute()
            return
        }
        else{
            YALog.print("Audio Session Changed with Exception Details - \(userInfo)")
        }
 */
 */
       
        self.updateAvailableOutputsArray()
    }
    
    @objc func interruptionOccurred(userInfo:NSNotification){
        YALog.print("Interruption Occurred :\(userInfo)")
    }
    
    private func updateAvailableOutputsArray()
    {
        if AVAudioSession.sharedInstance().availableInputs != nil{
            _ = availableOutputs
            availableOutputs.removeAll()
            appendDefaultOutputPorts()
            
            for port in AVAudioSession.sharedInstance().availableInputs!
            {
                if port.portType != AVAudioSessionPortBuiltInMic
                {
                    let filteredOutput =  availableOutputs.filter{$0.portName == port.portName}
                    
                    if filteredOutput.count == 0
                    {
                        let newPort = AudioPortDescription(portName: port.portName, portType: port.portType, uid: port.uid)
                        availableOutputs.append(newPort)
                    }
                    
                    if port.portType == AVAudioSessionPortHeadsetMic
                    {
                        let filteredOutputPhone =  availableOutputs.filter{$0.portType != "iPhone"}
                        availableOutputs = filteredOutputPhone
                    }
                }
            }
            
            self.delegate?.avaiableAudioOutputPortChanged()
 /** Extra Audio Code
            /*
            let prevArrayBTAvailable = prevOutputArray.filter{$0.portType == AVAudioSessionPortBluetoothHFP || $0.portType == AVAudioSessionPortBluetoothLE || $0.portType == AVAudioSessionPortBluetoothA2DP}
            
            let filteredOutputBluetoothAdded =  availableOutputs.filter{$0.portType == AVAudioSessionPortBluetoothHFP || $0.portType == AVAudioSessionPortBluetoothLE || $0.portType == AVAudioSessionPortBluetoothA2DP}
            if filteredOutputBluetoothAdded.count > 0
            {
                if prevArrayBTAvailable.count == 0
                {
                    self.delegate?.availableAudioOutputDidChange(isAdded: true, type: AVAudioSessionPortBluetoothA2DP)
                }
            }
                
            else{
                if prevArrayBTAvailable.count > 0
                {
                    self.delegate?.availableAudioOutputDidChange(isAdded: false, type: AVAudioSessionPortBluetoothA2DP)
                }
            }
 */
            */
        }

    }
    
    private func isNewBTAttachedOrDetached(forRoute route: AVAudioSessionRouteDescription) -> Bool
    {
        let portDesc = route.outputs[0]
        let portType = portDesc.portType
        
        if isPortBluetooth(portType: portType){
        return true
        }
        return false
    }
    
       //MARK:- Play Pause Methods(s)
    
    
    func playAppAudio(fileName:String, fileExtension:SoundExtensionType, shouldRepeat:Bool, shouldVibrate:Bool, shouldEnableMicPermission:Bool){
        
        
        if shouldEnableMicPermission{
            if AVAudioSession.sharedInstance().recordPermission() == AVAudioSessionRecordPermission.undetermined{
                AVAudioSession.sharedInstance().requestRecordPermission { (isGranted) in
                    YALog.print("Request Granted \(isGranted)")
                }
            }
        }
        
        currentCallAudioModel.fileName = "Vibrate"
        currentCallAudioModel.fileExtension = .OTHER
        currentCallAudioModel.shouldRepeat = false
        currentCallAudioModel.shouldVibrate = false
        
        isAppAudioPlayingValue = true
        
        appAudioPlayer?.stop()
        let fileExtensionString = mapFileExtensionToAVAudioPlayer(fileExtensionType: fileExtension)
        do{
            try appAudioPlayer = AVAudioPlayer.init(contentsOf: URL.init(fileURLWithPath: Bundle.main.path(forResource: fileName, ofType: fileExtensionString)!))
        }catch{
            YALog.print("Unable to create audio player")
        }
        if shouldRepeat{
            appAudioPlayer?.numberOfLoops = -1
        }
        else{
            appAudioPlayer?.numberOfLoops = 0
        }
        if shouldVibrate{
            startRingtoneVibrationPattern(numberOFVibrations: 1, repeatTimeLength: 3)
        }
        else{
            stopVibration()
        }
        
        appAudioPlayer?.prepareToPlay()
        appAudioPlayer?.play()
        
    }
    
    func stopSystemSound(){
        currentCallAudioModel.fileName = "SystemSound"
        currentCallAudioModel.fileExtension = .OTHER
        currentCallAudioModel.shouldRepeat = false
        currentCallAudioModel.shouldVibrate = false
        systemAudioPlayer?.stopAllSounds()
    }
    
    func stopVibration(){
        currentCallAudioModel.fileName = "Vibrate"
        currentCallAudioModel.fileExtension = .OTHER
        currentCallAudioModel.shouldRepeat = false
        currentCallAudioModel.shouldVibrate = false
        if ringtoneVibrateTimer != nil{
            ringtoneVibrateTimer?.invalidate()
        }
    }
    
    func stopAppSound(){
        appAudioPlayer?.stop()
    }
    
    func stopAllSounds(){
        stopSystemSound()
        stopAppSound()
        stopVibration()
    }
    
    
    //MARK:- Handle Avialable Outputs
    func checkIfHeadphonesConnected() -> Bool{
        for portDescription in availableOutputs
        {
            if portDescription.portType == AVAudioSessionPortHeadphones || portDescription.portType == AVAudioSessionPortBuiltInMic || portDescription.portType == AVAudioSessionPortHeadsetMic
            {
                return true
            }
        }
        return false
    }
    
    func checkIfBluetoothConnected() -> Bool{
        for portDescription in availableOutputs
        {
            if isPortBluetooth(portType: portDescription.portType)
            {
                return true
            }
        }
        return false
    }
    
    private func isPortBluetooth(portType: String)-> Bool{
        if portType == AVAudioSessionPortBluetoothHFP || portType == AVAudioSessionPortBluetoothLE || portType == AVAudioSessionPortBluetoothA2DP
        {
            return true
        }
        return false
    }
    
    func getAvailableOutputTypes() -> [AudioPortDescription]
    {
        return availableOutputs
    }

    func getCurrentlySelectedOutput() -> SpeakerType?
    {
        return currentCallAudioModel.speakerType
    }
    
    func setAndUpdateCurrentOutputRoute()
    {
        for port in AVAudioSession.sharedInstance().currentRoute.outputs
        {
            let currentSetSpeakerType = mapPortTypeToSpeakerType(portType: (port.portType))
            if currentSetSpeakerType != currentCallAudioModel.speakerType{
                currentCallAudioModel.speakerType = currentSetSpeakerType
                switchOutputSpeaker(speakerType: currentCallAudioModel.speakerType!)
            }
        }
    }

    
    //MARK:- Switch Speaker Methods(s)
    
    func switchOutputSpeaker(speakerType:AudioManager.SpeakerType, uid : String = "", isUserSelected: Bool = false){
        
        if isInitiatedByuser || isUserSelected
        {
            isInitiatedByuser = false
            updateCurrentlySelctedOutput(speakerType: speakerType)
        }
        currentCallAudioModel.speakerType = speakerType
        if !isUserSelected{
            if checkIfBluetoothConnected() || checkIfHeadphonesConnected()
            {
                setAndUpdateCurrentOutputRoute()
                return
            }
        }
        
        if !isOutPutSpeakerAvailable(speakerType:speakerType){
            delegate?.outputSpeakerFailedToChange(speakerType: speakerType)
            return
        }

        switch speakerType {
        case .PhoneInEarSpeaker:
            switchToPhoneInEarSpeaker()
        case .PhoneLoudSpeaker:
            switchToPhoneLoudSpeaker()
        case .EarPhones:
            switchOutputToHeadphone()
        case .BluetoothSpeaker,.CarAudio,.USBAudio:
            switchToBluetooth(withUid: uid)
        case .None:
            switchToPhoneInEarSpeaker()
        case .Default:
            setAndUpdateCurrentOutputRoute()
        default:
            switchToPhoneLoudSpeaker()
        }
    }
    
    private func updateCurrentlySelctedOutput(speakerType: AudioManager.SpeakerType)
    {
        switch speakerType {
        case .PhoneInEarSpeaker:
            self.delegate?.didUpdateSpeakerType(speakerType: .InEarPhone)
        case .PhoneLoudSpeaker:
            self.delegate?.didUpdateSpeakerType(speakerType: .SpeakerOn)
        case .EarPhones:
            self.delegate?.didUpdateSpeakerType(speakerType: .Headphones)
            setAndUpdateCurrentOutputRoute()
        case .BluetoothSpeaker,.CarAudio,.USBAudio:
            self.delegate?.didUpdateSpeakerType(speakerType: .Bluetooth)
        case .None:
            self.delegate?.didUpdateSpeakerType(speakerType: .InEarPhone)
        case .Default:
            self.delegate?.didUpdateSpeakerType(speakerType: .InEarPhone)
        default:
            self.delegate?.didUpdateSpeakerType(speakerType: .SpeakerOn)
        }
    }

    
    //MARK:- Sound State Methods(s)
//    
//    func isAudioPlaying()->Bool{
//        return (isSystemAudioPlayingValue && isAppAudioPlayingValue)
//    }
//    
//    func isSystemAudioPlaying()->Bool{
//        return isSystemAudioPlayingValue
//    }
//    
//    func isAppAudioPlaying()->Bool{
//        return isAppAudioPlayingValue
//    }
//    
//    func getSystemSoundVolume()->Float?{
//        return appAudioPlayer?.volume
//    }
//    
//    func getAppSoundVolume()->Float?{
//        return appAudioPlayer?.volume
//    }
//    
    func setAppSoundVolume(volume:Float){
        appAudioPlayer?.volume = volume
    }
    
    
    //MARK: ...Mapping Method(s)
//    private func mapFileExtensionToJSQSoundManager(fileExtensionType:SoundExtensionType)->String{
//        switch fileExtensionType {
//        case .AIF:
//            return kJSQSystemSoundTypeAIF
//        case .CAF:
//            return kJSQSystemSoundTypeCAF
//        case .AIFF:
//            return kJSQSystemSoundTypeAIFF
//        case .WAV:
//            return kJSQSystemSoundTypeWAV
//        default:
//            return ""
//        }
//    }
    
    private func mapFileExtensionToAVAudioPlayer(fileExtensionType:SoundExtensionType)->String{
        switch fileExtensionType {
        case .AIF:
            return "aif"
        case .CAF:
            return "caf"
        case .AIFF:
            return "aiff"
        case .WAV:
            return "wav"
        case .MP3:
            return "mp3"
        default:
            return ""
        }
    }
    
    private func mapPortTypeToSpeakerType(portType:String)->AudioManager.SpeakerType{
        switch portType{
        case AVAudioSessionPortBuiltInReceiver:
            return .PhoneInEarSpeaker
        case AVAudioSessionPortBuiltInSpeaker:
            return .PhoneLoudSpeaker
        case AVAudioSessionPortHeadphones:
            return .EarPhones
        case AVAudioSessionPortBluetoothA2DP,AVAudioSessionPortBluetoothLE,AVAudioSessionPortBluetoothHFP:
            return .BluetoothSpeaker
        case AVAudioSessionPortCarAudio:
            return .CarAudio
        case AVAudioSessionPortUSBAudio:
            return .USBAudio
        default:
            return .None
        }
    }
    
    private func mapSpeakerTypeToPortType(speakerType:AudioManager.SpeakerType)->String{
        switch speakerType{
        case .PhoneInEarSpeaker:
            return AVAudioSessionPortBuiltInReceiver
        case .PhoneLoudSpeaker :
            return AVAudioSessionPortBuiltInSpeaker
        case .EarPhones :
            return AVAudioSessionPortHeadphones
        case .BluetoothSpeaker:
            return AVAudioSessionPortBluetoothA2DP
        case .CarAudio :
            return AVAudioSessionPortCarAudio
        case .USBAudio :
            return AVAudioSessionPortUSBAudio
        default:
            return AVAudioSessionPortBuiltInSpeaker
        }
    }
    
    
    //MARK:- Play System Sound Method(s)
    private func startRingtoneVibrationPattern(numberOFVibrations:Int, repeatTimeLength:Int){
        stopVibration()
        self.playSystemVibrateSound()
        ringtoneVibrateTimer = Timer.scheduledTimer(withTimeInterval: TimeInterval(repeatTimeLength), repeats: true) { (timer) in
            for _ in 0...numberOFVibrations {
                self.systemAudioPlayer?.playVibrateSound()
            }
        }
    }
    
    func playSystemVibrateSound(){
        currentCallAudioModel.fileName = "Vibrate"
        currentCallAudioModel.fileExtension = .OTHER
        currentCallAudioModel.shouldRepeat = false
        currentCallAudioModel.shouldVibrate = false
        systemAudioPlayer?.playVibrateSound()
    }

    
    //MARK:- Handling Input and Output ports
    
    private func overrideOutputPort(port: AVAudioSessionPortOverride)
    {
        do{
            try AVAudioSession.sharedInstance().overrideOutputAudioPort(port)
        }catch{
            YALog.print("Unable to override")
        }
        
    }
    
    private func setPreferredInputPort(port: AVAudioSessionPortDescription)
    {
        do{
            try AVAudioSession.sharedInstance().setPreferredInput(port)
        }catch{
            YALog.print("Unable to override")
        }
        
    }
    
     //MARK : Switching Audio Output Ports
    
    private func switchToPhoneInEarSpeaker() {

        self.overrideOutputPort(port: .none)
        delegate?.outputSpeakerDidChange(speakerType: .EarPhones, routeChangeReason: .Others)
    }
    
    private func switchOutputToNone(){
        self.overrideOutputPort(port: .none)
        delegate?.outputSpeakerDidChange(speakerType: .EarPhones, routeChangeReason : .Others)
    }
    
    private func switchOutputToHeadphone()
    {
        for route in AVAudioSession.sharedInstance().availableInputs! {
            if route.portType == AVAudioSessionPortHeadsetMic{
                let portDescription = route
                self.overrideOutputPort(port: .none)
                self.setPreferredInputPort(port: portDescription)
                delegate?.outputSpeakerDidChange(speakerType: .EarPhones, routeChangeReason : .Others)
            }
        }
    }
    
    private func switchToBluetooth(withUid uid: String){
        for route in AVAudioSession.sharedInstance().availableInputs! {
            if route.uid == uid
            {
                let portDescription = route
                setAudioSessionActive(isActive: false)
                self.overrideOutputPort(port: .none)
                self.setPreferredInputPort(port: portDescription)
                delegate?.outputSpeakerDidChange(speakerType: .BluetoothSpeaker, routeChangeReason : .Others)
                setAudioSessionActive(isActive: true)
            }
        }
    }
    
    private func switchToPhoneLoudSpeaker(){
//        self.overrideOutputPort(port: .none)
        self.overrideOutputPort(port: .speaker)
        delegate?.outputSpeakerDidChange(speakerType: .PhoneLoudSpeaker, routeChangeReason : .Others)
    }
    
    
    //MARK:.... Audio Session Method(s)
    
    private func isOutPutSpeakerAvailable(speakerType:AudioManager.SpeakerType)->Bool{
        
        //NEED TO find the proper solution to find the in ear speaker phones
        if UIDevice.current.model.hasPrefix("iPod") && ((speakerType == .PhoneInEarSpeaker) || (speakerType == .None)){
            return false
        }
        
        if UIDevice.current.model.hasPrefix("iPad") && ((speakerType == .PhoneInEarSpeaker) || (speakerType == .None)){
            return false
        }
            
//        else if UIDevice.current.model.hasPrefix("iPhone") && (speakerType == .PhoneInEarSpeaker){
//            return true
//        }
        return true
    }
    
    func appendDefaultOutputPorts()
    {
        let speakerPort : AudioPortDescription = AudioPortDescription(portName: "Speaker", portType: "Speaker", uid: "Speaker")
        availableOutputs.append(speakerPort)
        if !(UIDevice.current.model.hasPrefix("iPod") || UIDevice.current.model.hasPrefix("iPad") ){
            let inEarPort : AudioPortDescription = AudioPortDescription(portName: "iPhone", portType: "iPhone", uid: "iPhone")
            availableOutputs.append(inEarPort)
            
        }
    }
    
    
}
