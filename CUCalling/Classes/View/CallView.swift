//
//  CallView.swift
//  CUCalling
//
//  Created by yapapp on 3/20/18.
//  Copyright © 2018 yapapp.com. All rights reserved.
//

import UIKit
import MediaPlayer

protocol OngoingCallViewDelegate:class {
    func endCallButtonTapped()
    func ongoingChatButtonTapped()
    func ongoingMicRecordButtonTapped(isMicEnabled:Bool)
    func ongoingSpeakerButtonTapped(isSpeakerEnabled:Bool)
    func ongoingAudioVideoButtonTapped(isVideoEnabled:Bool)
    func ongoingCameraButtonTapped()
    func ongoingswapVideosButtontapped(_ swapValue:Bool)
    func ongoingCall(_ view: CallView, chekcIfNetworkIsPoor completionBlock:@escaping(Bool)->Void)
}

class CallView: UIView {

    @IBOutlet weak var messageBadgeLabel: UILabel!
    @IBOutlet weak var chatBadgeView: UIView!
    @IBOutlet var remoteUserView:UIView?
    @IBOutlet var localUserView:UIView?
    @IBOutlet var cameraSwitchButton:UIButton?
    @IBOutlet weak var videoButton: UIButton!
    @IBOutlet weak var micButton: UIButton!
    @IBOutlet weak var speakerButton: UIButton!
    @IBOutlet weak var chatButton: UIButton!
    @IBOutlet weak var endCallButton: UIButton!
    @IBOutlet weak var callActionViewBottomConstraint: NSLayoutConstraint!
    
//    @IBOutlet weak var videoCallerImage: GIFImageView!
     @IBOutlet weak var videoCallerImage: UIImageView!
    @IBOutlet weak var videoCallStatusLabel: UILabel!
    @IBOutlet weak var videoCallContactLabel: UILabel!
    @IBOutlet weak var videoCallDurationLabel: UILabel!
    @IBOutlet weak var videoViewCallerDetailView: UIView!
    @IBOutlet weak var bottomControlViewHeightConstarint: NSLayoutConstraint!
    @IBOutlet weak var localCameraSwitchButton: UIButton!
    @IBOutlet weak var swapVideosButton: UIButton!
    //MARK::- Variables/Constants
    private var videoEnableValue:Bool = true
    private var micEnableValue:Bool = true
    private var speakerEnableValue:Bool = true
    private  var volumeView = MPVolumeView()
    var remoteVideoSize : CGSize = CGSize(width: 0, height: 0)
    @IBOutlet weak var navigationView: UIView!
    private var audioAnimationGif: String = "audio_animation"
    
    weak var delegate:OngoingCallViewDelegate?
    
    
    
    
    var isSelfViewLarge : Bool = false
    var isRemoteVideo:Bool = false
    var calleeAppState : appState = .Foreground
    var containerView : UIView?
    var smallViewSize : CGRect = CGRect()
    var largeViewSize : CGRect = CGRect()
    var lastSelecetdViewPosition : ViewPostion = .BottomRight
    var topYPoint:CGFloat = 0.0
    
    enum ViewPostion {
        case TopLeft
        case TopRight
        case BottomLeft
        case BottomRight
    }
    
    
    enum appState{
        case Background
        case Foreground
    }
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
    //MARK:- IBACtion Method(s)
    @IBAction func cameraSwitchButtonTapped(_ sender:UIButton){
        self.delegate?.ongoingCameraButtonTapped()
    }
    @IBAction func endCallButtonTapped(sender:UIButton){
        self.delegate?.endCallButtonTapped()
    }
    @IBAction func chatMessageButtonTapped(_ sender: UIButton) {
        //        YALog.print(isVideoEnabled)
        handleMessageBadgeLabel(isVisible: false,withCount: 0)
        self.delegate?.ongoingChatButtonTapped()
    }
    
    @IBAction func micRecordButtonTapped(_ sender: UIButton) {
        micEnableValue = !micEnableValue
        setUpControlButtons()
        self.delegate?.ongoingMicRecordButtonTapped(isMicEnabled: micEnableValue)
    }
    
    @IBAction func speakerButtonTapped(_ sender: UIButton) {
        speakerEnableValue = !speakerEnableValue
        setUpControlButtons()
        self.delegate?.ongoingSpeakerButtonTapped(isSpeakerEnabled: speakerEnableValue)
    }
    
    @IBAction func videoButtonTapped(_ sender: UIButton) {
        
        checkAccessForMedia(isVideoCall: true) { (isGranted, accessType) in
            if isGranted
            {
                if !self.videoEnableValue
                {
                    //check network range
                    self.delegate?.ongoingCall(self, chekcIfNetworkIsPoor: { (isPoor) in
                        if isPoor
                        {
                            //                                AlertCentral.displayOkAlertOnWindow(withTitle: "CU", message: "You cannot switch to video call due to slow network.")
                            return
                        }
                        else
                        {
                            self.videoTapOperation()
                        }
                    })
                }
                else{
                    self.videoTapOperation()
                }
                
            }
            else
            {
                //                    Utilities.showNoAccessAlert(isVideoCall: true, accessType: accessType, isIncomingCall: true)
            }
        }
    }
    @IBAction func localCameraSwitchButtonTapped(_ sender: UIButton) {
        self.delegate?.ongoingCameraButtonTapped()
    }
    
}

extension CallView {
    public func displayVideoScreen(){
        self.videoViewCallerDetailView.isHidden = false
        
    }
    
    public func displayAudioScreen(onView parentView: UIView){
        self.videoViewCallerDetailView.isHidden = true
    }
    /*
    public func setRemoteViewSize(size:CGSize) {
        remoteVideoSize = size
        if self.ongoingCallVideoPauseView.isHidden{
            //  self.remoteUserView = ongoingCallVideoView.remoteUserView
            if size.width > 0 && size.height > 0 {
                UIView.animate(withDuration: 0.1, animations: {
                    if size.width > size.height {
                        let videoRatio = size.width / size.height
                        YALog.print("width - \(size.width), height - \(size.height)")
                        var transform: CATransform3D = CATransform3DIdentity
                        transform = CATransform3DTranslate(transform, ((self.remoteUserView?.frame.size.width)! - (self.remoteUserView?.frame.size.height)!) / 2, ((self.remoteUserView?.frame.size.width)! - (self.remoteUserView?.frame.size.height)!) / 2, 0);
                        transform = CATransform3DMakeRotation(CGFloat(-Double.pi) / CGFloat(2), 0, 0, 1)
                        self.remoteUserView?.layer.transform = transform
                        
                        let remoteViewHeight = UIScreen.main.bounds.size.height
                        let remoteViewWidth = remoteViewHeight / videoRatio
                        
                        YALog.print("Screen width - \(remoteViewWidth), Screen height - \(remoteViewHeight)")
                        //                        self.remoteUserView?.frame = CGRect(x: (self.remoteUserView?.frame.origin.x)!, y: (self.remoteUserView?.frame.origin.y)!, width: remoteViewWidth, height: remoteViewHeight)
                        for (_, view) in (self.remoteUserView?.subviews.enumerated())! {
                            view.frame = CGRect(x: 0, y: 0, width: (self.remoteUserView?.frame.size.height)!, height: (self.remoteUserView?.frame.size.width)!)
                        }
                    }
                    else {
                        let videoRatio = size.height / size.width
                        YALog.print("width - \(size.width), height - \(size.height)")
                        
                        var transform: CATransform3D = CATransform3DIdentity
                        transform = CATransform3DTranslate(transform, ((self.remoteUserView?.frame.size.width)! - (self.remoteUserView?.frame.size.height)!) / 2, ((self.remoteUserView?.frame.size.width)! - (self.remoteUserView?.frame.size.height)!) / 2, 0);
                        transform = CATransform3DMakeRotation(0, 0, 0, 1)
                        self.remoteUserView?.layer.transform = transform
                        
                        let remoteViewHeight = UIScreen.main.bounds.size.height
                        let remoteViewWidth = remoteViewHeight / videoRatio
                        
                        YALog.print("Screen width - \(remoteViewWidth), Screen height - \(remoteViewHeight)")
                        
                        //                        self.remoteUserView?.frame = CGRect(x: (self.remoteUserView?.frame.origin.x)!, y: (self.remoteUserView?.frame.origin.y)!, width: remoteViewWidth, height: remoteViewHeight)
                        
                        for (_, view) in (self.remoteUserView?.subviews.enumerated())! {
                            view.frame = CGRect(x: 0, y: 0, width: (self.remoteUserView?.frame.size.width)!, height: (self.remoteUserView?.frame.size.height)!)
                        }
                        
                    }
                })
            }
        }
    }
    */
    public func isSpeakerEnabled()->Bool{
        return speakerEnableValue
    }
    
    public func isMicEnabled()->Bool{
        return micEnableValue
    }
    
    public func isVideoEnabled()->Bool{
        return videoEnableValue
    }
    
    public func setAudioVideoControl(isVideoEnabled:Bool){
        self.videoEnableValue = isVideoEnabled
        setUpControlButtons()
        
    }
    
    public func setSpeakerControl(isSpeakerEnabled:Bool){
        speakerEnableValue = isSpeakerEnabled
        setUpControlButtons()
        
    }
    
    public func setMicControl(isMicEnabled:Bool){
        micEnableValue = isMicEnabled
        setUpControlButtons()
    }
    
    public func setLocalVideoControl(isLocalVideo: Bool){
        localUserView?.isHidden = !isLocalVideo
        cameraSwitchButton?.isHidden = !isLocalVideo
    }
    
    //New Message Received
    
    public func recievedNewChatMessage(unreadCount : Int){
        if unreadCount > 0{
            handleMessageBadgeLabel(isVisible: true,withCount: unreadCount)
        }
        else
        {
            handleMessageBadgeLabel(isVisible: false,withCount: unreadCount)
            
        }
    }
    
    func handleMessageBadgeLabel(isVisible visible : Bool,withCount unreadMessageCount : Int)
    {
        chatBadgeView.isHidden = !visible
        messageBadgeLabel.text = String(stringInterpolationSegment: unreadMessageCount)
        
    }
    
    func handleUpdatedCallTime(time : Int){
        
    }
    
    public func updateCallCurrentStatus(state:String)
    {
        
        self.videoCallStatusLabel.text = NSLocalizedString(state, comment: "")
        
        // ongoingCallVideoView?.setCallCurrentStatus(state: state)
    }
    
    public func setSpeakerButton(isBTConnected: Bool)
    {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1, execute: {
            self.setSpeakerControlIcon(isBluetooth: isBTConnected)
            
        })
        
        
        //        if outputCategory == .Bluetooth
        //        {
        //            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1, execute: {
        //                self.setSpeakerControlIcon(isBluetooth: true)
        //
        //            })
        //        }
        //        else
        //        {
        //
        //            self.setSpeakerControlIcon(isBluetooth: false)
        //        }
    }
    
    public func setSpeakerControlIcon(isBluetooth: Bool)
    {
        if isBluetooth
        {
            volumeView.frame = CGRect(x: speakerButton.frame.origin.x, y: speakerButton.frame.origin.y, width: speakerButton.frame.size.width, height: speakerButton.frame.size.height)
            volumeView.showsRouteButton = true
            volumeView.setRouteButtonImage(#imageLiteral(resourceName: "audioBluetooth"), for: .normal)
            volumeView.setRouteButtonImage(#imageLiteral(resourceName: "audioBluetooth"), for: .normal)
            volumeView.showsVolumeSlider = false
            volumeView.removeFromSuperview()

            speakerButton.isHidden = true
        }
        else
        {
            volumeView.removeFromSuperview()
            speakerButton.isHidden = false
        }
    }
    
    //MARK:.....MediaView Method(s)
    public func getOngoingMediaViewLocal()->UIView{
        return localUserView!
    }
    
    
    public func setConatactImage(imageURL:String?) {
        /*
        if let thumbImageURL = imageURL {
            let thumbUrl = URL(string: thumbImageURL)
            KingfisherManager.shared.retrieveImage(with:thumbUrl!, options: nil, progressBlock: nil, completionHandler: { (image, error, cacheType, imageURL) -> () in
                if let thumbImage = image {
                    self.setContactImage(image: thumbImage)
                }
            })
            
        }
        */
    }
    
    public func updateContactLabel(_ name:String?){
        if let contactName = name{
            self.videoCallContactLabel.text = contactName.decodeEmoji
            self.videoCallContactLabel.text = contactName.decodeEmoji
        }
    }
}

private extension CallView {
    
    func videoTapOperation()
    {
        self.videoButton.isEnabled = false
        DispatchQueue.main.async {
            self.videoEnableValue = !self.videoEnableValue
            self.setUpControlButtons()
            self.delegate?.ongoingAudioVideoButtonTapped(isVideoEnabled:self.videoEnableValue)
        }
    }
    
    func setUpControlButtons(){
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
            self.micButton.isSelected = !self.micEnableValue
            self.speakerButton.isSelected = !self.speakerEnableValue
            self.videoButton.isSelected = !self.videoEnableValue
        }
        
    }
    
    //MARK:- Handling Media Access
    func checkAccessForMedia(isVideoCall: Bool,completionBlock:@escaping(Bool,Utilities.MediaAccessType)->Void)
    {
        if isVideoCall
        {
            Utilities.checkMediaAccessForVideoCall(completionBlock: { (isGranted, accessType) in
                completionBlock(isGranted, accessType)
            })
        }
        else{
            Utilities.checkMediaAccessForAudioCall(completionBlock: { (isGranted, accessType) in
                completionBlock(isGranted, accessType)
            })
            
        }
    }
}
