# CUCalling

[![CI Status](http://img.shields.io/travis/sourabh_yapapp/CUCalling.svg?style=flat)](https://travis-ci.org/sourabh_yapapp/CUCalling)
[![Version](https://img.shields.io/cocoapods/v/CUCalling.svg?style=flat)](http://cocoapods.org/pods/CUCalling)
[![License](https://img.shields.io/cocoapods/l/CUCalling.svg?style=flat)](http://cocoapods.org/pods/CUCalling)
[![Platform](https://img.shields.io/cocoapods/p/CUCalling.svg?style=flat)](http://cocoapods.org/pods/CUCalling)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

CUCalling is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'CUCalling'
```

## Author

sourabh_yapapp, sourabh.sharotria@yapits.com

## License

CUCalling is available under the MIT license. See the LICENSE file for more info.
